package site.yuanshen.genshin.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import site.yuanshen.genshin.core.entity.History;
import site.yuanshen.genshin.core.mapper.HistoryMapper;
import site.yuanshen.genshin.core.service.HistoryService;

@Service
public class HistoryServiceImpl extends ServiceImpl<HistoryMapper, History> implements HistoryService {

}


