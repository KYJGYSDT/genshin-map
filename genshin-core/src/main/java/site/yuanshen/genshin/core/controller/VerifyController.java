package site.yuanshen.genshin.core.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.core.dto.PunctuateAuditDTO;
import site.yuanshen.genshin.core.manager.OptionManager;
import site.yuanshen.genshin.core.manager.PunctuateManager;
import site.yuanshen.genshin.core.service.PunctuateService;
import site.yuanshen.genshin.core.vo.AreaVO;
import site.yuanshen.genshin.core.vo.PunctuateVO;

import javax.validation.Valid;
import java.util.List;


/**
 * 审核
 *
 * @author Hu
 * @date 2021-06-08 19:21:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/verify")
@Api(value = "verify", tags = "审核API")
public class VerifyController {
	private final OptionManager optionManager;
	private final PunctuateService punctuateService;
	private final PunctuateManager punctuateManager;

	/**
	 * 审核选项查询
	 *
	 * @return R
	 */
	@ApiOperation(value = "审核选项查询", notes = "审核选项查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", paramType = "query"),
	})
	@GetMapping("/option")
	@PreAuthorize("@pms.hasPermission('punctuate_list')")
	public R<List<AreaVO>> verifyOptions(@RequestParam(required = false) String keyword) {
		return R.ok(optionManager.getVerifyOptions(keyword));
	}

	/**
	 * 用户标点列表查询
	 *
	 * @return R
	 */
	@ApiOperation(value = "用户标点列表查询", notes = "用户标点列表查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "itemId", required = true, value = "选项ID", paramType = "path"),
	})
	@GetMapping("/{itemId}")
	@PreAuthorize("@pms.hasPermission('punctuate_list')")
	public R<List<PunctuateVO>> list(@PathVariable Long itemId, @RequestParam(required = false) String keyword) {
		return R.ok(punctuateService.listByItemId(itemId, keyword));
	}

	/**
	 * 用户标点审核通过
	 *
	 * @return R
	 */
	@ApiOperation(value = "用户标点审核通过", notes = "用户标点审核通过")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('punctuate_pass')")
	public R<Boolean> pass(@RequestBody @Valid PunctuateAuditDTO punctuateAuditDTO) {
		return R.ok(punctuateManager.pass(punctuateAuditDTO));
	}

	/**
	 * 用户标点审核拒绝
	 *
	 * @return R
	 */
	@ApiOperation(value = "用户标点审核拒绝", notes = "用户标点审核拒绝")
	@PostMapping("/reject")
	@PreAuthorize("@pms.hasPermission('punctuate_reject')")
	public R<Boolean> reject(@RequestBody @Valid PunctuateAuditDTO punctuateAuditDTO) {
		return R.ok(punctuateService.reject(punctuateAuditDTO));
	}

	/**
	 * 用户标点删除
	 *
	 * @return R
	 */
	@ApiOperation(value = "用户标点删除", notes = "用户标点删除")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "punctuateIds", required = true, value = "用户标点ID", paramType = "query"),
	})
	@DeleteMapping
	@PreAuthorize("@pms.hasPermission('punctuate_delete')")
	public R<Boolean> remove(@RequestParam List<Long> punctuateIds) {
		return R.ok(punctuateService.removeByIds(punctuateIds));
	}

}
