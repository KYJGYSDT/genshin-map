package site.yuanshen.genshin.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import site.yuanshen.genshin.core.entity.Item;

import java.util.List;

@Mapper
public interface ItemMapper extends BaseMapper<Item> {
	/**
	 * 查询所有选项 并标记代办
	 *
	 * @param keyword 关键字
	 * @return List
	 */
	List<Item> selectVerifyList(@Param("keyword") String keyword);

	/**
	 * 根据点位id查询选项
	 *
	 * @param markerId 点位id
	 * @return 选项
	 */
	Item selectByMarkerId(@Param("markerId") Long markerId);
}