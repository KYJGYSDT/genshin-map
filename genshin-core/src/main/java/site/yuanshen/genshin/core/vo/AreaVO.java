package site.yuanshen.genshin.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import site.yuanshen.genshin.common.base.TreeNode;
import site.yuanshen.genshin.core.entity.Area;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * 地区
 *
 * @author :  Hu
 */
@Data
@NoArgsConstructor
public class AreaVO extends TreeNode implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@ApiModelProperty(value = "ID")
	private Long id;

	/**
	 * 名
	 */
	@ApiModelProperty(value = "名")
	private String name;

	/**
	 * 图标
	 */
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 待办
	 */
	@ApiModelProperty(value = "待办")
	private Long pending;

	/**
	 * 是否显示
	 */
	@ApiModelProperty(value = "是否显示")
	private Boolean visibility;

	/**
	 * 修改次数
	 */
	@ApiModelProperty(value = "修改次数")
	private Long version;

//	@ApiModelProperty(value = "子级")
//	private List<AreaVO> areaVOList;




	/**
	 * 类型list
	 */
	@ApiModelProperty(value = "类型列表")
	private List<TypeVO> types;

	public AreaVO(Area area) {
		if (Objects.nonNull(area)) {
			BeanUtils.copyProperties(area, this);
		}
	}
}
