package site.yuanshen.genshin.core.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.yuanshen.genshin.common.exception.ExecuteFailException;
import site.yuanshen.genshin.core.entity.ItemPunctuate;
import site.yuanshen.genshin.core.mapper.ItemPunctuateMapper;
import site.yuanshen.genshin.core.service.ItemPunctuateService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemPunctuateServiceImpl extends ServiceImpl<ItemPunctuateMapper, ItemPunctuate> implements ItemPunctuateService {

	@Override
	@Transactional
	public Boolean batchUpdate(List<ItemPunctuate> itemPunctuates) {
		List<Long> punctuateIds = itemPunctuates.stream().map(ItemPunctuate::getPunctuateId).collect(Collectors.toList());

		if (!SqlHelper.retBool(baseMapper.delete(Wrappers.<ItemPunctuate>lambdaQuery().in(ItemPunctuate::getPunctuateId, punctuateIds)))) {
			throw new ExecuteFailException("更新失败");
		}

		if (!this.saveBatch(itemPunctuates)) {
			throw new ExecuteFailException("更新失败");
		}

		return Boolean.TRUE;
	}
}
