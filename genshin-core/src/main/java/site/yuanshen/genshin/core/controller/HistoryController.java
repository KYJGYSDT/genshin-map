package site.yuanshen.genshin.core.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.core.entity.History;
import site.yuanshen.genshin.core.manager.HistoryManager;
import site.yuanshen.genshin.core.service.HistoryService;

/**
 * 历史记录
 *
 * @author :  Hu
 */
@RestController
@AllArgsConstructor
@RequestMapping("/history")
@Api(value = "history", tags = "历史记录API")
public class HistoryController {
	private final HistoryService historyService;
	private final HistoryManager historyManager;


	/**
	 * 历史记录分页
	 *
	 * @return R
	 */
	@ApiOperation(value = "历史记录分页", notes = "历史记录分页")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('history_list')")
	public R<Page<History>> listHistory(Page<History> page, Integer type, Long id) {
		return R.ok(historyService.page(page, Wrappers.<History>lambdaQuery().eq(History::getType, type).eq(History::getTId, id)));
	}


	/**
	 * 回滚记录
	 *
	 * @return R
	 */
	@ApiOperation(value = "回滚记录", notes = "回滚记录")
	@PostMapping("/rollback")
	@PreAuthorize("@pms.hasPermission('history_rollback')")
	public R<Boolean> rollback(Long id) {
		return R.ok(historyManager.rollback(id));
	}

}
