package site.yuanshen.genshin.core.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import site.yuanshen.genshin.common.amqp.QueueProperties;
import site.yuanshen.genshin.core.service.RabbitMqAsyncService;

/**
 * 使用RabbitMQ的异步消息推送服务 实现类
 *
 * @author Moment
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class RabbitMqAsyncServiceImpl implements RabbitMqAsyncService {

	private final RabbitTemplate rabbitTemplate;
	private final QueueProperties queueProperties;

	/**
	 * 分发消息到分发队列
	 * @param message    消息内容
	 * @param methodName 方法名(用于在日志中区分不同的方法)
	 */
	@Override
	@Async
	public void distributePunctuate(String message, String methodName) {
		try {
			rabbitTemplate.convertAndSend(queueProperties.getDistributePunctuate(), message);
		} catch (AmqpException e) {
			log.error("AMQP ERROR：{} 消息发送失败:{}",methodName, e.getMessage());
		}
	}
}
