package site.yuanshen.genshin.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.common.constant.CommonConstant;
import site.yuanshen.genshin.common.exception.DataObsoleteException;
import site.yuanshen.genshin.common.exception.ExecuteFailException;
import site.yuanshen.genshin.common.exception.ResourceNotExistException;
import site.yuanshen.genshin.common.minio.MinioTemplate;
import site.yuanshen.genshin.common.util.FileUtil;
import site.yuanshen.genshin.core.convert.HistoryConvert;
import site.yuanshen.genshin.core.dto.TypeDTO;
import site.yuanshen.genshin.core.entity.Type;
import site.yuanshen.genshin.core.mapper.TypeMapper;
import site.yuanshen.genshin.core.service.HistoryService;
import site.yuanshen.genshin.core.service.TypeService;
import site.yuanshen.genshin.core.vo.TypeVO;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 类目
 *
 * @author Hu
 * @date 2021-06-08 19:21:23
 */
@Service
@AllArgsConstructor
public class TypeServiceImpl extends ServiceImpl<TypeMapper, Type> implements TypeService {
	private final CacheManager cacheManager;
	private final MinioTemplate minioTemplate;
	private final HistoryService historyService;

	@Override
	@Cacheable(value = CacheConstant.TYPE_VISIBILITY, key = "'all'", unless = "#result == null || #result.isEmpty()")
	public List<Type> findAllVisibility() {
		return baseMapper.selectList(Wrappers.<Type>lambdaQuery().eq(Type::getVisibility, 1));
	}

	@Override
	@Cacheable(value = CacheConstant.ALL_TYPE, key = "#areaId", unless = "#result == null || #result.isEmpty()")
	public List<TypeVO> voListByAreaId(Long areaId) {
		List<Type> types = baseMapper.selectList(Wrappers.<Type>lambdaQuery().eq(Type::getAreaId, areaId));
		return types.stream().map(TypeVO::new).collect(Collectors.toList());
	}

	@Override
	@CacheEvict(value = CacheConstant.ALL_TYPE, key = "#typeDTO.areaId")
	public Boolean saveType(TypeDTO typeDTO) {
		MultipartFile file = typeDTO.getIconBase64();
		FileUtil.limitImgFile(file);

		Type type = new Type();
		BeanUtils.copyProperties(typeDTO, type);
		type.setIcon(minioTemplate.uploadFile(file));
		type.setVersion(null);

		if (!SqlHelper.retBool(baseMapper.insert(type))) {
			throw new ExecuteFailException("新增失败");
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	@Caching(evict = {
			@CacheEvict(value = CacheConstant.ALL_TYPE, key = "#typeDTO.areaId"),
			@CacheEvict(value = CacheConstant.TYPE_VISIBILITY, key = "'all'")
	})
	public Boolean updateType(TypeDTO typeDTO) {
		Type type = baseMapper.selectById(typeDTO.getTypeId());
		if (Objects.isNull(type)) {
			throw new ResourceNotExistException("类型不存在");
		}

		if (!Objects.equals(type.getVersion(), typeDTO.getVersion())) {
			throw new DataObsoleteException("数据已被修改");
		}

		historyService.save(HistoryConvert.convert(type));

		BeanUtils.copyProperties(typeDTO, type);

		MultipartFile file = typeDTO.getIconBase64();
		if (Objects.nonNull(file)) {
			FileUtil.limitImgFile(file);
			type.setIcon(minioTemplate.uploadFile(file));
		}
		if (!SqlHelper.retBool(baseMapper.updateById(type))) {
			throw new ExecuteFailException("更新失败");
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	public Boolean deleteType(Long id) {
		Type type = baseMapper.selectById(id);
		if (Objects.isNull(type)) {
			throw new ResourceNotExistException("类型不存在");
		}
		historyService.save(HistoryConvert.convert(type));

		if (!SqlHelper.retBool(baseMapper.deleteById(id))) {
			throw new ExecuteFailException("删除失败");
		}

		cacheManager.getCache(CacheConstant.ALL_TYPE).evict(type.getAreaId());
		if (Objects.equals(type.getVisibility(), CommonConstant.VISIBILITY)) {
			cacheManager.getCache(CacheConstant.TYPE_VISIBILITY).evict("all");
		}

		return Boolean.TRUE;
	}

	@Override
	public Boolean switchType(Long id) {
		Type type = baseMapper.selectById(id);
		if (Objects.isNull(type)) {
			throw new ResourceNotExistException("类型不存在");
		}

		UpdateWrapper<Type> wrapper = new UpdateWrapper<>();
		wrapper.setSql("visibility = case visibility when 1 then 0 else 1 end").eq("id", id);

		if (!SqlHelper.retBool(baseMapper.update(null, wrapper))) {
			throw new ExecuteFailException("修改失败");
		}
		cacheManager.getCache(CacheConstant.ALL_TYPE).evict(type.getAreaId());
		cacheManager.getCache(CacheConstant.TYPE_VISIBILITY).evict("all");

		return Boolean.TRUE;
	}

	@Override
	public List<Type> findVisibilityByIds(List<Long> typeIds) {
		return baseMapper.selectList(Wrappers.<Type>lambdaQuery().eq(Type::getVisibility, 1).in(Type::getId, typeIds));
	}

	@Override
	@Transactional
	public Boolean update(Type type) {
		historyService.save(HistoryConvert.convert(type));

		if (!SqlHelper.retBool(baseMapper.updateById(type))) {
			throw new ExecuteFailException("更新失败");
		}
		cacheManager.getCache(CacheConstant.ALL_TYPE).evict(type.getAreaId());
		cacheManager.getCache(CacheConstant.TYPE_VISIBILITY).evict("all");

		return Boolean.TRUE;
	}
}
