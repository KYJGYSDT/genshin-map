package site.yuanshen.genshin.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "批量标点DTO")
public class BatchPunctuateDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 操作类型
	 *
	 * @see site.yuanshen.genshin.core.enums.PunctuateOptionType
	 */
	@NotNull(message = "操作类型不能为空")
	@Max(value = 3, message = "操作类型不在可选范围内")
	@Min(value = 1, message = "操作类型不在可选范围内")
	@ApiModelProperty(value = "操作类型 1: 新增 2: 修改 3: 删除")
	private Integer optionType;

	@Valid
	@Size.List(@Size(min = 1, message = "点位信息不能为空"))
	private List<PunctuateDTO> punctuates;
}
