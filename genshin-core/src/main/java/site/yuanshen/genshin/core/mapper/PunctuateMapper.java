package site.yuanshen.genshin.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import site.yuanshen.genshin.core.entity.Punctuate;
import site.yuanshen.genshin.core.vo.PunctuateVO;

import java.util.List;

@Mapper
public interface PunctuateMapper extends BaseMapper<Punctuate> {

	/**
	 * 根据itemId和keyword查询
	 *
	 * @param itemId  选项id
	 * @param keyword 关键字
	 * @return List
	 */
	List<PunctuateVO> listVOByItemIdAndKeywordAndCreatorId(@Param("itemId") Long itemId, @Param("keyword") String keyword, @Param("creatorId") Long creatorId);
}