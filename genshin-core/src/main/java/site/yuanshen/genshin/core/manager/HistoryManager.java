package site.yuanshen.genshin.core.manager;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import site.yuanshen.genshin.common.exception.ParamNotSupportException;
import site.yuanshen.genshin.common.exception.ResourceNotExistException;
import site.yuanshen.genshin.core.convert.HistoryConvert;
import site.yuanshen.genshin.core.entity.*;
import site.yuanshen.genshin.core.enums.HistoryType;
import site.yuanshen.genshin.core.service.*;

import java.util.Objects;

@Slf4j
@Component
@AllArgsConstructor
public class HistoryManager {
	private final HistoryService historyService;
	private final AreaService areaService;
	private final TypeService typeService;
	private final ItemService itemService;
	private final MarkerService markerService;

	public Boolean rollback(Long id) {
		History history = historyService.getById(id);
		if (Objects.isNull(history)) {
			throw new ResourceNotExistException("history: " + id + " not exist");
		}

		HistoryType historyType = HistoryType.from(history.getType());

		switch (historyType) {
			case AREA:
				Area area = (Area) HistoryConvert.reConvert(history, historyType);
				return areaService.update(area);
			case TYPE:
				Type type = (Type) HistoryConvert.reConvert(history, historyType);
				return typeService.update(type);
			case ITEM:
				Item item = (Item) HistoryConvert.reConvert(history, historyType);
				return itemService.update(item);
			case MARKER:
				Marker marker = (Marker) HistoryConvert.reConvert(history, historyType);
				return markerService.update(marker);
			default:
				throw new ParamNotSupportException("unknown history type " + history);
		}
	}
}
