package site.yuanshen.genshin.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import site.yuanshen.genshin.common.exception.ParamNotSupportException;

import java.util.Arrays;

@AllArgsConstructor
public enum PunctuateOptionType {

	/**
	 * 新增
	 */
	INSERT(1),

	/**
	 * 修改
	 */
	UPDATE(2),

	/**
	 * 删除
	 */
	DELETE(3),

	;

	@Getter
	private final Integer value;

	public static PunctuateOptionType from(Integer value) {
		return Arrays.stream(values()).filter(type -> type.getValue().equals(value)).findAny().orElseThrow(() -> new ParamNotSupportException("操作类型[ " + value + " ]不在可选范围内"));
	}
}
