package site.yuanshen.genshin.core.controller;


import cn.hutool.core.lang.Assert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.common.exception.CheckException;
import site.yuanshen.genshin.core.dto.AreaDTO;
import site.yuanshen.genshin.core.dto.ItemDTO;
import site.yuanshen.genshin.core.dto.MarkerDTO;
import site.yuanshen.genshin.core.dto.TypeDTO;
import site.yuanshen.genshin.core.service.AreaService;
import site.yuanshen.genshin.core.service.ItemService;
import site.yuanshen.genshin.core.service.MarkerService;
import site.yuanshen.genshin.core.service.TypeService;
import site.yuanshen.genshin.core.vo.AreaVO;
import site.yuanshen.genshin.core.vo.ItemVO;
import site.yuanshen.genshin.core.vo.MarkerVO;
import site.yuanshen.genshin.core.vo.TypeVO;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * 地图基础信息
 *
 * @author :  Hu
 */
@Validated
@RestController
@AllArgsConstructor
@RequestMapping("/basic")
@Api(value = "basic", tags = "地图基础信息API")
public class BasicController {
	private final AreaService areaService;
	private final TypeService typeService;
	private final ItemService itemService;
	private final MarkerService markerService;

	/* ----------------------------- 区域 ----------------------------- */

	/**
	 * 区域列表
	 *
	 * @return R
	 */
	@ApiOperation(value = "区域列表", notes = "区域列表")
	@GetMapping("/area")
	@PreAuthorize("@pms.hasPermission('basic_list')")
	public R<List<AreaVO>> listArea() {
		return R.ok(areaService.voList());
	}


	/**
	 * 新增区域
	 *
	 * @return R
	 */
	@ApiOperation(value = "新增区域", notes = "新增区域")
	@PutMapping("/area")
	@PreAuthorize("@pms.hasPermission('basic_add')")
	public R<Boolean> saveArea(@RequestBody @Valid AreaDTO areaDTO) {
		Assert.notNull(areaDTO.getIconBase64(), () -> new CheckException("图标不能为空"));
		return R.ok(areaService.saveArea(areaDTO));
	}

	/**
	 * 修改区域
	 *
	 * @return R
	 */
	@ApiOperation(value = "修改区域", notes = "修改区域")
	@PostMapping("/area")
	@PreAuthorize("@pms.hasPermission('basic_edit')")
	public R<Boolean> updateArea(@RequestBody @Valid AreaDTO areaDTO) {
		Assert.isTrue(Objects.nonNull(areaDTO.getIconBase64()) || StringUtils.isNotBlank(areaDTO.getIcon()), () -> new CheckException("图标不能为空"));
		Assert.notNull(areaDTO.getAreaId(), () -> new CheckException("区域ID不能为空"));
		Assert.notNull(areaDTO.getVersion(), () -> new CheckException("版本号不能为空"));
		return R.ok(areaService.updateArea(areaDTO));
	}

	/**
	 * 删除区域
	 *
	 * @return R
	 */
	@ApiOperation(value = "删除区域", notes = "删除区域")
	@DeleteMapping("/area/{id}")
	@PreAuthorize("@pms.hasPermission('basic_delete')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", required = true, value = "区域ID", paramType = "path"),
	})
	public R<Boolean> deleteArea(@PathVariable Long id) {
		return R.ok(areaService.deleteArea(id));
	}

	/**
	 * 启停区域
	 *
	 * @return R
	 */
	@ApiOperation(value = "启停区域", notes = "启停区域")
	@PostMapping("/area/switch/{id}")
	@PreAuthorize("@pms.hasPermission('basic_switch')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", required = true, value = "区域ID", paramType = "path"),
	})
	public R<Boolean> switchArea(@PathVariable Long id) {
		return R.ok(areaService.switchArea(id));
	}

	/* ----------------------------- 类型 ----------------------------- */

	/**
	 * 类型列表
	 *
	 * @return R
	 */
	@ApiOperation(value = "类型列表", notes = "类型列表")
	@GetMapping("/type/{areaId}")
	@PreAuthorize("@pms.hasPermission('basic_list')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "areaId", required = true, value = "区域ID", paramType = "path"),
	})
	public R<List<TypeVO>> listType(@PathVariable Long areaId) {
		return R.ok(typeService.voListByAreaId(areaId));
	}

	/**
	 * 新增类型
	 *
	 * @return R
	 */
	@ApiOperation(value = "新增类型", notes = "新增类型")
	@PutMapping("/type")
	@PreAuthorize("@pms.hasPermission('basic_add')")
	public R<Boolean> saveType(@RequestBody @Valid TypeDTO typeDTO) {
		Assert.notNull(typeDTO.getIconBase64(), () -> new CheckException("图标不能为空"));
		return R.ok(typeService.saveType(typeDTO));
	}

	/**
	 * 修改类型
	 *
	 * @return R
	 */
	@ApiOperation(value = "修改类型", notes = "修改类型")
	@PostMapping("/type")
	@PreAuthorize("@pms.hasPermission('basic_edit')")
	public R<Boolean> updateType(@RequestBody @Valid TypeDTO typeDTO) {
		Assert.isTrue(Objects.nonNull(typeDTO.getIconBase64()) || StringUtils.isNotBlank(typeDTO.getIcon()), () -> new CheckException("图标不能为空"));
		Assert.notNull(typeDTO.getTypeId(), () -> new CheckException("类型ID不能为空"));
		Assert.notNull(typeDTO.getVersion(), () -> new CheckException("版本号不能为空"));
		return R.ok(typeService.updateType(typeDTO));
	}

	/**
	 * 删除类型
	 *
	 * @return R
	 */
	@ApiOperation(value = "删除类型", notes = "删除类型")
	@DeleteMapping("/type/{id}")
	@PreAuthorize("@pms.hasPermission('basic_delete')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", required = true, value = "类型ID", paramType = "path"),
	})
	public R<Boolean> deleteType(@PathVariable Long id) {
		return R.ok(typeService.deleteType(id));
	}

	/**
	 * 启停类型
	 *
	 * @return R
	 */
	@ApiOperation(value = "启停类型", notes = "启停类型")
	@PostMapping("/type/switch/{id}")
	@PreAuthorize("@pms.hasPermission('basic_switch')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", required = true, value = "类型ID", paramType = "path"),
	})
	public R<Boolean> switchType(@PathVariable Long id) {
		return R.ok(typeService.switchType(id));
	}

	/* ----------------------------- 选项 ----------------------------- */

	/**
	 * 选项列表
	 *
	 * @return R
	 */
	@ApiOperation(value = "选项列表", notes = "选项列表")
	@GetMapping("/item/{typeId}")
	@PreAuthorize("@pms.hasPermission('basic_list')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "typeId", required = true, value = "类型ID", paramType = "path"),
	})
	public R<List<ItemVO>> listItem(@PathVariable Long typeId) {
		return R.ok(itemService.voListByTypeId(typeId));
	}

	/**
	 * 新增选项
	 *
	 * @return R
	 */
	@ApiOperation(value = "新增选项", notes = "新增选项")
	@PutMapping("/item")
	@PreAuthorize("@pms.hasPermission('basic_add')")
	public R<Boolean> saveType(@RequestBody @Valid ItemDTO itemDTO) {
		Assert.notNull(itemDTO.getIconBase64(), () -> new CheckException("图标不能为空"));
		return R.ok(itemService.saveItem(itemDTO));
	}

	/**
	 * 修改选项
	 *
	 * @return R
	 */
	@ApiOperation(value = "修改选项", notes = "修改选项")
	@PostMapping("/item")
	@PreAuthorize("@pms.hasPermission('basic_edit')")
	public R<Boolean> updateItem(@RequestBody @Valid ItemDTO itemDTO) {
		Assert.isTrue(Objects.nonNull(itemDTO.getIconBase64()) || StringUtils.isNotBlank(itemDTO.getIcon()), () -> new CheckException("图标不能为空"));
		Assert.notNull(itemDTO.getItemId(), () -> new CheckException("选项ID不能为空"));
		Assert.notNull(itemDTO.getVersion(), () -> new CheckException("版本号不能为空"));
		return R.ok(itemService.updateItem(itemDTO));
	}

	/**
	 * 删除选项
	 *
	 * @return R
	 */
	@ApiOperation(value = "删除选项", notes = "删除选项")
	@DeleteMapping("/item/{id}")
	@PreAuthorize("@pms.hasPermission('basic_delete')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", required = true, value = "类型ID", paramType = "path"),
	})
	public R<Boolean> deleteItem(@PathVariable Long id) {
		return R.ok(itemService.deleteItem(id));
	}

	/**
	 * 启停选项
	 *
	 * @return R
	 */
	@ApiOperation(value = "启停选项", notes = "启停选项")
	@PostMapping("/item/switch/{id}")
	@PreAuthorize("@pms.hasPermission('basic_switch')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", required = true, value = "选项ID", paramType = "path"),
	})
	public R<Boolean> switchItem(@PathVariable Long id) {
		return R.ok(itemService.switchItem(id));
	}

	/* ----------------------------- 点位 ----------------------------- */

	/**
	 * 点位列表
	 *
	 * @return R
	 */
	@ApiOperation(value = "点位列表", notes = "点位列表")
	@GetMapping("/marker/{itemId}")
	@PreAuthorize("@pms.hasPermission('basic_list')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "itemId", required = true, value = "选项ID", paramType = "path"),
			@ApiImplicitParam(name = "keyword", value = "关键字", paramType = "param"),
	})
	public R<List<MarkerVO>> listMarker(@PathVariable Long itemId,
										@RequestParam(required = false) String keyword) {
		return R.ok(markerService.voListByIdAndKeyword(itemId, keyword));
	}

	/**
	 * 查询点位
	 *
	 * @return R
	 */
	@ApiOperation(value = "查询点位", notes = "查询点位")
	@GetMapping("/marker")
	@PreAuthorize("@pms.hasPermission('basic_list')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "markerId", required = true, value = "点位ID", paramType = "query"),
	})
	public R<MarkerVO> findMarker(@RequestParam Long markerId) {
		return R.ok(markerService.voById(markerId));
	}

	/**
	 * 新增点位
	 *
	 * @return R
	 */
	@ApiOperation(value = "新增点位", notes = "新增点位")
	@PutMapping("/marker")
	@PreAuthorize("@pms.hasPermission('basic_add')")
	public R<Boolean> saveMarker(@RequestBody @Valid MarkerDTO markerDTO) {
		return R.ok(markerService.saveMarker(markerDTO));
	}

	/**
	 * 修改点位
	 *
	 * @return R
	 */
	@ApiOperation(value = "修改点位", notes = "修改点位")
	@PostMapping("/marker")
	@PreAuthorize("@pms.hasPermission('basic_edit')")
	public R<Boolean> updateMarker(@RequestBody List<@Valid MarkerDTO> markerDTOS) {
		markerDTOS.forEach(markerDTO -> {
			Assert.notNull(markerDTO.getMarkerId(), () -> new CheckException("点位ID不能为空"));
			Assert.notNull(markerDTO.getVersion(), () -> new CheckException("版本号不能为空"));
		});
		return R.ok(markerService.batchUpdateMarker(markerDTOS));
	}

	/**
	 * 删除点位
	 *
	 * @return R
	 */
	@ApiOperation(value = "删除点位", notes = "删除点位")
	@DeleteMapping("/marker/{id}")
	@PreAuthorize("@pms.hasPermission('basic_delete')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", required = true, value = "点位ID", paramType = "path"),
	})
	public R<Boolean> deleteMarker(@PathVariable Long id) {
		return R.ok(markerService.deleteMarker(id));
	}

	/**
	 * 启停点位
	 *
	 * @return R
	 */
	@ApiOperation(value = "启停点位", notes = "启停点位")
	@PostMapping("/marker/switch/{id}")
	@PreAuthorize("@pms.hasPermission('basic_switch')")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", required = true, value = "点位ID", paramType = "path"),
	})
	public R<Boolean> switchMarker(@PathVariable Long id) {
		return R.ok(markerService.switchMarker(id));
	}

}
