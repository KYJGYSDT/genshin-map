package site.yuanshen.genshin.core.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

@ApiModel(value = "`type`")
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "`type`")
public class Type extends BaseEntity {
	private static final long serialVersionUID = 1L;
	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "id")
	private Long id;

	/**
	 * 名
	 */
	@TableField(value = "`name`")
	@ApiModelProperty(value = "名")
	private String name;

	/**
	 * 图标
	 */
	@TableField(value = "icon")
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 是否显示 1显示 0不显示
	 */
	@TableField(value = "visibility")
	@ApiModelProperty(value = "是否显示 1显示 0不显示")
	private Boolean visibility;

	/**
	 * 区域ID
	 */
	@TableField(value = "area_id")
	@ApiModelProperty(value = "区域ID")
	private Long areaId;
}