package site.yuanshen.genshin.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import site.yuanshen.genshin.common.multipart.Base64MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "点位DTO")
public class MarkerDTO implements Serializable {


	/**
	 * id
	 */
	@ApiModelProperty(value = "id")
	private Long markerId;

	/**
	 * 选项id
	 */
	@NotNull(message = "选项ID不能为空")
	@Size.List(@Size(min = 1, message = "选项ID不能为空"))
	@ApiModelProperty(value = "选项id")
	private List<Long> itemIds;

	/**
	 * 标点内容
	 */
	@ApiModelProperty(value = "标点内容")
	private String content;

	/**
	 * 坐标
	 */
	@NotBlank(message = "坐标不能为空")
	@ApiModelProperty(value = "坐标")
	private String position;

	/**
	 * 刷新时间
	 */
	@NotNull(message = "刷新时间不能为空")
	@ApiModelProperty(value = "刷新时间")
	private Integer time;

	/**
	 * 资源文件
	 */
	@ApiModelProperty(value = "资源文件Base64", dataType = "java.lang.String")
	private Base64MultipartFile resourceBase64;

	/**
	 * 资源地址
	 */
	@ApiModelProperty(value = "资源地址")
	private String resource;

	/**
	 * 修改次数
	 */
	@ApiModelProperty(value = "修改次数")
	private Long version;
}
