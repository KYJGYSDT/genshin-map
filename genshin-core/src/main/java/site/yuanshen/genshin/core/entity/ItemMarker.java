package site.yuanshen.genshin.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value = "item_marker")
@Data
@Builder
@TableName(value = "item_marker")
public class ItemMarker implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * item_id
	 */
	@TableField(value = "item_id")
	@ApiModelProperty(value = "item_id")
	private Long itemId;

	/**
	 * marker_id
	 */
	@TableField(value = "marker_id")
	@ApiModelProperty(value = "marker_id")
	private Long markerId;
}