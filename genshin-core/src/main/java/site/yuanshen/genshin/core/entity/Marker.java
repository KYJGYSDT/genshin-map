package site.yuanshen.genshin.core.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

@ApiModel(value = "marker")
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "marker")
public class Marker extends BaseEntity {
	private static final long serialVersionUID = 1L;
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "")
	private Long id;

	/**
	 * 兼容老版本
	 */
	@TableField(value = "m_layer")
	@ApiModelProperty(value = "兼容老版本")
	private Long mLayer;

	/**
	 * 兼容老版本
	 */
	@TableField(value = "m_item_id")
	@ApiModelProperty(value = "兼容老版本")
	private Long mItemId;

	/**
	 * xy
	 */
	@TableField(value = "`position`")
	@ApiModelProperty(value = "xy")
	private String position;

	/**
	 * 点位名称
	 */
	@TableField(value = "`name`")
	@ApiModelProperty(value = "点位名称")
	private String name;

	/**
	 * 描述
	 */
	@TableField(value = "content")
	@ApiModelProperty(value = "描述")
	private String content;

	/**
	 * 图标
	 */
	@TableField(value = "icon")
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 是否显示 1显示 0不显示
	 */
	@TableField(value = "visibility")
	@ApiModelProperty(value = "是否显示 1显示 0不显示")
	private Boolean visibility;

	/**
	 * 刷新时间 小时
	 */
	@TableField(value = "`time`")
	@ApiModelProperty(value = "刷新时间 小时")
	private Integer time;

	/**
	 * 资源地址
	 */
	@TableField(value = "`resource`")
	@ApiModelProperty(value = "资源地址")
	private String resource;
}