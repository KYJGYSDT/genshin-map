package site.yuanshen.genshin.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.core.dto.MarkerDTO;
import site.yuanshen.genshin.core.entity.Marker;
import site.yuanshen.genshin.core.entity.Punctuate;
import site.yuanshen.genshin.core.vo.MarkerVO;

import java.util.List;
import java.util.Map;

/**
 * 点位
 *
 * @author Hu
 * @date 2021-06-08 19:21:23
 */
public interface MarkerService extends IService<Marker> {

	/**
	 * 根据 item id 查询点位
	 *
	 * @param id item id
	 * @return 点位列表
	 */
	List<MarkerVO> findByItemId(Long id);

	/**
	 * 查询全部点位信息
	 * 接入缓存
	 *
	 * @param itemId  选项ID
	 * @param keyword 关键字
	 * @return List
	 */
	List<MarkerVO> voListByIdAndKeyword(Long itemId, String keyword);

	/**
	 * 新增点位信息
	 *
	 * @param markerDTO 点位信息DTO
	 * @return Boolean
	 */
	Boolean saveMarker(MarkerDTO markerDTO);

	/**
	 * 修改点位
	 *
	 * @param markerDTO 点位信息DTO
	 * @return Boolean
	 */
	Boolean batchUpdateMarker(List<MarkerDTO> markerDTOS);

	/**
	 * 删除点位
	 *
	 * @param id 点位信息id
	 * @return Boolean
	 */
	Boolean deleteMarker(Long id);

	/**
	 * 启停点位
	 *
	 * @param id 点位信息id
	 * @return Boolean
	 */
	Boolean switchMarker(Long id);

	/**
	 * 修改点位
	 *
	 * @param marker 点位信息
	 * @return Boolean
	 */
	Boolean update(Marker marker);

	/**
	 * 根据id查询点位
	 *
	 * @param markerId id
	 * @return 点位
	 */
	MarkerVO voById(Long markerId);

	/**
	 * 新增点位信息
	 *
	 * @param punctuateList 用户标点List
	 * @return Boolean
	 */
	Boolean saveMarker(List<Punctuate> punctuateList);


	/**
	 * 修改点位信息
	 *
	 * @param punctuateList 用户标点List
	 * @return Boolean
	 */
	Boolean updateMarker(List<Punctuate> punctuateList);

	/**
	 * 删除点位信息
	 *
	 * @param punctuateList 用户标点List
	 * @return Boolean
	 */
	Boolean removeMarker(List<Punctuate> punctuateList);

	/**
	 * 映射所有点位 <ItemId, List<MarkerVO>>
	 *
	 * @return Map
	 */
	Map<Long, List<MarkerVO>> mapAll();
}
