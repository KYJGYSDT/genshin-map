package site.yuanshen.genshin.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.common.constant.CommonConstant;
import site.yuanshen.genshin.common.exception.DataObsoleteException;
import site.yuanshen.genshin.common.exception.ExecuteFailException;
import site.yuanshen.genshin.common.exception.ResourceNotExistException;
import site.yuanshen.genshin.common.minio.MinioTemplate;
import site.yuanshen.genshin.common.util.FileUtil;
import site.yuanshen.genshin.core.convert.HistoryConvert;
import site.yuanshen.genshin.core.dto.AreaDTO;
import site.yuanshen.genshin.core.entity.Area;
import site.yuanshen.genshin.core.mapper.AreaMapper;
import site.yuanshen.genshin.core.service.AreaService;
import site.yuanshen.genshin.core.service.HistoryService;
import site.yuanshen.genshin.core.vo.AreaVO;
import site.yuanshen.genshin.system.util.MenuTreeUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 地区
 *
 * @author Hu
 * @date 2021-06-08 19:21:23
 */
@Slf4j
@Service
@AllArgsConstructor
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements AreaService {
	private final CacheManager cacheManager;
	private final MinioTemplate minioTemplate;
	private final HistoryService historyService;

	@Override
	@Cacheable(value = CacheConstant.AREA_VISIBILITY, key = "'all'", unless = "#result == null")
	public List<Area> findAllVisibility() {
		return baseMapper.selectList(Wrappers.<Area>lambdaQuery().eq(Area::getVisibility, 1));
	}

	@Override
	@Cacheable(value = CacheConstant.ALL_AREA, key = "'all'", unless = "#result == null")
	public List<AreaVO> voList() {
		List<Area> areas = baseMapper.selectList(Wrappers.emptyWrapper());
		List<AreaVO> collect = areas.stream().map(AreaVO::new).collect(Collectors.toList());
		return MenuTreeUtil.build(collect, 0L);
	}

	@Override
	@CacheEvict(value = CacheConstant.ALL_AREA, allEntries = true)
	public Boolean saveArea(AreaDTO areaDTO) {
		MultipartFile file = areaDTO.getIconBase64();
		FileUtil.limitImgFile(file);

		Area area = new Area();
		BeanUtils.copyProperties(areaDTO, area);
		area.setIcon(minioTemplate.uploadFile(file));
		area.setVersion(null);

		if (!SqlHelper.retBool(baseMapper.insert(area))) {
			throw new ExecuteFailException("新增失败");
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	@CacheEvict(value = {CacheConstant.ALL_AREA, CacheConstant.AREA_VISIBILITY}, key = "'all'")
	public Boolean updateArea(AreaDTO areaDTO) {
		Area area = baseMapper.selectOne(Wrappers.<Area>lambdaQuery().eq(Area::getId, areaDTO.getAreaId()));
		if (Objects.isNull(area)) {
			throw new ResourceNotExistException("区域不存在");
		}

		if (!Objects.equals(area.getVersion(), areaDTO.getVersion())) {
			throw new DataObsoleteException("数据已被修改");
		}

		historyService.save(HistoryConvert.convert(area));

		BeanUtils.copyProperties(areaDTO, area);

		MultipartFile file = areaDTO.getIconBase64();
		if (Objects.nonNull(file)) {
			FileUtil.limitImgFile(file);
			area.setIcon(minioTemplate.uploadFile(file));
		}
		if (!SqlHelper.retBool(baseMapper.updateById(area))) {
			throw new ExecuteFailException("更新失败");
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	@CacheEvict(value = CacheConstant.ALL_AREA, key = "'all'")
	public Boolean deleteArea(Long id) {
		Area area = baseMapper.selectOne(Wrappers.<Area>lambdaQuery().eq(Area::getId, id));
		if (Objects.isNull(area)) {
			throw new ResourceNotExistException("区域不存在");
		}
		historyService.save(HistoryConvert.convert(area));

		if (Objects.equals(area.getVisibility(), CommonConstant.VISIBILITY)) {
			cacheManager.getCache(CacheConstant.AREA_VISIBILITY).evict("all");
		}

		if (!SqlHelper.retBool(baseMapper.deleteById(id))) {
			throw new ExecuteFailException("删除失败");
		}
		UpdateWrapper<Area> wrapper = new UpdateWrapper<>();
		wrapper.eq("parent_id", id);
		if (!SqlHelper.retBool(baseMapper.delete(wrapper))) {
			throw new ExecuteFailException("删除失败");
		}

		return Boolean.TRUE;
	}

	@Override
	@CacheEvict(value = {CacheConstant.ALL_AREA, CacheConstant.AREA_VISIBILITY}, allEntries = true)
	public Boolean switchArea(Long id) {
		UpdateWrapper<Area> wrapper = new UpdateWrapper<>();
		wrapper.setSql("visibility = case visibility when 1 then 0 else 1 end ")
				.and(Wrapper -> Wrapper.eq("id", id).or().eq("parent_id", id));
		if (!SqlHelper.retBool(baseMapper.update(null, wrapper))) {
			throw new ExecuteFailException("修改失败");
		}

		return Boolean.TRUE;
	}

	@Override
	public List<Area> findVisibilityByIds(List<Long> areaIds) {
		return baseMapper.selectList(Wrappers.<Area>lambdaQuery().eq(Area::getVisibility, 1).in(Area::getId, areaIds));
	}

	@Override
	@Transactional
	@CacheEvict(value = {CacheConstant.ALL_AREA, CacheConstant.AREA_VISIBILITY}, key = "'all'")
	public Boolean update(Area area) {
		historyService.save(HistoryConvert.convert(area));

		if (!SqlHelper.retBool(baseMapper.updateById(area))) {
			throw new ExecuteFailException("更新失败");
		}

		return Boolean.TRUE;
	}
}
