package site.yuanshen.genshin.core.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import site.yuanshen.genshin.common.exception.DataObsoleteException;
import site.yuanshen.genshin.common.exception.ResourceNotExistException;
import site.yuanshen.genshin.common.minio.MinioTemplate;
import site.yuanshen.genshin.common.util.FileUtil;
import site.yuanshen.genshin.common.util.SecurityUtil;
import site.yuanshen.genshin.core.dto.BatchPunctuateDTO;
import site.yuanshen.genshin.core.dto.PunctuateAuditDTO;
import site.yuanshen.genshin.core.dto.PunctuateDTO;
import site.yuanshen.genshin.core.entity.Item;
import site.yuanshen.genshin.core.entity.ItemPunctuate;
import site.yuanshen.genshin.core.entity.Punctuate;
import site.yuanshen.genshin.core.enums.PunctuateOptionType;
import site.yuanshen.genshin.core.enums.PunctuateStatus;
import site.yuanshen.genshin.core.mapper.PunctuateMapper;
import site.yuanshen.genshin.core.service.ItemPunctuateService;
import site.yuanshen.genshin.core.service.ItemService;
import site.yuanshen.genshin.core.service.PunctuateService;
import site.yuanshen.genshin.core.vo.PunctuateVO;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class PunctuateServiceImpl extends ServiceImpl<PunctuateMapper, Punctuate> implements PunctuateService {
	private final ItemService itemService;
	private final MinioTemplate minioTemplate;
	private final ItemPunctuateService itemPunctuateService;

	@Override
	@Transactional
	public Boolean punctuate(BatchPunctuateDTO batchPunctuateDTO) {
		List<PunctuateDTO> punctuateDTOList = batchPunctuateDTO.getPunctuates();
		Integer optionType = batchPunctuateDTO.getOptionType();

		List<Long> itemIds = punctuateDTOList.stream().map(PunctuateDTO::getItemIds).flatMap(Collection::stream).distinct().collect(Collectors.toList());
		List<Item> items = itemService.listByIds(itemIds);
		if (items.size() != itemIds.size()) {
			throw new ResourceNotExistException("选项不存在或已被删除");
		}
		Map<Long, Item> idOfItem = items.stream().collect(Collectors.toMap(Item::getId, Function.identity(), (n, o) -> n));

		Map<Punctuate, List<Long>> punctuateOfItemIds = punctuateDTOList.stream().collect(Collectors.toMap(punctuateDTO -> {
			Punctuate punctuate = new Punctuate();
			BeanUtils.copyProperties(punctuateDTO, punctuate);
			punctuate.setOptionType(optionType);

			if (PunctuateOptionType.INSERT.getValue().equals(optionType)) {
				punctuate.setMarkerId(null);
			}

			if (StringUtils.isEmpty(punctuate.getName())) {
				Item item = idOfItem.get(punctuateDTO.getItemIds().get(0));
				punctuate.setName(item.getName());
			}

			MultipartFile file = punctuateDTO.getResourceBase64();

			if (Objects.nonNull(file)) {
				FileUtil.limitImgFile(file);
				punctuate.setResource(minioTemplate.uploadFile(file));
			}
			return punctuate;
		}, PunctuateDTO::getItemIds));

		boolean saveResult = this.saveBatch(punctuateOfItemIds.keySet());

		if (!saveResult) {
			return Boolean.FALSE;
		}

		List<ItemPunctuate> itemPunctuates = punctuateOfItemIds.entrySet().stream().map(entry -> {
			Punctuate punctuate = entry.getKey();
			return entry.getValue().stream().map(itemId -> {
				return ItemPunctuate.builder().punctuateId(punctuate.getId()).itemId(itemId).build();
			}).collect(Collectors.toList());
		}).flatMap(Collection::stream).collect(Collectors.toList());

		return itemPunctuateService.saveBatch(itemPunctuates);
	}

	@Override
	public List<PunctuateVO> listByItemId(Long itemId, String keyword) {
		return baseMapper.listVOByItemIdAndKeywordAndCreatorId(itemId, keyword, null);
	}

	@Override
	public Boolean reject(PunctuateAuditDTO punctuateAuditDTO) {
		List<Punctuate> punctuates = listWaitAuditPunctuate(punctuateAuditDTO.getPunctuateIds());
		List<Long> existIds = punctuates.stream().map(Punctuate::getId).collect(Collectors.toList());

		Optional<Long> hasBeenAudit = punctuateAuditDTO.getPunctuateIds().stream().filter(id -> !existIds.contains(id)).findAny();
		if (hasBeenAudit.isPresent()) {
			log.error("punctuate:{} has been audit", hasBeenAudit.get());
			throw new ResourceNotExistException("点位 [" + hasBeenAudit.get() + "] 已被审核!");
		}

		return audit(punctuateAuditDTO.getPunctuateIds(), PunctuateStatus.AUDIT_REJECT, punctuateAuditDTO.getAuditRemark());
	}

	@Override
	public Boolean audit(List<Long> punctuateIds, PunctuateStatus punctuateStatus, String auditRemark) {
		UpdateWrapper<Punctuate> wrapper = new UpdateWrapper<>();
		wrapper.set("status", punctuateStatus.getValue()).set("audit_remark", auditRemark).in("id", punctuateIds);
		return SqlHelper.retBool(baseMapper.update(null, wrapper));
	}

	@Override
	public List<Punctuate> listWaitAuditPunctuate(List<Long> punctuateIds) {
		List<Punctuate> punctuates = baseMapper.selectList(Wrappers.<Punctuate>lambdaQuery().in(Punctuate::getId, punctuateIds)
				.eq(Punctuate::getStatus, PunctuateStatus.WAIT_AUDIT.getValue()));
		if (CollUtil.isEmpty(punctuates)) {
			log.error("punctuates:{} is null", punctuateIds);
			throw new ResourceNotExistException(HttpStatus.NOT_FOUND, "点位信息不存在或已被审核");
		}
		return punctuates;
	}

	@Override
	public List<PunctuateVO> listOwnByItemId(Long itemId, String keyword) {
		return baseMapper.listVOByItemIdAndKeywordAndCreatorId(itemId, keyword, SecurityUtil.getUserId());
	}

	@Override
	@Transactional
	public Boolean edit(PunctuateDTO punctuateDTO) {
		Punctuate punctuate = this.getById(punctuateDTO.getPunctuateId());
		if (Objects.isNull(punctuate)) {
			throw new ResourceNotExistException("标点不存在");
		}

		if (!PunctuateStatus.couldEdit(punctuate.getStatus())) {
			throw new DataObsoleteException("当前状态不允许修改");
		}

		BeanUtils.copyProperties(punctuateDTO, punctuate);
		punctuate.setStatus(PunctuateStatus.WAIT_AUDIT.getValue());
		boolean updateResult = SqlHelper.retBool(baseMapper.updateById(punctuate));
		if (!updateResult) {
			return Boolean.FALSE;
		}

		List<ItemPunctuate> itemPunctuates = punctuateDTO.getItemIds().stream().map(itemId -> {
			return ItemPunctuate.builder().punctuateId(punctuateDTO.getPunctuateId()).itemId(itemId).build();
		}).collect(Collectors.toList());

		return itemPunctuateService.batchUpdate(itemPunctuates);
	}
}

