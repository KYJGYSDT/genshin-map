package site.yuanshen.genshin.core.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Pair;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import site.yuanshen.genshin.common.exception.ExecuteFailException;
import site.yuanshen.genshin.common.exception.ResourceNotExistException;
import site.yuanshen.genshin.common.minio.MinioTemplate;
import site.yuanshen.genshin.core.convert.HistoryConvert;
import site.yuanshen.genshin.core.dto.MarkerDTO;
import site.yuanshen.genshin.core.entity.*;
import site.yuanshen.genshin.core.mapper.MarkerMapper;
import site.yuanshen.genshin.core.service.*;
import site.yuanshen.genshin.core.vo.ItemMarkerVO;
import site.yuanshen.genshin.core.vo.MarkerVO;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 点位
 *
 * @author Hu
 * @date 2021-06-08 19:21:23
 */
@Slf4j
@Service
@AllArgsConstructor
public class MarkerServiceImpl extends ServiceImpl<MarkerMapper, Marker> implements MarkerService {
	private final ItemService itemService;
	private final MinioTemplate minioTemplate;
	private final HistoryService historyService;
	private final ItemPunctuateService itemPunctuateService;
	private final ItemMarkerService itemMarkerService;

	@Override
	public List<MarkerVO> findByItemId(Long id) {
		List<Marker> markers = baseMapper.listByItemIdAndKeyword(id, true, null);
		return markers.stream().map(MarkerVO::new).collect(Collectors.toList());
	}

	@Override
	public List<MarkerVO> voListByIdAndKeyword(Long itemId, String keyword) {
		List<Marker> markers = baseMapper.listByItemIdAndKeyword(itemId, false, keyword);
		return markers.stream().map(MarkerVO::new).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public Boolean saveMarker(MarkerDTO markerDTO) {
		List<Item> items = itemService.listByIds(markerDTO.getItemIds());
		if (CollUtil.isEmpty(items)) {
			throw new ResourceNotExistException("选项不存在");
		}
		Item item = items.get(0);

		Marker marker = new Marker();
		marker.setName(item.getName());
		marker.setContent(StringUtils.getIfBlank(markerDTO.getContent(), item::getContent));
		marker.setPosition(markerDTO.getPosition());
		marker.setIcon(item.getIcon());
		marker.setTime(markerDTO.getTime());

		MultipartFile file = markerDTO.getResourceBase64();
		if (Objects.nonNull(file)) {
			marker.setResource(minioTemplate.uploadFile(file));
		}

		if (!SqlHelper.retBool(baseMapper.insert(marker))) {
			throw new ExecuteFailException("新增失败");
		}

		List<ItemMarker> itemMarkers = items.stream().map(i -> {
			return ItemMarker.builder().markerId(marker.getId()).itemId(i.getId()).build();
		}).collect(Collectors.toList());

		if (!itemMarkerService.saveBatch(itemMarkers)) {
			throw new ExecuteFailException("新增失败");
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	public Boolean batchUpdateMarker(List<MarkerDTO> markerDTOS) {
		Map<Long, MarkerDTO> idOfMarkerDTO = markerDTOS.stream().collect(Collectors.toMap(MarkerDTO::getMarkerId, Function.identity()));

		List<Marker> originalMarkers = this.listByIds(idOfMarkerDTO.keySet());
		if (CollectionUtil.isEmpty(originalMarkers)) {
			throw new ResourceNotExistException("点位不存在");
		}
		historyService.saveBatch(originalMarkers.stream().map(HistoryConvert::convert).collect(Collectors.toList()));

		Map<Long, Marker> idOfMarker = originalMarkers.stream().collect(Collectors.toMap(Marker::getId, Function.identity()));

		List<Pair<Long, Marker>> pairs = markerDTOS.stream().map(markerDTO -> {
			return markerDTO.getItemIds().stream().map(itemId -> {
				Marker marker = idOfMarker.get(markerDTO.getMarkerId());
				if (Objects.isNull(marker)) {
					throw new ResourceNotExistException("点位不存在");
				}

				BeanUtils.copyProperties(markerDTO, marker);

				MultipartFile file = markerDTO.getResourceBase64();
				if (Objects.nonNull(file)) {
					marker.setResource(minioTemplate.uploadFile(file));
				}

				return Pair.of(itemId, marker);
			}).collect(Collectors.toList());
		}).flatMap(Collection::stream).collect(Collectors.toList());


		if (!this.updateBatchById(pairs.stream().map(Pair::getValue).distinct().collect(Collectors.toList()))) {
			throw new ExecuteFailException("更新失败");
		}

		// 转换 点位选项关系
		List<ItemMarker> itemMarkers = pairs.stream().map(pair -> {
			return ItemMarker.builder().markerId(pair.getValue().getId()).itemId(pair.getKey()).build();
		}).collect(Collectors.toList());

		if (!itemMarkerService.batchUpdate(itemMarkers)) {
			throw new ExecuteFailException("更新失败");
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	public Boolean deleteMarker(Long id) {
		Marker marker = baseMapper.selectById(id);
		if (Objects.isNull(marker)) {
			throw new ResourceNotExistException("点位不存在");
		}

		Item item = itemService.findByMarkerId(marker.getId());
		if (Objects.isNull(item)) {
			throw new ResourceNotExistException("选项不存在");
		}

		historyService.save(HistoryConvert.convert(marker));

		if (!SqlHelper.retBool(baseMapper.deleteById(id))) {
			throw new ExecuteFailException("删除失败");
		}

		return Boolean.TRUE;
	}

	@Override
	public Boolean switchMarker(Long id) {
		Marker marker = baseMapper.selectById(id);
		if (Objects.isNull(marker)) {
			throw new ResourceNotExistException("点位不存在");
		}

		Item item = itemService.findByMarkerId(marker.getId());
		if (Objects.isNull(item)) {
			throw new ResourceNotExistException("选项不存在");
		}

		UpdateWrapper<Marker> wrapper = new UpdateWrapper<>();
		wrapper.setSql("visibility = case visibility when 1 then 0 else 1 end").eq("id", id);

		if (!SqlHelper.retBool(baseMapper.update(null, wrapper))) {
			throw new ExecuteFailException("修改失败");
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	public Boolean update(Marker marker) {
		Item item = itemService.findByMarkerId(marker.getId());
		if (Objects.isNull(item)) {
			throw new ResourceNotExistException("选项不存在");
		}

		historyService.save(HistoryConvert.convert(marker));

		if (!SqlHelper.retBool(baseMapper.updateById(marker))) {
			throw new ExecuteFailException("更新失败");
		}

		return Boolean.TRUE;
	}

	@Override
	public MarkerVO voById(Long markerId) {
		return new MarkerVO(baseMapper.selectById(markerId));
	}

	@Override
	@Transactional
	public Boolean saveMarker(List<Punctuate> punctuates) {
		Map<Long, Punctuate> idOfPunctuate = punctuates.stream().collect(Collectors.toMap(Punctuate::getId, Function.identity()));

		List<ItemPunctuate> itemPunctuates = itemPunctuateService.list(Wrappers.<ItemPunctuate>lambdaQuery().in(ItemPunctuate::getPunctuateId, idOfPunctuate.keySet()));
		if (CollUtil.isEmpty(itemPunctuates)) {
			throw new ResourceNotExistException(HttpStatus.NOT_FOUND, "选项不存在或已被删除");
		}
		Map<Long, List<Long>> punctuateIdOfItemIds = itemPunctuates.stream().collect(Collectors.groupingBy(ItemPunctuate::getPunctuateId, Collectors.mapping(ItemPunctuate::getItemId, Collectors.toList())));

		List<Long> itemIds = punctuateIdOfItemIds.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
		List<Item> items = itemService.list(Wrappers.<Item>lambdaQuery().in(Item::getId, itemIds));
		if (CollUtil.isEmpty(items)) {
			log.error("items:{} is null", itemIds);
			throw new ResourceNotExistException(HttpStatus.NOT_FOUND, "选项不存在或已被删除");
		}

		Map<Long, Item> idOfItem = items.stream().collect(Collectors.toMap(Item::getId, Function.identity()));

		List<Pair<Long, Marker>> pairs = punctuateIdOfItemIds.entrySet().stream().map(entry -> {
			Long punctuateId = entry.getKey();
			Punctuate punctuate = idOfPunctuate.get(punctuateId);
			return entry.getValue().stream().map(itemId -> {
				Item item = idOfItem.get(itemId);
				if (Objects.isNull(item)) {
					log.error("item:{} is null", itemId);
					throw new ResourceNotExistException(HttpStatus.NOT_FOUND, punctuate.getId() + " 审核失败，选项不存在或已被删除");
				}

				Marker marker = new Marker();
				marker.setName(StringUtils.getIfBlank(punctuate.getName(), item::getName));
				marker.setContent(StringUtils.getIfBlank(punctuate.getContent(), item::getContent));
				marker.setPosition(punctuate.getPosition());
				marker.setIcon(item.getIcon());
				marker.setTime(punctuate.getTime());
				marker.setResource(punctuate.getResource());
				marker.setCreatorId(punctuate.getCreatorId());
				return Pair.of(itemId, marker);
			}).collect(Collectors.toList());
		}).flatMap(Collection::stream).collect(Collectors.toList());

		boolean saveResult = this.saveBatch(pairs.stream().map(Pair::getValue).collect(Collectors.toList()));

		List<ItemMarker> itemMarkers = pairs.stream().map(pair -> {
			return ItemMarker.builder().itemId(pair.getKey()).markerId(pair.getValue().getId()).build();
		}).collect(Collectors.toList());

		if (!itemMarkerService.saveBatch(itemMarkers)) {
			throw new ExecuteFailException("新增失败");
		}

		return saveResult;
	}

	@Override
	@Transactional
	public Boolean updateMarker(List<Punctuate> punctuates) {
		List<Long> markerIds = punctuates.stream().map(Punctuate::getMarkerId).collect(Collectors.toList());

		List<Marker> originalMarkers = this.listByIds(markerIds);
		if (originalMarkers.size() != markerIds.size()) {
			throw new ResourceNotExistException("点位不存在");
		}
		historyService.saveBatch(originalMarkers.stream().map(HistoryConvert::convert).collect(Collectors.toList()));

		Map<Long, Punctuate> idOfPunctuate = punctuates.stream().collect(Collectors.toMap(Punctuate::getId, Function.identity()));

		List<ItemPunctuate> itemPunctuates = itemPunctuateService.list(Wrappers.<ItemPunctuate>lambdaQuery().in(ItemPunctuate::getPunctuateId, idOfPunctuate.keySet()));
		if (CollUtil.isEmpty(itemPunctuates)) {
			throw new ResourceNotExistException(HttpStatus.NOT_FOUND, "选项不存在或已被删除");
		}
		Map<Long, List<Long>> punctuateIdOfItemIds = itemPunctuates.stream().collect(Collectors.groupingBy(ItemPunctuate::getPunctuateId, Collectors.mapping(ItemPunctuate::getItemId, Collectors.toList())));

		List<Pair<Long, Marker>> pairs = punctuateIdOfItemIds.entrySet().stream().map(entry -> {
			Long punctuateId = entry.getKey();
			Punctuate punctuate = idOfPunctuate.get(punctuateId);

			return entry.getValue().stream().map(itemId -> {
				Marker marker = new Marker();
				BeanUtils.copyProperties(punctuate, marker);
				marker.setId(punctuate.getMarkerId());
				return Pair.of(itemId, marker);
			}).collect(Collectors.toList());
		}).flatMap(Collection::stream).collect(Collectors.toList());

		boolean updateResult = this.updateBatchById(pairs.stream().map(Pair::getValue).collect(Collectors.toList()));

		List<ItemMarker> itemMarkers = pairs.stream().map(pair -> {
			return ItemMarker.builder().itemId(pair.getKey()).markerId(pair.getValue().getId()).build();
		}).collect(Collectors.toList());

		if (!itemMarkerService.batchUpdate(itemMarkers)) {
			throw new ExecuteFailException("新增失败");
		}
		return updateResult;
	}

	@Override
	@Transactional
	public Boolean removeMarker(List<Punctuate> punctuates) {
		List<Long> markerIds = punctuates.stream().map(Punctuate::getMarkerId).collect(Collectors.toList());

		List<Marker> markers = this.listByIds(markerIds);
		if (markers.size() != markerIds.size()) {
			throw new ResourceNotExistException("点位不存在");
		}
		historyService.saveBatch(markers.stream().map(HistoryConvert::convert).collect(Collectors.toList()));

		return this.removeByIds(markerIds);
	}

	@Override
	public Map<Long, List<MarkerVO>> mapAll() {
		List<ItemMarkerVO> itemMarkerVOList = baseMapper.listAllMarkerItem();
		return itemMarkerVOList.stream().collect(Collectors.groupingBy(ItemMarkerVO::getItemId, Collectors.mapping(Function.identity(), Collectors.toList())));
	}
}
