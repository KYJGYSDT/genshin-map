package site.yuanshen.genshin.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import site.yuanshen.genshin.core.entity.Item;

import java.io.Serializable;
import java.util.Objects;

/**
 * 选项
 *
 * @author :  Hu
 */
@Data
@NoArgsConstructor
public class ItemVO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@ApiModelProperty(value = "id")
	private Long id;

	/**
	 * 名
	 */
	@ApiModelProperty(value = "名")
	private String name;

	/**
	 * 审核通过填充内容
	 */
	@ApiModelProperty(value = "审核通过填充内容")
	private String content;

	/**
	 * 图标
	 */
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 数量
	 */
	@ApiModelProperty(value = "数量")
	private Integer count;

	/**
	 * layer
	 */
	@ApiModelProperty(value = "layer")
	private Long layer;

	/**
	 * 是否显示
	 */
	@ApiModelProperty(value = "是否显示")
	private Boolean visibility;

	/**
	 * 待办
	 */
	@ApiModelProperty(value = "待办")
	private Long pending;

	/**
	 * 修改次数
	 */
	@ApiModelProperty(value = "修改次数")
	private Long version;

	public ItemVO(Item item) {
		if (Objects.nonNull(item)) {
			BeanUtils.copyProperties(item, this);
			if (Objects.isNull(this.layer)) {
				this.layer = this.id;
			}
		}
	}
}
