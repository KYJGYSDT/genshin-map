package site.yuanshen.genshin.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import site.yuanshen.genshin.common.multipart.Base64MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value = "选项DTO")
public class ItemDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@ApiModelProperty(value = "id")
	private Long itemId;

	/**
	 * 名
	 */
	@NotBlank(message = "名不能为空")
	@ApiModelProperty(value = "名")
	private String name;

	/**
	 * 审核通过填充内容
	 */
	@NotBlank(message = "审核通过填充内容不能为空")
	@ApiModelProperty(value = "审核通过填充内容")
	private String content;

	/**
	 * 类型
	 */
	@NotNull(message = "类型ID不能为空")
	@ApiModelProperty(value = "类型")
	private Long typeId;

	/**
	 * 图标文件
	 */
	@ApiModelProperty(value = "图标文件Base64", dataType = "java.lang.String")
	private Base64MultipartFile iconBase64;

	/**
	 * 图标
	 */
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 修改次数
	 */
	@ApiModelProperty(value = "修改次数")
	private Long version;
}
