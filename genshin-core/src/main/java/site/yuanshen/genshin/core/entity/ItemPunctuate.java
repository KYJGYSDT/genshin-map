package site.yuanshen.genshin.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value = "item_punctuate")
@Data
@Builder
@TableName(value = "item_punctuate")
public class ItemPunctuate implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * item_id
	 */
	@TableField(value = "item_id")
	@ApiModelProperty(value = "item_id")
	private Long itemId;

	/**
	 * marker_id
	 */
	@TableField(value = "punctuate_id")
	@ApiModelProperty(value = "marker_id")
	private Long punctuateId;
}