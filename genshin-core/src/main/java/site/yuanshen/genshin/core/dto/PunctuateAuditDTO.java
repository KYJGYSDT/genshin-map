package site.yuanshen.genshin.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "标点审核DTO")
public class PunctuateAuditDTO implements Serializable {

	/**
	 * 用户标点ID
	 */
	@NotNull
	@Size.List(@Size(min = 1, message = "用户标点ID不能为空"))
	@ApiModelProperty(value = "用户标点ID", required = true)
	private List<Long> punctuateIds;

	/**
	 * 审核备注
	 */
	@ApiModelProperty(value = "审核备注")
	private String auditRemark;
}
