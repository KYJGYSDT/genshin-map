package site.yuanshen.genshin.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import site.yuanshen.genshin.common.multipart.Base64MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "标点DTO")
public class PunctuateDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 标点记录ID
	 */
	@ApiModelProperty(value = "标点记录ID")
	private Long punctuateId;

	/**
	 * 原点位ID
	 */
	@ApiModelProperty(value = "原点位ID")
	private Long markerId;

	/**
	 * 选项ID
	 */
	@NotNull(message = "选项不能为空")
	@Size.List(@Size(min = 1, message = "选项不能为空"))
	@ApiModelProperty(value = "选项ID (原型图中的标点类型的ID)", required = true)
	private List<Long> itemIds;

	/**
	 * 标点内容
	 */
	@ApiModelProperty(value = "标点内容")
	private String content;

	/**
	 * 点位名称
	 */
	@ApiModelProperty(value = "点位名称")
	private String name;

	/**
	 * 图片
	 */
	@ApiModelProperty(value = "资源大图Base64", dataType = "java.lang.String")
	private Base64MultipartFile resourceBase64;

	/**
	 * 图片地址
	 */
	@ApiModelProperty(value = "图片地址")
	private String resource;

	/**
	 * 点位坐标
	 */
	@NotBlank(message = "点位坐标不能为空")
	@ApiModelProperty(value = "点位坐标", required = true)
	private String position;

	/**
	 * 刷新时间
	 */
	@ApiModelProperty(value = "刷新时间")
	private Integer time;

}
