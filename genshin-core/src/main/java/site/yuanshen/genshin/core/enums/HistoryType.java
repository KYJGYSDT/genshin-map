package site.yuanshen.genshin.core.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;
import site.yuanshen.genshin.common.exception.ParamNotSupportException;

import java.util.Arrays;

@AllArgsConstructor
public enum HistoryType {

	AREA(1),
	TYPE(2),
	ITEM(3),
	MARKER(4),
	;

	@Getter
	private final Integer code;

	public static HistoryType from(Integer code) {
		return Arrays.stream(values()).filter(type -> type.getCode().equals(code)).findAny().orElseThrow(() -> new ParamNotSupportException("类型[ " + code + " ]不在可选范围内"));
	}

}
