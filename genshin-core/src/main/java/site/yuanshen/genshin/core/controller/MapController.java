package site.yuanshen.genshin.core.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.core.manager.OptionManager;
import site.yuanshen.genshin.core.service.MarkerService;
import site.yuanshen.genshin.core.vo.AreaVO;
import site.yuanshen.genshin.core.vo.MarkerVO;

import java.util.List;
import java.util.Map;

/**
 * 地图
 */
@RestController
@AllArgsConstructor
@RequestMapping
@Api(value = "map", tags = "地图API", description = "需要ts&sign")
public class MapController {
	private final MarkerService markerService;
	private final OptionManager optionManager;

	/**
	 * 选项查询
	 *
	 * @return R
	 */
	@ApiOperation(value = "选项查询", notes = "选项查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", paramType = "query"),
	})
	@GetMapping("/option")
	public R<List<AreaVO>> get(@RequestParam(required = false) String keyword) {
		return R.ok(optionManager.getOptions(keyword));
	}

	/**
	 * 点位查询
	 *
	 * @return R
	 */
	@ApiOperation(value = "点位查询", notes = "点位查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", required = true, value = "选项ID", paramType = "path"),
	})
	@GetMapping("/marker/{id}")
	public R<List<MarkerVO>> get(@PathVariable Long id) {
		return R.ok(markerService.findByItemId(id));
	}


	/**
	 * 全部点位查询
	 *
	 * @return R
	 */
	@ApiOperation(value = "全部点位查询", notes = "全部点位查询")
	@GetMapping("/marker/mapping")
	public R<Map<Long, List<MarkerVO>>> map() {
		return R.ok(markerService.mapAll());
	}


}
