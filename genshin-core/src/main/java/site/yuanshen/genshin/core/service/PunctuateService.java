package site.yuanshen.genshin.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.core.dto.BatchPunctuateDTO;
import site.yuanshen.genshin.core.dto.PunctuateAuditDTO;
import site.yuanshen.genshin.core.dto.PunctuateDTO;
import site.yuanshen.genshin.core.entity.Punctuate;
import site.yuanshen.genshin.core.enums.PunctuateStatus;
import site.yuanshen.genshin.core.vo.PunctuateVO;

import java.util.List;

/**
 * 用户打点
 */
public interface PunctuateService extends IService<Punctuate> {

	/**
	 * 用户打点
	 *
	 * @param punctuateDTO 点位DTO
	 * @return Boolean
	 */
	Boolean punctuate(BatchPunctuateDTO batchPunctuateDTO);

	/**
	 * 根据选项ID查询用户打点数据
	 *
	 * @param itemId  选项ID
	 * @param keyword 关键字
	 * @return List
	 */
	List<PunctuateVO> listByItemId(Long itemId, String keyword);

	/**
	 * 用户标点信息审核拒绝
	 *
	 * @param punctuateAuditDTO 用户标点审核DTO
	 * @return Boolean
	 */
	Boolean reject(PunctuateAuditDTO punctuateAuditDTO);

	/**
	 * 用户标点信息审核
	 *
	 * @param punctuateIds 用户标点信息ID
	 * @return Boolean
	 */
	Boolean audit(List<Long> punctuateIds, PunctuateStatus punctuateStatus, String auditRemark);

	/**
	 * 查询待审核点位
	 *
	 * @param punctuateIds 用户标点信息ID
	 * @return 待审核点位List
	 */
	List<Punctuate> listWaitAuditPunctuate(List<Long> punctuateIds);

	/**
	 * 根据选项ID查询用户打点数据
	 *
	 * @param itemId  选项ID
	 * @param keyword 关键字
	 * @return List
	 */
	List<PunctuateVO> listOwnByItemId(Long itemId, String keyword);

	/**
	 * 修改标点记录
	 *
	 * @param punctuateDTO 标点记录DTO
	 * @return Boolean
	 */
	Boolean edit(PunctuateDTO punctuateDTO);
}
