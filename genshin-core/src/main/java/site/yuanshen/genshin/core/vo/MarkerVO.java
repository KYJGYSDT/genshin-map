package site.yuanshen.genshin.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import site.yuanshen.genshin.core.entity.Marker;

import java.io.Serializable;
import java.util.Objects;

/**
 * 点位
 *
 * @author :  Hu
 */
@Data
@NoArgsConstructor
public class MarkerVO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@ApiModelProperty(value = "id")
	private Long id;

	/**
	 * 兼容老版本
	 */
	@ApiModelProperty(value = "兼容老版本")
	private Long mLayer;

	/**
	 * 兼容老版本
	 */
	@ApiModelProperty(value = "兼容老版本")
	private Long mItemId;

	/**
	 * 坐标
	 */
	@ApiModelProperty(value = "坐标")
	private String position;

	/**
	 * 点位名称
	 */
	@ApiModelProperty(value = "点位名称")
	private String name;

	/**
	 * 内容
	 */
	@ApiModelProperty(value = "内容")
	private String content;

	/**
	 * 图标
	 */
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 刷新时间
	 */
	@ApiModelProperty(value = "刷新时间")
	private Integer time;

	/**
	 * 资源地址
	 */
	@ApiModelProperty(value = "资源地址")
	private String resource;

	/**
	 * 是否显示
	 */
	@ApiModelProperty(value = "是否显示")
	private Boolean visibility;

	/**
	 * 修改次数
	 */
	@ApiModelProperty(value = "修改次数")
	private Long version;

	public MarkerVO(Marker marker) {
		if (Objects.nonNull(marker)) {
			BeanUtils.copyProperties(marker, this);
		}
	}
}
