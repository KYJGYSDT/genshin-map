package site.yuanshen.genshin.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.core.dto.ItemDTO;
import site.yuanshen.genshin.core.entity.Item;
import site.yuanshen.genshin.core.vo.ItemVO;

import java.util.List;

/**
 * 选项
 *
 * @author Hu
 * @date 2021-06-08 19:21:23
 */
public interface ItemService extends IService<Item> {

	/**
	 * 查询所有可显示的选项
	 *
	 * @return List
	 */
	List<Item> findAll();

	/**
	 * 查询所有需要审核的选项
	 *
	 * @return List
	 */
	List<Item> findAllVerify();

	/**
	 * 查询所有选项
	 *
	 * @param typeId 类型ID
	 * @return List
	 */
	List<ItemVO> voListByTypeId(Long typeId);

	/**
	 * 新增选项
	 *
	 * @param itemDTO 选项DTO
	 * @return Boolean
	 */
	Boolean saveItem(ItemDTO itemDTO);

	/**
	 * 修改选项
	 *
	 * @param itemDTO 选项DTO
	 * @return Boolean
	 */
	Boolean updateItem(ItemDTO itemDTO);

	/**
	 * 删除选项
	 *
	 * @param id 选项id
	 * @return Boolean
	 */
	Boolean deleteItem(Long id);

	/**
	 * 启停选项
	 *
	 * @param id 选项id
	 * @return Boolean
	 */
	Boolean switchItem(Long id);

	/**
	 * 根据关键字查询选项List
	 *
	 * @param keyword 关键字
	 * @return 选项List
	 */
	List<Item> findByKeyword(String keyword);

	/**
	 * 根据关键字查询需要审核的选项
	 *
	 * @param keyword 关键字
	 * @return 需要审核的选项List
	 */
	List<Item> findVerifyByKeyword(String keyword);

	/**
	 * 修改选项
	 *
	 * @param item 选项
	 * @return Boolean
	 */
	Boolean update(Item item);

	/**
	 * 根据点位id查询选项
	 *
	 * @param markerId 点位id
	 * @return 选项
	 */
	Item findByMarkerId(Long markerId);
}
