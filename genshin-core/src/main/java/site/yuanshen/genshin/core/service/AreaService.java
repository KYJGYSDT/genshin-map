package site.yuanshen.genshin.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.core.dto.AreaDTO;
import site.yuanshen.genshin.core.entity.Area;
import site.yuanshen.genshin.core.vo.AreaVO;

import java.util.List;

/**
 * 地区
 *
 * @author Hu
 * @date 2021-06-08 19:21:23
 */
public interface AreaService extends IService<Area> {

	/**
	 * 查询全部地区
	 * 接入缓存
	 *
	 * @return List
	 */
	List<Area> findAllVisibility();

	/**
	 * 查询全部地区
	 *
	 * @return List
	 */
	List<AreaVO> voList();

	/**
	 * 新增区域信息
	 *
	 * @param areaDTO 区域DTO
	 * @return Boolean
	 */
	Boolean saveArea(AreaDTO areaDTO);

	/**
	 * 修改区域信息
	 *
	 * @param areaDTO 区域DTO
	 * @return Boolean
	 */
	Boolean updateArea(AreaDTO areaDTO);

	/**
	 * 删除区域
	 *
	 * @param id 区域id
	 * @return Boolean
	 */
	Boolean deleteArea(Long id);

	/**
	 * 启停区域
	 *
	 * @param id 区域id
	 * @return Boolean
	 */
	Boolean switchArea(Long id);

	/**
	 * 根据id查询激活的区域
	 *
	 * @param areaIds areaIds
	 * @return 区域List
	 */
	List<Area> findVisibilityByIds(List<Long> areaIds);

	/**
	 * 更新区域
	 *
	 * @param area 区域
	 * @return Boolean
	 */
	Boolean update(Area area);
}
