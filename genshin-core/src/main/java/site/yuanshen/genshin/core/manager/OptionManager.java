package site.yuanshen.genshin.core.manager;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import site.yuanshen.genshin.core.entity.Area;
import site.yuanshen.genshin.core.entity.Item;
import site.yuanshen.genshin.core.entity.Type;
import site.yuanshen.genshin.core.service.AreaService;
import site.yuanshen.genshin.core.service.ItemService;
import site.yuanshen.genshin.core.service.TypeService;
import site.yuanshen.genshin.core.vo.AreaVO;
import site.yuanshen.genshin.core.vo.ItemVO;
import site.yuanshen.genshin.core.vo.TypeVO;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 选项
 *
 * @author :  Hu
 */
@Slf4j
@Component
@AllArgsConstructor
public class OptionManager {
	private final AreaService areaService;
	private final TypeService typeService;
	private final ItemService itemService;

	/**
	 * 查询选项
	 *
	 * @param keyword 关键字
	 * @return 选项
	 */
	public List<AreaVO> getOptions(String keyword) {
		if (StringUtils.isEmpty(keyword)) {
			List<Area> areas = areaService.findAllVisibility();
			if (CollUtil.isEmpty(areas)) {
				return Lists.newArrayList();
			}
			List<Type> types = typeService.findAllVisibility();
			List<Item> items = itemService.findAll();

			return buildTree(areas, types, items);
		}

		List<Item> items = itemService.findByKeyword(keyword);
		return treeByItems(items);
	}

	/**
	 * 查询选项 并标注代办
	 *
	 * @param keyword 关键字
	 * @return 选项
	 */
	public List<AreaVO> getVerifyOptions(String keyword) {
		if (StringUtils.isEmpty(keyword)) {
			List<Area> areas = areaService.findAllVisibility();
			if (CollUtil.isEmpty(areas)) {
				return Lists.newArrayList();
			}

			List<Type> types = typeService.findAllVisibility();
			List<Item> items = itemService.findAllVerify();

			return buildTree(areas, types, items);
		}

		List<Item> items = itemService.findVerifyByKeyword(keyword);
		return treeByItems(items);
	}

	private List<AreaVO> treeByItems(List<Item> items) {
		if (CollUtil.isEmpty(items)) {
			return Lists.newArrayList();
		}

		List<Long> areaIds = items.stream().map(Item::getAreaId).distinct().collect(Collectors.toList());
		List<Long> typeIds = items.stream().map(Item::getTypeId).collect(Collectors.toList());

		List<Area> areas = areaService.findVisibilityByIds(areaIds);
		List<Type> types = typeService.findVisibilityByIds(typeIds);

		return buildTree(areas, types, items);
	}

	private List<AreaVO> buildTree(List<Area> areas, List<Type> types, List<Item> items) {
		Map<Long, List<Item>> typeIdOfItems = items.stream().collect(Collectors.groupingBy(Item::getTypeId));

		Map<Long, List<Type>> areaIdOfTypes = types.stream().collect(Collectors.groupingBy(Type::getAreaId));

		return areas.stream().map(area -> {
			AreaVO areaVO = new AreaVO(area);

			List<Type> areaTypes = areaIdOfTypes.get(area.getId());
			if (CollUtil.isEmpty(areaTypes)) {
				areaVO.setPending(0L);
				return areaVO;
			}

			List<TypeVO> typeVOs = areaTypes.stream().map(type -> {
				TypeVO typeVO = new TypeVO(type);

				List<Item> typeItems = typeIdOfItems.get(type.getId());
				if (CollUtil.isEmpty(typeItems)) {
					return typeVO;
				}

				List<ItemVO> itemVOs = typeItems.stream().map(ItemVO::new).collect(Collectors.toList());

				long typePending = itemVOs.stream().filter(vo -> Objects.nonNull(vo.getPending()) && vo.getPending() > 0).count();

				typeVO.setItems(itemVOs);
				typeVO.setPending(typePending);
				return typeVO;
			}).collect(Collectors.toList());

			long areaPending = typeVOs.stream().filter(vo -> Objects.nonNull(vo.getPending()) && vo.getPending() > 0).count();

			areaVO.setTypes(typeVOs);
			areaVO.setPending(areaPending);

			return areaVO;
		}).collect(Collectors.toList());
	}

}
