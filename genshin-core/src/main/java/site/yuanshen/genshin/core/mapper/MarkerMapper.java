package site.yuanshen.genshin.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import site.yuanshen.genshin.core.entity.Marker;
import site.yuanshen.genshin.core.vo.ItemMarkerVO;

import java.util.List;

@Mapper
public interface MarkerMapper extends BaseMapper<Marker> {
	/**
	 * 根据 item id 查询点位
	 *
	 * @param id item id
	 * @return 点位列表
	 */
	List<Marker> listByItemIdAndKeyword(@Param("id") Long id, @Param("visibility") Boolean visibility, @Param("keyword") String keyword);

	/**
	 * 查询全部点位（带选项ID）
	 *
	 * @return 点位列表
	 */
	List<ItemMarkerVO> listAllMarkerItem();
}