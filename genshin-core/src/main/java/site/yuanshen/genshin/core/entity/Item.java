package site.yuanshen.genshin.core.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

@ApiModel(value = "item")
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "item")
public class Item extends BaseEntity {
	private static final long serialVersionUID = 1L;
	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "id")
	private Long id;

	/**
	 * 名
	 */
	@TableField(value = "`name`")
	@ApiModelProperty(value = "名")
	private String name;

	/**
	 * 审核填充内容
	 */
	@TableField(value = "content")
	@ApiModelProperty(value = "审核填充内容")
	private String content;

	/**
	 * 类型
	 */
	@TableField(value = "type_id")
	@ApiModelProperty(value = "类型")
	private Long typeId;

	/**
	 * layer
	 */
	@TableField(value = "layer")
	@ApiModelProperty(value = "layer")
	private Long layer;

	/**
	 * 数量
	 */
	@TableField(value = "`count`")
	@ApiModelProperty(value = "数量")
	private Integer count;

	/**
	 * 地区id
	 */
	@TableField(value = "area_id")
	@ApiModelProperty(value = "地区id")
	private Long areaId;

	/**
	 * 图标
	 */
	@TableField(value = "icon")
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 是否显示 1显示 0不显示
	 */
	@TableField(value = "visibility")
	@ApiModelProperty(value = "是否显示 1显示 0不显示")
	private Boolean visibility;
	/**
	 * 代办
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "代办")
	private Long pending;
}