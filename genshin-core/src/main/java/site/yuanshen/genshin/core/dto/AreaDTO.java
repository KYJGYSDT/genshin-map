package site.yuanshen.genshin.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import site.yuanshen.genshin.common.multipart.Base64MultipartFile;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel(value = "区域DTO")
public class AreaDTO implements Serializable {
	private static final long serialVersionUID = 1L;


	/**
	 * 区域ID
	 */
	@ApiModelProperty(value = "区域ID，修改时不能为空")
	private Long areaId;

	/**
	 * 名
	 */
	@NotBlank(message = "区域名称不能为空")
	@ApiModelProperty(value = "名", required = true)
	private String name;

	/**
	 * 图标文件
	 */
	@ApiModelProperty(value = "图标文件Base64", dataType = "java.lang.String")
	private Base64MultipartFile iconBase64;

	/**
	 * 图标
	 */
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 修改次数
	 */
	@ApiModelProperty(value = "修改次数")
	private Long version;



	@ApiModelProperty(value = "区域父ID，一级的时候传0")
	private Long parentId;
}
