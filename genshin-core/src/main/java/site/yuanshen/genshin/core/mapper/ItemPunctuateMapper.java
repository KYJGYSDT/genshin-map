package site.yuanshen.genshin.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import site.yuanshen.genshin.core.entity.ItemPunctuate;

@Mapper
public interface ItemPunctuateMapper extends BaseMapper<ItemPunctuate> {
}