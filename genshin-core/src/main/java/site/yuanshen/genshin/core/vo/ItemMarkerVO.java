package site.yuanshen.genshin.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ItemMarkerVO extends MarkerVO {

	/**
	 * 选项ID
	 */
	@ApiModelProperty(value = "选项ID")
	private Long itemId;

}
