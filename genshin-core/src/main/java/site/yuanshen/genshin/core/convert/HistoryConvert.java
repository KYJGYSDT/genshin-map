package site.yuanshen.genshin.core.convert;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.collect.Lists;
import lombok.SneakyThrows;
import org.apache.commons.lang3.tuple.Pair;
import site.yuanshen.genshin.common.exception.ParamNotSupportException;
import site.yuanshen.genshin.core.entity.*;
import site.yuanshen.genshin.core.enums.HistoryType;

import java.util.List;

public class HistoryConvert {
	private static final JsonMapper MAPPER = new JsonMapper();

	static {
		MAPPER.registerModule(new JavaTimeModule());
		MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
	}


	private static final List<Convert> converts = Lists.newArrayList(
			new AreaConvert(), new TypeConvert(), new ItemConvert(), new MarkerConvert()
	);

	public static History convert(Object source) {
		return converts.stream().filter(convert -> convert.support(source)).findAny()
				.orElseThrow(() -> new ParamNotSupportException(source.getClass() + " not support")).convert(source);
	}

	public static Object reConvert(History history, HistoryType type) {
		return converts.stream().filter(convert -> convert.support(type)).findAny()
				.orElseThrow(() -> new ParamNotSupportException(type + " not support")).reConvert(history);
	}


	private static class AreaConvert extends DefaultConvert {
		private static final JsonMapper mapper = MAPPER;

		@Override
		public boolean support(Object o) {
			return o instanceof Area || (o instanceof HistoryType && getType().equals(o));
		}

		@Override
		@SneakyThrows
		public Object reConvert(History history) {
			return mapper.readValue(history.getContent(), Area.class);
		}

		@Override
		HistoryType getType() {
			return HistoryType.AREA;
		}

		@Override
		@SneakyThrows
		Pair<String, Long> getContentAndId(Object o) {
			Area area = (Area) o;
			area.setVersion(null);
			String content = mapper.writeValueAsString(area);
			Long id = area.getId();
			return Pair.of(content, id);
		}
	}

	private static class TypeConvert extends DefaultConvert {
		private static final JsonMapper mapper = MAPPER;

		@Override
		public boolean support(Object o) {
			return o instanceof Type || (o instanceof HistoryType && getType().equals(o));
		}

		@Override
		@SneakyThrows
		public Object reConvert(History history) {
			return mapper.readValue(history.getContent(), Type.class);
		}

		@Override
		HistoryType getType() {
			return HistoryType.TYPE;
		}

		@Override
		@SneakyThrows
		Pair<String, Long> getContentAndId(Object o) {
			Type type = (Type) o;
			type.setVersion(null);
			String content = mapper.writeValueAsString(type);
			Long id = type.getId();
			return Pair.of(content, id);
		}
	}

	private static class ItemConvert extends DefaultConvert {
		private static final JsonMapper mapper = MAPPER;

		@Override
		public boolean support(Object o) {
			return o instanceof Item || (o instanceof HistoryType && getType().equals(o));
		}

		@Override
		@SneakyThrows
		public Object reConvert(History history) {
			return mapper.readValue(history.getContent(), Item.class);
		}

		@Override
		HistoryType getType() {
			return HistoryType.ITEM;
		}

		@Override
		@SneakyThrows
		Pair<String, Long> getContentAndId(Object o) {
			Item item = (Item) o;
			item.setVersion(null);
			String content = mapper.writeValueAsString(item);
			Long id = item.getId();
			return Pair.of(content, id);
		}
	}

	private static class MarkerConvert extends DefaultConvert {
		private static final JsonMapper mapper = MAPPER;

		@Override
		public boolean support(Object o) {
			return o instanceof Marker || (o instanceof HistoryType && getType().equals(o));
		}

		@Override
		@SneakyThrows
		public Object reConvert(History history) {
			return mapper.readValue(history.getContent(), Marker.class);
		}

		@Override
		HistoryType getType() {
			return HistoryType.MARKER;
		}

		@Override
		@SneakyThrows
		Pair<String, Long> getContentAndId(Object o) {
			Marker marker = (Marker) o;
			marker.setVersion(null);
			String content = mapper.writeValueAsString(marker);
			Long id = marker.getId();
			return Pair.of(content, id);
		}
	}


}
