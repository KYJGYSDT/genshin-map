package site.yuanshen.genshin.core.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

@ApiModel(value = "history")
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "history")
public class History extends BaseEntity {
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "")
	private Long id;

	/**
	 * 内容
	 */
	@TableField(value = "content")
	@ApiModelProperty(value = "内容")
	private String content;

	/**
	 * md5
	 */
	@TableField(value = "md5")
	@ApiModelProperty(value = "md5")
	private String md5;

	/**
	 * 类型id
	 */
	@TableField(value = "t_id")
	@ApiModelProperty(value = "类型id")
	private Long tId;

	/**
	 * 记录类型
	 */
	@TableField(value = "`type`")
	@ApiModelProperty(value = "记录类型")
	private Integer type;

	/**
	 * ipv4
	 */
	@TableField(value = "ipv4")
	@ApiModelProperty(value = "ipv4")
	private String ipv4;
}