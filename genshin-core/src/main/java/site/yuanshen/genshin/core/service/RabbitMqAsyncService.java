package site.yuanshen.genshin.core.service;

import org.springframework.beans.factory.annotation.Value;

/**
 * 使用RabbitMQ的异步消息推送服务 接口
 * @author Moment
 */
public interface RabbitMqAsyncService {

	/**
	 * @param message 消息内容
	 * @param methodName 方法名(用于在日志中区分不同的方法)
	 */
	void distributePunctuate(String message, String methodName);

}
