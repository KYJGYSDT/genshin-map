package site.yuanshen.genshin.core.controller;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.common.exception.CheckException;
import site.yuanshen.genshin.core.dto.BatchPunctuateDTO;
import site.yuanshen.genshin.core.dto.PunctuateDTO;
import site.yuanshen.genshin.core.service.PunctuateService;
import site.yuanshen.genshin.core.service.RabbitMqAsyncService;
import site.yuanshen.genshin.core.vo.PunctuateVO;

import javax.validation.Valid;
import java.util.List;

/**
 * 用户标点
 *
 * @author :  Hu
 */
@RestController
@AllArgsConstructor
@RequestMapping("/punctuate")
@Api(value = "punctuate", tags = "用户标点API")
public class PunctuateController {
	private final PunctuateService punctuateService;
	private final RabbitMqAsyncService mqService;

	/**
	 * 用户对点位的增删查改
	 *
	 * @return R
	 */
	@ApiOperation(value = "标点", notes = "用户对点位的增删查改")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('punctuate_add')")
	public R<Boolean> punctuate(@RequestBody @Valid BatchPunctuateDTO batchPunctuateDTO) {
		R<Boolean> r = R.ok(punctuateService.punctuate(batchPunctuateDTO));
		mqService.distributePunctuate(JSON.toJSONString(batchPunctuateDTO),"用户对点位的增删查改");
		return r;
	}

	/**
	 * 用户标点列表查询
	 *
	 * @return R
	 */
	@ApiOperation(value = "用户标点列表查询", notes = "用户标点列表查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "itemId", required = true, value = "选项ID", paramType = "path"),
	})
	@GetMapping("/{itemId}")
	@PreAuthorize("@pms.hasPermission('punctuate_add')")
	public R<List<PunctuateVO>> list(@PathVariable Long itemId, @RequestParam(required = false) String keyword) {
		return R.ok(punctuateService.listOwnByItemId(itemId, keyword));
	}

	/**
	 * 用户对标点修改
	 *
	 * @return R
	 */
	@ApiOperation(value = "用户对标点修改", notes = "用户对标点修改")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "punctuateId", required = true, value = "标点", paramType = "path"),
	})
	@PostMapping
	@PreAuthorize("@pms.hasPermission('punctuate_add')")
	public R<Boolean> edit(@RequestBody PunctuateDTO punctuateDTO) {
		Assert.notNull(punctuateDTO.getPunctuateId(), () -> new CheckException("原标点记录ID不能为空"));
		R<Boolean> r = R.ok(punctuateService.edit(punctuateDTO));
		mqService.distributePunctuate(JSON.toJSONString(punctuateDTO),"用户对标点修改");
		return r;
	}

}
