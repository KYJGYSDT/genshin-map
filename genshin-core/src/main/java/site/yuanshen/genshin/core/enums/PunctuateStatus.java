package site.yuanshen.genshin.core.enums;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import site.yuanshen.genshin.common.exception.ParamNotSupportException;

import java.util.Arrays;

@AllArgsConstructor
public enum PunctuateStatus {

	WAIT_AUDIT(0),
	AUDIT_PASS(1),
	AUDIT_REJECT(2),
	;

	@Getter
	private final Integer value;

	public static PunctuateStatus from(Integer value) {
		return Arrays.stream(values()).filter(type -> type.getValue().equals(value)).findAny().orElseThrow(() -> new ParamNotSupportException("状态[ " + value + " ]不在可选范围内"));
	}

	public static Boolean couldEdit(Integer value) {
		return Lists.newArrayList(WAIT_AUDIT, AUDIT_REJECT).stream().map(PunctuateStatus::getValue).anyMatch(t -> t.equals(value));
	}
}
