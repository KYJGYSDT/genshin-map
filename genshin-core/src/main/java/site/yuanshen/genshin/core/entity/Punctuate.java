package site.yuanshen.genshin.core.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

/**
 * 用户打点
 */
@ApiModel(value = "用户打点")
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "punctuate")
public class Punctuate extends BaseEntity {
	private static final long serialVersionUID = 1L;
	/**
	 * ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "ID")
	private Long id;

	/**
	 * 点位ID
	 */
	@TableField(value = "marker_id")
	@ApiModelProperty(value = "点位ID")
	private Long markerId;

	/**
	 * 标点内容
	 */
	@TableField(value = "content")
	@ApiModelProperty(value = "标点内容")
	private String content;

	/**
	 * 点位名称
	 */
	@TableField(value = "`name`")
	@ApiModelProperty(value = "点位名称")
	private String name;

	/**
	 * 图片地址
	 */
	@TableField(value = "`resource`")
	@ApiModelProperty(value = "图片地址")
	private String resource;

	/**
	 * 坐标
	 */
	@TableField(value = "`position`")
	@ApiModelProperty(value = "坐标")
	private String position;

	/**
	 * 刷新时间 小时
	 */
	@TableField(value = "`time`")
	@ApiModelProperty(value = "刷新时间 小时")
	private Integer time;

	/**
	 * 状态
	 */
	@TableField(value = "`status`")
	@ApiModelProperty(value = "状态")
	private Integer status;

	/**
	 * 审核备注
	 */
	@TableField(value = "audit_remark")
	@ApiModelProperty(value = "审核备注")
	private String auditRemark;

	/**
	 * 操作类型 1: 新增 2: 修改 3: 删除
	 */
	@TableField(value = "option_type")
	@ApiModelProperty(value = "操作类型 1: 新增 2: 修改 3: 删除")
	private Integer optionType;
}