package site.yuanshen.genshin.core.dto;


import lombok.Data;

@Data
public class ItemMarkerDTO {

	private Long ItemId;

	private Long markerId;
}
