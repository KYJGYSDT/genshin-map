package site.yuanshen.genshin.core.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import site.yuanshen.genshin.core.entity.Type;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * 品类
 *
 * @author :  Hu
 */
@Data
@NoArgsConstructor
public class TypeVO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@ApiModelProperty(value = "ID")
	private Long id;

	/**
	 * 名
	 */
	@ApiModelProperty(value = "名")
	private String name;

	/**
	 * 图标
	 */
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 待办
	 */
	@ApiModelProperty(value = "待办")
	private Long pending;

	/**
	 * 是否显示
	 */
	@ApiModelProperty(value = "是否显示")
	private Boolean visibility;

	/**
	 * 修改次数
	 */
	@ApiModelProperty(value = "修改次数")
	private Long version;

	/**
	 * 条目
	 */
	@ApiModelProperty(value = "条目列表")
	private List<ItemVO> items;

	public TypeVO(Type type) {
		if (Objects.nonNull(type)) {
			BeanUtils.copyProperties(type, this);
		}
	}
}
