package site.yuanshen.genshin.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.core.entity.ItemPunctuate;

import java.util.List;

public interface ItemPunctuateService extends IService<ItemPunctuate> {

	/**
	 * 批量更新Item Punctuate 关系
	 *
	 * @param itemPunctuates Item Punctuate 关系
	 * @return Boolean
	 */
	Boolean batchUpdate(List<ItemPunctuate> itemPunctuates);
}
