package site.yuanshen.genshin.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import site.yuanshen.genshin.core.entity.ItemMarker;

@Mapper
public interface ItemMarkerMapper extends BaseMapper<ItemMarker> {
}