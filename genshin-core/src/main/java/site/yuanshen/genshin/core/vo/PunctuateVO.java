package site.yuanshen.genshin.core.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import site.yuanshen.genshin.core.entity.Punctuate;

import java.io.Serializable;
import java.util.Objects;

/**
 * 用户上报点位信息
 *
 * @author :  Hu
 */
@Data
@NoArgsConstructor
public class PunctuateVO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@ApiModelProperty(value = "ID")
	private Long id;

	/**
	 * 点位ID
	 */
	@TableField(value = "marker_id")
	@ApiModelProperty(value = "点位ID")
	private Long markerId;

	/**
	 * 标点内容
	 */
	@ApiModelProperty(value = "标点内容")
	private String content;

	/**
	 * 点位名称
	 */
	@ApiModelProperty(value = "点位名称")
	private String name;

	/**
	 * 图片地址
	 */
	@ApiModelProperty(value = "图片地址")
	private String resource;

	/**
	 * 坐标
	 */
	@ApiModelProperty(value = "坐标")
	private String position;

	/**
	 * 刷新时间
	 */
	@ApiModelProperty(value = "刷新时间")
	private Integer time;

	/**
	 * 状态
	 */
	@ApiModelProperty(value = "状态 0:待审核 1:审核通过 2:审核拒绝")
	private Integer status;

	/**
	 * 审核备注
	 */
	@ApiModelProperty(value = "审核备注")
	private String auditRemark;

	/**
	 * 图标
	 */
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 打点人ID
	 */
	@ApiModelProperty(value = "打点人ID")
	private Long creatorId;

	/**
	 * 操作类型 1: 新增 2: 修改 3: 删除
	 */
	@ApiModelProperty(value = "操作类型 1: 新增 2: 修改 3: 删除")
	private Integer optionType;

	/**
	 * 修改次数
	 */
	@ApiModelProperty(value = "修改次数")
	private Long version;

	public PunctuateVO(Punctuate target) {
		if (Objects.nonNull(target)) {
			BeanUtils.copyProperties(target, this);
		}
	}
}
