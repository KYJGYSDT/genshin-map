package site.yuanshen.genshin.core.manager;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import site.yuanshen.genshin.core.dto.PunctuateAuditDTO;
import site.yuanshen.genshin.core.entity.Punctuate;
import site.yuanshen.genshin.core.enums.PunctuateOptionType;
import site.yuanshen.genshin.core.enums.PunctuateStatus;
import site.yuanshen.genshin.core.service.MarkerService;
import site.yuanshen.genshin.core.service.PunctuateService;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class PunctuateManager {
	private final PunctuateService punctuateService;
	private final MarkerService markerService;

	@Transactional
	public Boolean pass(PunctuateAuditDTO punctuateAuditDTO) {
		List<Punctuate> punctuates = punctuateService.listWaitAuditPunctuate(punctuateAuditDTO.getPunctuateIds());

		punctuates.stream().collect(Collectors.groupingBy(Punctuate::getOptionType)).forEach((optionType, punctuateList) -> {
			PunctuateOptionType punctuateOptionType = PunctuateOptionType.from(optionType);

			switch (punctuateOptionType) {
				case INSERT:
					markerService.saveMarker(punctuateList);
					break;
				case UPDATE:
					markerService.updateMarker(punctuateList);
					break;
				case DELETE:
					markerService.removeMarker(punctuateList);
					break;
			}
		});

		return punctuateService.audit(punctuateAuditDTO.getPunctuateIds(), PunctuateStatus.AUDIT_PASS, punctuateAuditDTO.getAuditRemark());
	}
}
