package site.yuanshen.genshin.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.core.entity.ItemMarker;

import java.util.List;

public interface ItemMarkerService extends IService<ItemMarker> {

	/**
	 * 批量更新点位选项关系表
	 *
	 * @param itemMarkers 点位选项关系
	 * @return Boolean
	 */
	Boolean batchUpdate(List<ItemMarker> itemMarkers);

}
