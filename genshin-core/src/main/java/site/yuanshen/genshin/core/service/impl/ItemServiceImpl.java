package site.yuanshen.genshin.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.common.constant.CommonConstant;
import site.yuanshen.genshin.common.exception.DataObsoleteException;
import site.yuanshen.genshin.common.exception.ExecuteFailException;
import site.yuanshen.genshin.common.exception.ResourceNotExistException;
import site.yuanshen.genshin.common.minio.MinioTemplate;
import site.yuanshen.genshin.common.util.FileUtil;
import site.yuanshen.genshin.core.convert.HistoryConvert;
import site.yuanshen.genshin.core.dto.ItemDTO;
import site.yuanshen.genshin.core.entity.Item;
import site.yuanshen.genshin.core.entity.Type;
import site.yuanshen.genshin.core.mapper.ItemMapper;
import site.yuanshen.genshin.core.service.HistoryService;
import site.yuanshen.genshin.core.service.ItemService;
import site.yuanshen.genshin.core.service.TypeService;
import site.yuanshen.genshin.core.vo.ItemVO;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 选项
 *
 * @author Hu
 * @date 2021-06-08 19:21:23
 */
@Service
@AllArgsConstructor
public class ItemServiceImpl extends ServiceImpl<ItemMapper, Item> implements ItemService {
	private final TypeService typeService;
	private final CacheManager cacheManager;
	private final MinioTemplate minioTemplate;
	private final HistoryService historyService;

	@Override
	@Cacheable(value = CacheConstant.ITEM_VISIBILITY, key = "'all'", unless = "#result == null || #result.isEmpty()")
	public List<Item> findAll() {
		return baseMapper.selectList(Wrappers.<Item>lambdaQuery().eq(Item::getVisibility, 1));
	}

	@Override
	public List<Item> findAllVerify() {
		return baseMapper.selectVerifyList(null);
	}

	@Override
	@Cacheable(value = CacheConstant.ALL_ITEM, key = "#typeId", unless = "#result == null || #result.isEmpty()")
	public List<ItemVO> voListByTypeId(Long typeId) {
		List<Item> items = baseMapper.selectList(Wrappers.<Item>lambdaQuery().eq(Item::getTypeId, typeId));
		return items.stream().map(ItemVO::new).collect(Collectors.toList());
	}

	@Override
	@CacheEvict(value = CacheConstant.ALL_ITEM, key = "#itemDTO.typeId")
	public Boolean saveItem(ItemDTO itemDTO) {
		Type type = typeService.getById(itemDTO.getTypeId());
		if (Objects.isNull(type)) {
			throw new ResourceNotExistException("类型不存在");
		}
		MultipartFile file = itemDTO.getIconBase64();
		FileUtil.limitImgFile(file);

		Item item = new Item();
		BeanUtils.copyProperties(itemDTO, item);
		item.setIcon(minioTemplate.uploadFile(file));
		item.setAreaId(type.getAreaId());
		item.setVersion(null);

		if (!SqlHelper.retBool(baseMapper.insert(item))) {
			throw new ExecuteFailException("新增失败");
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	@Caching(evict = {
			@CacheEvict(value = CacheConstant.ALL_ITEM, key = "#itemDTO.typeId"),
			@CacheEvict(value = CacheConstant.ITEM_VISIBILITY, key = "'all'")
	})
	public Boolean updateItem(ItemDTO itemDTO) {
		Item item = baseMapper.selectById(itemDTO.getItemId());
		if (Objects.isNull(item)) {
			throw new ResourceNotExistException("选项不存在");
		}

		if (!Objects.equals(item.getVersion(), itemDTO.getVersion())) {
			throw new DataObsoleteException("数据已被修改");
		}

		historyService.save(HistoryConvert.convert(item));

		BeanUtils.copyProperties(itemDTO, item);

		MultipartFile file = itemDTO.getIconBase64();
		if (Objects.nonNull(file)) {
			FileUtil.limitImgFile(file);
			item.setIcon(minioTemplate.uploadFile(file));
		}

		if (!SqlHelper.retBool(baseMapper.updateById(item))) {
			throw new ExecuteFailException("更新失败");
		}

		return Boolean.TRUE;
	}

	@Override
	@Transactional
	@CacheEvict(value = CacheConstant.ALL_ITEM, key = "#id")
	public Boolean deleteItem(Long id) {
		Item item = baseMapper.selectById(id);
		if (Objects.isNull(item)) {
			throw new ResourceNotExistException("选项不存在");
		}
		historyService.save(HistoryConvert.convert(item));

		if (Objects.equals(item.getVisibility(), CommonConstant.VISIBILITY)) {
			cacheManager.getCache(CacheConstant.ITEM_VISIBILITY).evict(item.getTypeId());
		}

		if (!SqlHelper.retBool(baseMapper.deleteById(id))) {
			throw new ExecuteFailException("删除失败");
		}

		return Boolean.TRUE;
	}

	@Override
	public Boolean switchItem(Long id) {
		Item item = baseMapper.selectById(id);
		if (Objects.isNull(item)) {
			throw new ResourceNotExistException("选项不存在");
		}

		UpdateWrapper<Item> wrapper = new UpdateWrapper<>();
		wrapper.setSql("visibility = case visibility when 1 then 0 else 1 end").eq("id", id);

		if (!SqlHelper.retBool(baseMapper.update(null, wrapper))) {
			throw new ExecuteFailException("修改失败");
		}

		cacheManager.getCache(CacheConstant.ALL_ITEM).evict(item.getTypeId());
		cacheManager.getCache(CacheConstant.ITEM_VISIBILITY).evict("all");

		return Boolean.TRUE;
	}

	@Override
	public List<Item> findByKeyword(String keyword) {
		return baseMapper.selectList(Wrappers.<Item>lambdaQuery().like(Item::getName, keyword));
	}

	@Override
	public List<Item> findVerifyByKeyword(String keyword) {
		return baseMapper.selectVerifyList(keyword);
	}

	@Override
	@Transactional
	@Caching(evict = {
			@CacheEvict(value = CacheConstant.ALL_ITEM, key = "#item.typeId"),
			@CacheEvict(value = CacheConstant.ITEM_VISIBILITY, key = "'all'")
	})
	public Boolean update(Item item) {
		historyService.save(HistoryConvert.convert(item));

		if (!SqlHelper.retBool(baseMapper.updateById(item))) {
			throw new ExecuteFailException("更新失败");
		}

		return Boolean.TRUE;
	}

	@Override
	public Item findByMarkerId(Long markerId) {
		return baseMapper.selectByMarkerId(markerId);
	}
}
