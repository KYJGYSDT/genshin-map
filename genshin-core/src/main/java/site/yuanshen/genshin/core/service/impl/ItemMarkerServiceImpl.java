package site.yuanshen.genshin.core.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.yuanshen.genshin.common.exception.ExecuteFailException;
import site.yuanshen.genshin.core.entity.ItemMarker;
import site.yuanshen.genshin.core.mapper.ItemMarkerMapper;
import site.yuanshen.genshin.core.service.ItemMarkerService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ItemMarkerServiceImpl extends ServiceImpl<ItemMarkerMapper, ItemMarker> implements ItemMarkerService {

	@Override
	@Transactional
	public Boolean batchUpdate(List<ItemMarker> itemMarkers) {
		List<Long> markerIds = itemMarkers.stream().map(ItemMarker::getMarkerId).collect(Collectors.toList());

		if (!SqlHelper.retBool(baseMapper.delete(Wrappers.<ItemMarker>lambdaQuery().in(ItemMarker::getMarkerId, markerIds)))) {
			throw new ExecuteFailException("更新失败");
		}

		if (!this.saveBatch(itemMarkers)) {
			throw new ExecuteFailException("更新失败");
		}

		return Boolean.TRUE;
	}
}
