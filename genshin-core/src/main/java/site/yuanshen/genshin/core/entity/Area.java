package site.yuanshen.genshin.core.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

@ApiModel(value = "area")
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "area")
public class Area extends BaseEntity {
	private static final long serialVersionUID = 1L;
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "")
	private Long id;
	@TableField(value = "parent_id")
	@ApiModelProperty(value = "父id")
	private Long parentId;

	/**
	 * 地区名
	 */
	@TableField(value = "`name`")
	@ApiModelProperty(value = "地区名")
	private String name;

	/**
	 * 图标
	 */
	@TableField(value = "icon")
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 是否显示 1显示 0不显示
	 */
	@TableField(value = "visibility")
	@ApiModelProperty(value = "是否显示 1显示 0不显示")
	private Boolean visibility;
}