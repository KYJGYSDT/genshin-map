package site.yuanshen.genshin.core.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import site.yuanshen.genshin.common.multipart.Base64MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "类型DTO")
public class TypeDTO {

	/**
	 * 类型ID
	 */
	@ApiModelProperty(value = "类型ID，修改时不能为空")
	private Long typeId;

	/**
	 * 区域ID
	 */
	@NotNull(message = "区域ID不能为空")
	@ApiModelProperty(value = "区域ID")
	private Long areaId;

	/**
	 * 名
	 */
	@NotBlank(message = "区域名称不能为空")
	@ApiModelProperty(value = "名", required = true)
	private String name;

	/**
	 * 图标文件
	 */
	@ApiModelProperty(value = "图标文件Base64", dataType = "java.lang.String")
	private Base64MultipartFile iconBase64;

	/**
	 * 图标
	 */
	@ApiModelProperty(value = "图标")
	private String icon;

	/**
	 * 修改次数
	 */
	@ApiModelProperty(value = "修改次数")
	private Long version;
}
