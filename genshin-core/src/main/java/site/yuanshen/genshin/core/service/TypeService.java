package site.yuanshen.genshin.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.core.dto.TypeDTO;
import site.yuanshen.genshin.core.entity.Type;
import site.yuanshen.genshin.core.vo.TypeVO;

import java.util.List;

/**
 * 类型
 *
 * @author Hu
 * @date 2021-06-08 19:21:23
 */
public interface TypeService extends IService<Type> {

	/**
	 * 查询所有可显示的类型
	 *
	 * @return List
	 */
	List<Type> findAllVisibility();

	/**
	 * 查询所有类型
	 *
	 * @param areaId 区域ID
	 * @return List
	 */
	List<TypeVO> voListByAreaId(Long areaId);

	/**
	 * 新增类型
	 *
	 * @param typeDTO 类型DTO
	 * @return Boolean
	 */
	Boolean saveType(TypeDTO typeDTO);

	/**
	 * 修改类型
	 *
	 * @param typeDTO 类型DTO
	 * @return Boolean
	 */
	Boolean updateType(TypeDTO typeDTO);

	/**
	 * 删除类型
	 *
	 * @param id 类型ID
	 * @return Boolean
	 */
	Boolean deleteType(Long id);

	/**
	 * 启停类型
	 *
	 * @param id 类型ID
	 * @return Boolean
	 */
	Boolean switchType(Long id);

	/**
	 * 根据id查询类型
	 *
	 * @param typeIds typeIds
	 * @return 类型List
	 */
	List<Type> findVisibilityByIds(List<Long> typeIds);

	/**
	 * 修改类型
	 *
	 * @param type 类型
	 * @return Boolean
	 */
	Boolean update(Type type);
}
