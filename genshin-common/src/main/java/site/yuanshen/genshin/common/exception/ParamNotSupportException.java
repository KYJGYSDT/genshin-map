package site.yuanshen.genshin.common.exception;

import org.springframework.http.HttpStatus;

public class ParamNotSupportException extends BaseException {
	public ParamNotSupportException(String message) {
		super(message);
	}

	public ParamNotSupportException(HttpStatus status, String message) {
		super(status, message);
	}
}
