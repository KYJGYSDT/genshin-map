package site.yuanshen.genshin.common.i18n;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@Component("localeResolver")
public class SmartLocaleResolver extends AcceptHeaderLocaleResolver {
	private final List<Locale> LOCALES = Arrays.asList(Locale.CHINA, Locale.TAIWAN, Locale.ENGLISH, Locale.JAPANESE);

	public SmartLocaleResolver() {
		super.setDefaultLocale(Locale.CHINA);
	}

	@Override
	public Locale resolveLocale(HttpServletRequest request) {
		Locale defaultLocale = getDefaultLocale();

		String lang = request.getHeader("lang");

		if (StringUtils.isBlank(lang)) {
			return defaultLocale;
		}
		List<Locale.LanguageRange> list = Locale.LanguageRange.parse(lang);

		Locale lookup = Locale.lookup(list, LOCALES);
		if (Objects.isNull(lookup)) {
			return defaultLocale;
		}

		return lookup;
	}
}