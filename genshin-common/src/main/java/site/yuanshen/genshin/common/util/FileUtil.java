package site.yuanshen.genshin.common.util;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.springframework.web.multipart.MultipartFile;
import site.yuanshen.genshin.common.exception.CheckException;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;


public class FileUtil {
	private static final Joiner joiner = Joiner.on(", ").skipNulls();

	public static final String IMG_TYPE_PNG = "PNG";
	public static final String IMG_TYPE_JPG = "JPG";
	public static final String IMG_TYPE_JPEG = "JPEG";
	public static final String IMG_TYPE_DMG = "BMP";
	public static final String IMG_TYPE_GIF = "GIF";
	public static final String IMG_TYPE_SVG = "SVG";

	public static final List<String> IMG_TYPE = Lists.newArrayList(IMG_TYPE_PNG, IMG_TYPE_JPG, IMG_TYPE_JPEG, IMG_TYPE_DMG,
			IMG_TYPE_GIF, IMG_TYPE_SVG);

	public static void limitImgFile(final MultipartFile file) {
		String suffix = FileUtil.getSuffix(file.getOriginalFilename());

		if (IMG_TYPE.stream().noneMatch(suffix::equalsIgnoreCase)) {
			throw new CheckException("仅支持 " + joiner.join(IMG_TYPE) + " 类型的文件");
		}

	}

	/**
	 * 获取文件后缀
	 *
	 * @param fileName 文件名
	 * @return String
	 */
	public static String getSuffix(String fileName) {
		if (fileName == null) {
			throw new CheckException("非法文件名称");
		}
		int index = fileName.lastIndexOf(".");
		if (index != -1) {
			String suffix = fileName.substring(index + 1);
			if (!suffix.isEmpty()) {
				return suffix;
			}
		}
		throw new CheckException("非法文件名称：" + fileName);
	}

	/**
	 * 获取文件名
	 *
	 * @param object 存储桶中的对象名称
	 * @return String
	 */
	public static String getFileName(String object) {
		if (object == null || "".equals(object)) {
			throw new CheckException("非法文件名称");
		}
		String[] a = object.split("/");
		return a[a.length - 1];
	}

	/**
	 * 获取年月日[2020, 09, 01]
	 *
	 * @return String
	 */
	public static String[] getDateFolder() {
		return getDateFolder(null);
	}

	/**
	 * 获取文件夹开头的年月日[folder,2020, 09, 01]
	 *
	 * @param folder
	 * @return
	 */
	public static String[] getDateFolder(String folder) {
		String[] retVal = new String[3];
		LocalDate localDate = LocalDate.now();
		retVal[0] = localDate.getYear() + "";
		int month = localDate.getMonthValue();
		retVal[1] = month < 10 ? "0" + month : month + "";
		int day = localDate.getDayOfMonth();
		retVal[2] = day < 10 ? "0" + day : day + "";
		String[] newRetVal = null;
		if (!(folder == null || "".equals(folder))) {
			newRetVal = Arrays.copyOf(retVal, retVal.length + 1);
			System.arraycopy(newRetVal, 0, newRetVal, 1, newRetVal.length - 1);
			newRetVal[0] = folder;
		}
		return newRetVal == null ? retVal : newRetVal;
	}
}