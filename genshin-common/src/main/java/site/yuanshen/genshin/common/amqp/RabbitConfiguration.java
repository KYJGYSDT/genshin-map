package site.yuanshen.genshin.common.amqp;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * 消息队列配置
 *
 * @author Moment
 */
@RequiredArgsConstructor
@Configuration
@EnableConfigurationProperties(QueueProperties.class)
public class RabbitConfiguration {

	private final QueueProperties queueProperties;

	@Bean
	public Queue boardCastQueue() {
		return new Queue(queueProperties.getDistributePunctuate(), false, false, false);
	}

}
