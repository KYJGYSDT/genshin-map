package site.yuanshen.genshin.common.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.http.HttpStatus;
import site.yuanshen.genshin.common.security.RestOAuth2ExceptionSerializer;

@JsonSerialize(using = RestOAuth2ExceptionSerializer.class)
public class OAuth2ForbiddenException extends OAuth2BaseException {

	public OAuth2ForbiddenException(String msg) {
		super(msg);
	}

	public OAuth2ForbiddenException(String msg, Throwable t) {
		super(msg, t);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "access_denied";
	}

	@Override
	public int getHttpErrorCode() {
		return HttpStatus.FORBIDDEN.value();
	}

}
