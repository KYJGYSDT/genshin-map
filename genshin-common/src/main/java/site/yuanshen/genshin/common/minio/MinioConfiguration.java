package site.yuanshen.genshin.common.minio;

import io.minio.MinioClient;
import lombok.AllArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
@EnableConfigurationProperties(MinioProperties.class)
public class MinioConfiguration {
	private final MinioProperties minioProperties;

	/**
	 * 获取MinioClient
	 */
	@Bean
	public MinioClient minioClient() {
		return MinioClient.builder()
				.endpoint(minioProperties.getEndpoint())
				.credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey())
				.build();
	}

	@Bean
	public MinioTemplate miniTemplate() {
		return new MinioTemplate(minioProperties, minioClient());
	}

}