package site.yuanshen.genshin.common.swagger;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.util.ClassUtils;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
public class SwaggerConfig {
	Class<?> sysTimestampController = Class.forName("site.yuanshen.genshin.system.controller.SysTimestampController");

	public SwaggerConfig() throws ClassNotFoundException {
	}

	@Bean
	public Docket all_api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo("V1.0"))
				.groupName("全部API")
				.enable(true)
				.select()
				.apis(RequestHandlerSelectors.basePackage("site.yuanshen.genshin"))
				.build();
	}

	@Bean
	public Docket map_api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo("V2.0"))
				.groupName("地图API")
				.enable(true)
				.select()
				.apis(input -> {
					Class<?> declaringClass = input.declaringClass();
					return declaringClass == sysTimestampController || ClassUtils.getPackageName(declaringClass).startsWith("site.yuanshen.genshin.core");
				}).build();
	}


	@Bean
	public Docket system_api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo("V2.0"))
				.groupName("系统管理API")
				.enable(true)
				.select()
				.apis(input -> {
					Class<?> declaringClass = input.declaringClass();
					return declaringClass != sysTimestampController && ClassUtils.getPackageName(declaringClass).startsWith("site.yuanshen.genshin.system");
				}).build();
	}

	public ApiInfo apiInfo(String version) {
		Contact contact = new Contact("", "", "");
		return new ApiInfo("yuanshen.site swagger api", "", version, "", contact, "", "", Lists.newArrayList());
	}
}