package site.yuanshen.genshin.common.security;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Data
@EqualsAndHashCode(callSuper = true)
public class SocialSecurityConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
	private GenshinUserDetailService userServiceImpl;
	private AuthenticationEntryPoint authenticationEntryPoint;
	private AuthenticationSuccessHandler socialLoginSuccessHandler;

	@Override
	public void configure(HttpSecurity http) {
		SocialAuthenticationFilter socialAuthenticationFilter = new SocialAuthenticationFilter();
		socialAuthenticationFilter.setAuthenticationManager(http.getSharedObject(AuthenticationManager.class));
		socialAuthenticationFilter.setAuthenticationEntryPoint(authenticationEntryPoint);
		socialAuthenticationFilter.setAuthenticationSuccessHandler(socialLoginSuccessHandler);

		SocialAuthenticationProvider socialAuthenticationProvider = new SocialAuthenticationProvider();
		socialAuthenticationProvider.setUserDetailsService(userServiceImpl);

		http.authenticationProvider(socialAuthenticationProvider)
				.addFilterAfter(socialAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
	}

}
