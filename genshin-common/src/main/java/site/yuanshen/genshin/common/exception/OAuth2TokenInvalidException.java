package site.yuanshen.genshin.common.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.http.HttpStatus;
import site.yuanshen.genshin.common.security.RestOAuth2ExceptionSerializer;

@JsonSerialize(using = RestOAuth2ExceptionSerializer.class)
public class OAuth2TokenInvalidException extends OAuth2BaseException {

	public OAuth2TokenInvalidException(String msg, Throwable t) {
		super(msg);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "invalid_token";
	}

	@Override
	public int getHttpErrorCode() {
		return HttpStatus.UNAUTHORIZED.value();
	}

}
