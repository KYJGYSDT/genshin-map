package site.yuanshen.genshin.common.security;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import site.yuanshen.genshin.common.constant.SecurityConstant;

import java.util.HashMap;
import java.util.Map;

public class TokenEnhancer implements org.springframework.security.oauth2.provider.token.TokenEnhancer {

	/**
	 * Provides an opportunity for customization of an access token (e.g. through its
	 * additional information map) during the process of creating a new token for use by a
	 * client.
	 *
	 * @param accessToken    the current access token with its expiration and refresh token
	 * @param authentication the current authentication including client and user details
	 * @return a new token enhanced with additional information
	 */
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		if (SecurityConstant.CLIENT_CREDENTIALS.equals(authentication.getOAuth2Request().getGrantType())) {
			return accessToken;
		}

		final Map<String, Object> additionalInfo = new HashMap<>(8);
		GenshinUser user = (GenshinUser) authentication.getUserAuthentication().getPrincipal();
		additionalInfo.put(SecurityConstant.DETAILS_USER, user);
		additionalInfo.put(SecurityConstant.DETAILS_LICENSE, SecurityConstant.GENSHIN_LICENSE);
		additionalInfo.put(SecurityConstant.ACTIVE, Boolean.TRUE);
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
		return accessToken;
	}

}
