package site.yuanshen.genshin.common.minio;

import io.minio.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.multipart.MultipartFile;
import site.yuanshen.genshin.common.util.FileUtil;
import site.yuanshen.genshin.common.util.UIDUtil;

import java.io.InputStream;

@Slf4j
public class MinioTemplate implements InitializingBean {
	public static final String URI_DELIMITER = "/";
	private final MinioProperties minioProperties;
	private final MinioClient client;
	private String bucket;

	public MinioTemplate(MinioProperties minioProperties, MinioClient client) {
		this.minioProperties = minioProperties;
		this.client = client;
	}

	/**
	 * 创建bucket
	 */
	@SneakyThrows
	public void createBucket(String bucketName) {
		if (!client.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build())) {
			client.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
		}
	}

	/**
	 * 上传文件
	 */
	@SneakyThrows
	public String uploadFile(MultipartFile file) {
		//判断文件是否为空
		if (null == file || 0 == file.getSize()) {
			return null;
		}
		//判断存储桶是否存在  不存在则创建
		createBucket(bucket);
		//文件名
		String originalFilename = file.getOriginalFilename();
		//新的文件名 = 存储桶文件名_时间戳.后缀名
		assert originalFilename != null;

		String[] folders = FileUtil.getDateFolder();

		String fileName = UIDUtil.getUUID() + "." + FileUtil.getSuffix(originalFilename);
		String finalPath = String.join(URI_DELIMITER, folders) + URI_DELIMITER + fileName;
		//开始上传
		client.putObject(
				PutObjectArgs.builder().bucket(bucket).object(finalPath)
						.stream(file.getInputStream(), file.getSize(), -1)
						.contentType(file.getContentType())
						.build()
		);
		return minioProperties.getEndpoint() + URI_DELIMITER + bucket + URI_DELIMITER + finalPath;
	}

	/**
	 * 获取⽂件外链
	 *
	 * @param objectName ⽂件名称
	 * @param expires    过期时间 <=7
	 * @return url
	 */
	@SneakyThrows
	public String getObjectURL(String objectName, Integer expires) {
		return client.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder().bucket(bucket).object(objectName).expiry(expires).build());
	}

	/**
	 * 获取⽂件
	 *
	 * @param objectName ⽂件名称
	 * @return ⼆进制流
	 */
	@SneakyThrows
	public InputStream getObject(String objectName) {
		return client.getObject(GetObjectArgs.builder().bucket(bucket).object(objectName).build());
	}

	/**
	 * 获取⽂件信息
	 *
	 * @param objectName ⽂件名称
	 */
	@SneakyThrows
	public StatObjectResponse getObjectInfo(String objectName) {
		return client.statObject(StatObjectArgs.builder().bucket(bucket).object(objectName).build());
	}

	/**
	 * 删除⽂件
	 *
	 * @param objectName ⽂件名称
	 */
	@SneakyThrows
	public void removeObject(String objectName) {
		client.removeObject(RemoveObjectArgs.builder().bucket(bucket).object(objectName).build());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		this.bucket = minioProperties.getBucket();
	}
}