package site.yuanshen.genshin.common.i18n;

import cn.hutool.core.map.MapUtil;
import com.google.common.collect.Lists;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.List;
import java.util.Locale;
import java.util.Map;

public interface BaseI18nService {

	/**
	 * 根据code locale 获取国际化内容
	 *
	 * @param code   code
	 * @param locale locale
	 * @return String
	 */
	String getMessage(String code, Locale locale);

	/**
	 * 存储i18n内容 返回关联code
	 *
	 * @param i18nMessages i18n内容
	 * @return 关联code
	 */
	String storeI18n(List<Map<String, String>> i18nMessages);

	/**
	 * 存储i18n内容 返回关联code
	 *
	 * @param content i18n内容
	 * @return 关联code
	 */
	default String storeI18n(String content) {
		return storeI18n(Lists.newArrayList(MapUtil.of(LocaleContextHolder.getLocale().toLanguageTag(), content)));
	}
}
