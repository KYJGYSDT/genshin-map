package site.yuanshen.genshin.common.async;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 线程池配置绑定
 *
 * @author Moment
 */
@Data
@Component
@ConfigurationProperties(prefix = "genshin.async")
public class AsyncProperties {

	private int coreSize;

	private int maxSize;

	private int queueCapacity;

	private int keepAliveSeconds;

	private String threadNamePrefix;

	private int awaitTerminationSeconds;
}
