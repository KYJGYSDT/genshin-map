package site.yuanshen.genshin.common.api;

import cn.hutool.core.util.ArrayUtil;
import lombok.AllArgsConstructor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author :  Hu
 */
@AllArgsConstructor
public class WebMvcConfig {
	private final OpposeSpiderProperties opposeSpiderProperties;
	private final RedisTemplate redisTemplate;

	@Bean
	public FilterRegistrationBean<OpposeSpiderFilter> registrationBean() {
		FilterRegistrationBean<OpposeSpiderFilter> filterRegistrationBean = new FilterRegistrationBean<>();
		filterRegistrationBean.setFilter(new OpposeSpiderFilter(opposeSpiderProperties, redisTemplate));
		filterRegistrationBean.addUrlPatterns(ArrayUtil.toArray(opposeSpiderProperties.getMatchApi(), String.class));
		filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return filterRegistrationBean;
	}
}
