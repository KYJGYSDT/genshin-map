package site.yuanshen.genshin.common.exception;

import org.springframework.http.HttpStatus;

/**
 * @author :  Hu
 */
public class CheckException extends BaseException {
	public CheckException(String message) {
		super(message);
	}

	public CheckException(HttpStatus status, String message) {
		super(status, message);
	}
}
