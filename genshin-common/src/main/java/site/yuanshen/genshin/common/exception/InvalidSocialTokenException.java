package site.yuanshen.genshin.common.exception;

import org.springframework.security.core.AuthenticationException;

public class InvalidSocialTokenException extends AuthenticationException {

	public InvalidSocialTokenException(String msg, Throwable t) {
		super(msg, t);
	}

	public InvalidSocialTokenException(String msg) {
		super(msg);
	}
}
