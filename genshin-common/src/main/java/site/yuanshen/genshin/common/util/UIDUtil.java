package site.yuanshen.genshin.common.util;

import java.util.UUID;

public class UIDUtil {


	/**
	 * 获取UUID
	 *
	 * @return String
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
