package site.yuanshen.genshin.common.exception;

import org.springframework.http.HttpStatus;

/**
 * @author :  Hu
 */
public class ResourceAlreadyUseException extends BaseException {
	public ResourceAlreadyUseException(String message) {
		super(message);
	}

	public ResourceAlreadyUseException(HttpStatus status, String message) {
		super(status, message);
	}
}
