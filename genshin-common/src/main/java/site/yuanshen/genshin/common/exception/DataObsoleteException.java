package site.yuanshen.genshin.common.exception;

import org.springframework.http.HttpStatus;

public class DataObsoleteException extends BaseException {
	public DataObsoleteException(String message) {
		super(message);
	}

	public DataObsoleteException(HttpStatus status, String message) {
		super(status, message);
	}
}
