package site.yuanshen.genshin.common.security;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.SneakyThrows;
import site.yuanshen.genshin.common.constant.CommonConstant;
import site.yuanshen.genshin.common.exception.OAuth2BaseException;

public class RestOAuth2ExceptionSerializer extends StdSerializer<OAuth2BaseException> {

	public RestOAuth2ExceptionSerializer() {
		super(OAuth2BaseException.class);
	}

	@Override
	@SneakyThrows
	public void serialize(OAuth2BaseException value, JsonGenerator gen, SerializerProvider provider) {
		gen.writeStartObject();
		gen.writeObjectField("code", CommonConstant.FAIL);
		gen.writeStringField("msg", value.getMessage());
		gen.writeStringField("data", value.getErrorCode());
		gen.writeEndObject();
	}

}
