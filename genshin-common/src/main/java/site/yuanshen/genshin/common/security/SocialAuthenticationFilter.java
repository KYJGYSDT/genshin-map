package site.yuanshen.genshin.common.security;

import com.alibaba.fastjson.JSONObject;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import site.yuanshen.genshin.common.constant.SecurityConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class SocialAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
	@Setter
	private AuthenticationEntryPoint authenticationEntryPoint;

	public SocialAuthenticationFilter() {
		super(new AntPathRequestMatcher(SecurityConstant.SOCIAL_TOKEN_URL, "POST"));
	}

	@Override
	@SneakyThrows
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
		if (!request.getMethod().equals(HttpMethod.POST.name())) {
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		}

		SocialAuthenticationToken token;

		token = new SocialAuthenticationToken(obtainAuthUser(request));
		this.setDetails(request, token);

		Authentication authResult = null;
		try {
			authResult = this.getAuthenticationManager().authenticate(token);

			logger.debug("Authentication success: " + authResult);
			SecurityContextHolder.getContext().setAuthentication(authResult);

		} catch (AuthenticationException failed) {
			SecurityContextHolder.clearContext();
			logger.debug("Authentication request failed: " + failed);

			try {
				authenticationEntryPoint.commence(request, response, failed);
			} catch (Exception e) {
				logger.error("authenticationEntryPoint handle error:{}", failed);
			}
		}

		return authResult;
	}

	/**
	 * 获取 justauth 登录后的用户信息
	 */
	@SneakyThrows
	protected SocialUser obtainAuthUser(HttpServletRequest request) {
		String accessToken = request.getParameter(SecurityConstant.ACCESS_TOKEN);
		OkHttpClient client = new OkHttpClient();
		Request req = new Request.Builder().get().url(String.format(SecurityConstant.GITEE_USER_INFO_URL, accessToken)).build();
		Response response = client.newCall(req).execute();
		if (response.isSuccessful()) {
			String result = response.body().string();
			JSONObject object = JSONObject.parseObject(result);

			return SocialUser.builder()
					.uuid(object.getString(SecurityConstant.ID))
					.avatar(object.getString(SecurityConstant.AVATAR_URL))
					.nickname(object.getString(SecurityConstant.NAME))
					.grantType(SecurityConstant.GITEE_GRANT)
					.build();
		}

		return null;
	}

	private void setDetails(HttpServletRequest request, SocialAuthenticationToken authRequest) {
		authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
	}
}
