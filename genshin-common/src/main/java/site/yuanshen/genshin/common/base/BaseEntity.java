package site.yuanshen.genshin.common.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.Version;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 实体类的基类
 */
@Data
public class BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 逻辑删除:0:未删除，1:删除
	 */
	@TableLogic
	@JsonIgnore
	@ApiModelProperty(value = "逻辑删除:0:未删除，1:删除")
	private String delFlag;

	/**
	 * 修改次数
	 */
	@Version
	@ApiModelProperty(value = "修改次数")
	private Long version;

	/**
	 * 创建人
	 */
	@TableField(fill = FieldFill.INSERT)
	@ApiModelProperty(value = "创建人")
	private Long creatorId;

	/**
	 * 更新人
	 */
	@TableField(fill = FieldFill.UPDATE)
	@ApiModelProperty(value = "更新人")
	private Long updaterId;

	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;

	/**
	 * 更新时间
	 */
	@ApiModelProperty(value = "更新时间")
	private LocalDateTime updateTime;

}
