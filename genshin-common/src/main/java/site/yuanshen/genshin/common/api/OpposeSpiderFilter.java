package site.yuanshen.genshin.common.api;

import cn.hutool.extra.servlet.ServletUtil;
import com.google.common.base.Charsets;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.common.util.ResponseUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 反爬过滤器
 *
 * @author :  Hu
 */
@Slf4j
@AllArgsConstructor
public class OpposeSpiderFilter implements Filter {
	private final OpposeSpiderProperties opposeSpiderProperties;
	private final RedisTemplate redisTemplate;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;

		String charset = Charsets.UTF_8.name();

		String ts = ServletUtil.getHeader(httpServletRequest, "ts", charset);
		String sign = ServletUtil.getHeader(httpServletRequest, "sign", charset);
		if (StringUtils.isEmpty(ts) || StringUtils.isEmpty(sign)) {
			log.error("oppose spider got error, required ts sign");
			ResponseUtil.responseWriter(httpServletResponse, MediaType.APPLICATION_JSON_VALUE, HttpServletResponse.SC_FORBIDDEN,
					R.failed(HttpServletResponse.SC_FORBIDDEN, "没有访问权限"));
			return;
		}

		Long delay = opposeSpiderProperties.getDelay();

		Boolean redoFlag = redisTemplate.opsForValue().setIfAbsent(CacheConstant.REDO_KEY + ts, ts, delay, TimeUnit.MILLISECONDS);

		String realSign = DigestUtils.md5Hex(ts + opposeSpiderProperties.getSecretKey());

		long realTs = System.currentTimeMillis();
		long longTs = Long.parseLong(ts);
		boolean timeout = realTs - longTs > delay;
		if (!StringUtils.equals(sign, realSign) || timeout || !redoFlag) {
			log.error("oppose spider got error, sign:{}, realSign:{}, realTs:{} - ts:{} > delay:{} = {}, redo:{}", sign, realSign,
					realTs, longTs, delay, timeout, !redoFlag);
			ResponseUtil.responseWriter(httpServletResponse, MediaType.APPLICATION_JSON_VALUE, HttpServletResponse.SC_FORBIDDEN,
					R.failed(HttpServletResponse.SC_FORBIDDEN, "没有访问权限"));
			return;
		}

		chain.doFilter(request, response);
	}
}
