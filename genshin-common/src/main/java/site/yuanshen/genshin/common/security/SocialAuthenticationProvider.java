package site.yuanshen.genshin.common.security;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import site.yuanshen.genshin.common.exception.InvalidSocialTokenException;
import site.yuanshen.genshin.common.util.SecurityMessageSourceUtil;

import java.util.Objects;

@Slf4j
public class SocialAuthenticationProvider implements AuthenticationProvider {
	private final MessageSourceAccessor messages = SecurityMessageSourceUtil.getAccessor();

	@Getter
	@Setter
	private GenshinUserDetailService userDetailsService;

	@Override
	public Authentication authenticate(Authentication authentication) {
		SocialAuthenticationToken socialAuthenticationToken = (SocialAuthenticationToken) authentication;

		SocialUser user = (SocialUser) socialAuthenticationToken.getPrincipal();
		if (Objects.isNull(user)) {
			throw new InvalidSocialTokenException("invalid social access token");
		}

		UserDetails userDetails = userDetailsService.loadUserBySocial(user.getUuid(), user.getGrantType());
		if (Objects.isNull(userDetails)) {
			userDetails = userDetailsService.registerSocialUser(user);
		}

		SocialAuthenticationToken authenticationToken = new SocialAuthenticationToken(userDetails, userDetails.getAuthorities());
		authenticationToken.setDetails(socialAuthenticationToken.getDetails());
		return authenticationToken;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return SocialAuthenticationToken.class.isAssignableFrom(authentication);
	}

}
