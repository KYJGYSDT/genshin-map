package site.yuanshen.genshin.common.exception;

import org.springframework.http.HttpStatus;

public class ResourceNotExistException extends BaseException {

	public ResourceNotExistException(String message) {
		super(message);
	}

	public ResourceNotExistException(HttpStatus status, String message) {
		super(status, message);
	}
}
