package site.yuanshen.genshin.common.constant;

/**
 * 缓存常量
 *
 * @author :  Hu
 */
public interface CacheConstant {

	/**
	 * 地区
	 */
	String ALL_AREA = "area_all";

	/**
	 * 地区 可显示的
	 */
	String AREA_VISIBILITY = "area_visibility";

	/**
	 * 类别
	 */
	String ALL_TYPE = "type_all";

	/**
	 * 类别 可显示的
	 */
	String TYPE_VISIBILITY = "type_visibility";

	/**
	 * 条目
	 */
	String ALL_ITEM = "item_all";

	/**
	 * 条目 可显示的
	 */
	String ITEM_VISIBILITY = "item_visibility";

	/**
	 * 用户信息缓存
	 */
	String USER_DETAILS = "user_details";

	/**
	 * 角色的菜单
	 */
	String MENU_OF_ROLE = "menu_of_role";

	/**
	 * 用户的权限
	 */
	String ROLE_OF_USER = "role_of_user";

	/**
	 * 菜单信息缓存
	 */
	String MENU_DETAILS = "menu_details";

	/**
	 * 角色信息缓存
	 */
	String ROLE_DETAILS = "role_details";

	/**
	 * 字典信息缓存
	 */
	String DICT_DETAILS = "dict_details";

	/**
	 * oauth 客户端信息
	 */
	String CLIENT_DETAILS_KEY = SecurityConstant.GENSHIN_OAUTH_PREFIX + "client:details";

	/**
	 * 参数缓存
	 */
	String PARAMS_DETAILS = "params_details";

	/**
	 * 激活的版本信息
	 */
	String VERSION_KEY = "version";

	/**
	 * 重放攻击Key
	 */
	String REDO_KEY = "redo:";

	/**
	 * I18N 缓存key
	 */
	String I18N_DETAILS = "i18n";
}
