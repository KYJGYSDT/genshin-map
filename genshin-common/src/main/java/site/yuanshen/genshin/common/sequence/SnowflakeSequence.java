package site.yuanshen.genshin.common.sequence;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.IdUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;

@Slf4j
public class SnowflakeSequence implements InitializingBean {
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private long workerId;//为终端ID
	private final long datacenterId = 1;//数据中心ID
	private final Snowflake snowflake = IdUtil.createSnowflake(workerId, datacenterId);

	@Override
	public void afterPropertiesSet() throws Exception {
		workerId = NetUtil.ipv4ToLong(NetUtil.getLocalhostStr());
	}

	public synchronized long nextId() {
		return snowflake.nextId();
	}

	public synchronized String nextStrId() {
		return String.valueOf(snowflake.nextId());
	}
}
