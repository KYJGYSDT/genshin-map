package site.yuanshen.genshin.common.amqp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 消息队列配置绑定
 *
 * @author Moment
 */
@Data
@Component
@ConfigurationProperties(prefix = "genshin.queue")
public class QueueProperties {

	private String distributePunctuate;

}
