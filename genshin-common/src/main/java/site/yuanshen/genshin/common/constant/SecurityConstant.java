package site.yuanshen.genshin.common.constant;

/**
 * @date 2017-12-18
 */
public interface SecurityConstant {

	/**
	 * 角色前缀
	 */
	String ROLE = "ROLE_";

	/**
	 * 前缀
	 */
	String GENSHIN_PREFIX = "genshin_";

	/**
	 * oauth 相关前缀
	 */
	String OAUTH_PREFIX = "oauth:";

	/**
	 * genshin_oauth:
	 */
	String GENSHIN_OAUTH_PREFIX = GENSHIN_PREFIX + OAUTH_PREFIX;

	/**
	 * 项目的license
	 */
	String GENSHIN_LICENSE = "made by KongYing map";

	/**
	 * 社交登录URL
	 */
	String SOCIAL_TOKEN_URL = "/oauth/token/social";

	/**
	 * 码云获取token
	 */
	String GITEE_AUTHORIZATION_CODE_URL = "https://gitee.com/oauth/token?grant_type="
			+ "authorization_code&code=%S&client_id=%s&redirect_uri=" + "%s&client_secret=%s";

	/**
	 * 码云获取用户信息
	 */
	String GITEE_USER_INFO_URL = "https://gitee.com/api/v5/user?access_token=%s";

	/**
	 * 客户端模式
	 */
	String CLIENT_CREDENTIALS = "client_credentials";

	/**
	 * 用户基本信息
	 */
	String DETAILS_USER = "user_info";

	/**
	 * 协议字段
	 */
	String DETAILS_LICENSE = "license";

	/**
	 * 激活字段 兼容外围系统接入
	 */
	String ACTIVE = "active";

	/**
	 * access_token 字段
	 */
	String ACCESS_TOKEN = "access_token";

	/**
	 * id 字段
	 */
	String ID = "id";

	/**
	 * 头像URL 字段
	 */
	String AVATAR_URL = "avatar_url";

	/**
	 * 昵称字段
	 */
	String NAME = "name";

	/**
	 * gitee授权方式
	 */
	String GITEE_GRANT = "gitee";
}
