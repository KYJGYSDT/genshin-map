package site.yuanshen.genshin.common.util;

import cn.hutool.core.util.StrUtil;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import site.yuanshen.genshin.common.constant.SecurityConstant;
import site.yuanshen.genshin.common.security.GenshinUser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@UtilityClass
public class SecurityUtil {

	private Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public GenshinUser getUser() {
		Authentication authentication = getAuthentication();
		return (GenshinUser) authentication.getPrincipal();
	}

	public Long getUserId() {
		GenshinUser user = getUser();
		if (Objects.isNull(user)) {
			return null;
		}
		return user.getId();
	}

	/**
	 * 获取用户角色信息
	 *
	 * @return 角色集合
	 */
	public List<Long> getRoles() {
		Authentication authentication = getAuthentication();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

		List<Long> roleIds = new ArrayList<>();
		authorities.stream().filter(granted -> StrUtil.startWith(granted.getAuthority(), SecurityConstant.ROLE))
				.forEach(granted -> {
					String id = StrUtil.removePrefix(granted.getAuthority(), SecurityConstant.ROLE);
					roleIds.add(Long.parseLong(id));
				});
		return roleIds;
	}
}
