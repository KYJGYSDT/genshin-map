package site.yuanshen.genshin.common.security;

import cn.hutool.core.util.ArrayUtil;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import site.yuanshen.genshin.common.constant.SecurityConstant;

@Order(4)
@Configuration
@AllArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	private final SecurityProperties securityProperties;
	private final RedisConnectionFactory redisConnectionFactory;
	private final GenshinUserDetailService userServiceImpl;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.requestMatchers().anyRequest()
				.and()
				.formLogin()
				.and()
				.authorizeRequests().antMatchers("/token/**", "/oauth/**").permitAll()
				.anyRequest().authenticated()
				.and()
				.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint()).accessDeniedHandler(restAccessDeniedHandler())
				.and()
				.csrf().disable()
				.apply(socialSecurityConfigurer());
	}

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers(ArrayUtil.toArray(securityProperties.getIgnoreApi(), String.class));
	}

	@Bean
	public AuthenticationEntryPoint restAuthenticationEntryPoint() {
		return new RestAuthenticationEntryPoint();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

	@Bean
	public AccessDeniedHandler restAccessDeniedHandler() {
		return new RestAccessDeniedHandler();
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public RedisTokenStore redisTokenStore() {
		RedisTokenStore redisTokenStore = new RedisTokenStore(redisConnectionFactory);
		redisTokenStore.setPrefix(SecurityConstant.GENSHIN_OAUTH_PREFIX);
		return redisTokenStore;
	}

	@Bean
	public AuthenticationSuccessHandler socialLoginSuccessHandler() {
		return new SocialLoginSuccessHandler();
	}

	@Bean
	public SocialSecurityConfigurer socialSecurityConfigurer() {
		SocialSecurityConfigurer socialSecurityConfigurer = new SocialSecurityConfigurer();
		socialSecurityConfigurer.setUserServiceImpl(userServiceImpl);
		socialSecurityConfigurer.setSocialLoginSuccessHandler(socialLoginSuccessHandler());
		socialSecurityConfigurer.setAuthenticationEntryPoint(restAuthenticationEntryPoint());
		return socialSecurityConfigurer;
	}
}
