package site.yuanshen.genshin.common.security;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.Collection;

@Getter
public class GenshinUser extends User implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	private final Long id;

	/**
	 * 部门ID
	 */
	private final Long deptId;

	/**
	 * 头像
	 */
	private final String avatar;

	public GenshinUser(Long id, String username, String password, boolean enabled, Long deptId, String avatar, boolean accountNonExpired,
					   boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.id = id;
		this.deptId = deptId;
		this.avatar = avatar;
	}
}
