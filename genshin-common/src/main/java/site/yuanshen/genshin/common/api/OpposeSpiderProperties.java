package site.yuanshen.genshin.common.api;


import com.google.common.collect.Sets;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Set;

/**
 * 反爬 properties
 */
@Data
@ConfigurationProperties(prefix = "genshin.oppose-spider")
public class OpposeSpiderProperties {
	/**
	 * 需要做反爬的接口
	 */
	private Set<String> matchApi = Sets.newHashSet();

	/**
	 * md5 secret key
	 */
	private String secretKey = "site.yuanshen";

	/**
	 * 延迟 ms
	 */
	private Long delay = 3000L;
}
