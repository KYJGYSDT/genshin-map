package site.yuanshen.genshin.common.security;

import cn.hutool.core.collection.CollectionUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import site.yuanshen.genshin.common.api.OpposeSpiderProperties;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

@AutoConfigureAfter(OpposeSpiderProperties.class)
@ConfigurationProperties(prefix = "genshin.security")
public class SecurityProperties implements InitializingBean {
	@Resource
	@Setter
	private OpposeSpiderProperties opposeSpiderProperties;
	@Getter
	@Setter
	private Set<String> ignoreApi = new HashSet<>();

	@Override
	public void afterPropertiesSet() throws Exception {
		CollectionUtil.addAll(ignoreApi, opposeSpiderProperties.getMatchApi());
	}
}

