package site.yuanshen.genshin.common.security;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;

@AllArgsConstructor
@EnableResourceServer
public class ResourceConfig extends ResourceServerConfigurerAdapter {
	private final AuthenticationEntryPoint restAuthenticationEntryPoint;
	private final AccessDeniedHandler restAccessDeniedHandler;
	private final SocialSecurityConfigurer socialSecurityConfigurer;

	@Override
	@SneakyThrows
	public void configure(HttpSecurity httpSecurity) {
		// 允许使用iframe 嵌套，避免swagger-ui 不被加载的问题
		httpSecurity.headers().frameOptions().disable();
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = httpSecurity
				.authorizeRequests();
		registry.antMatchers("/token/**", "/oauth/**").permitAll()
				.anyRequest().authenticated().and().csrf().disable()
				.apply(socialSecurityConfigurer);
	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.authenticationEntryPoint(restAuthenticationEntryPoint)
				.accessDeniedHandler(restAccessDeniedHandler);
	}
}
