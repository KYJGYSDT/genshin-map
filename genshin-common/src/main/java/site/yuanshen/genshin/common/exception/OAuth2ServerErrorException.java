package site.yuanshen.genshin.common.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.http.HttpStatus;
import site.yuanshen.genshin.common.security.RestOAuth2ExceptionSerializer;

@JsonSerialize(using = RestOAuth2ExceptionSerializer.class)
public class OAuth2ServerErrorException extends OAuth2BaseException {

	public OAuth2ServerErrorException(String msg, Throwable t) {
		super(msg);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "server_error";
	}

	@Override
	public int getHttpErrorCode() {
		return HttpStatus.INTERNAL_SERVER_ERROR.value();
	}

}
