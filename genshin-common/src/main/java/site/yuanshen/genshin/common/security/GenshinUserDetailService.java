package site.yuanshen.genshin.common.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface GenshinUserDetailService extends UserDetailsService {

	/**
	 * 第三方登陆
	 *
	 * @param uuid      uuid
	 * @param grantType grantType
	 * @return UserDetails
	 */
	UserDetails loadUserBySocial(String uuid, String grantType);

	/**
	 * 为三方账号注册本站账号
	 *
	 * @param user
	 * @return
	 */
	UserDetails registerSocialUser(SocialUser user);
}
