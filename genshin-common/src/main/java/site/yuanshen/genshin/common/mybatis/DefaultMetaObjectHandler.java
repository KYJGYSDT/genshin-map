package site.yuanshen.genshin.common.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import site.yuanshen.genshin.common.util.SecurityUtil;

/**
 * 元属性自动填充
 */
public class DefaultMetaObjectHandler implements MetaObjectHandler {

	@Override
	public void insertFill(MetaObject metaObject) {
		this.strictInsertFill(metaObject, "creatorId", Long.class, SecurityUtil.getUserId());
	}

	@Override
	public void updateFill(MetaObject metaObject) {
		this.strictUpdateFill(metaObject, "updaterId", Long.class, SecurityUtil.getUserId());
	}
}
