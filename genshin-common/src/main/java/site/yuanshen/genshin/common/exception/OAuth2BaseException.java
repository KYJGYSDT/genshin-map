package site.yuanshen.genshin.common.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import site.yuanshen.genshin.common.security.RestOAuth2ExceptionSerializer;

@JsonSerialize(using = RestOAuth2ExceptionSerializer.class)
public class OAuth2BaseException extends OAuth2Exception {

	@Getter
	private String errorCode;

	public OAuth2BaseException(String msg) {
		super(msg);
	}

	public OAuth2BaseException(String msg, Throwable t) {
		super(msg, t);
	}

	public OAuth2BaseException(String msg, String errorCode) {
		super(msg);
		this.errorCode = errorCode;
	}

}
