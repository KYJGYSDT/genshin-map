package site.yuanshen.genshin.common.exception;

import com.google.common.base.Throwables;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;
import site.yuanshen.genshin.common.api.R;

import javax.validation.ConstraintViolationException;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 全局异常捕获
 */
@Slf4j
@ResponseBody
@RestControllerAdvice
public class BaseExceptionHandler {

	/**
	 * BaseException 异常捕获处理
	 *
	 * @param ex 自定义BaseException异常类型
	 * @return Result
	 */
	@ExceptionHandler
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public R<?> handleException(BaseException ex) {
		log.error("程序异常：{}", Throwables.getStackTraceAsString(ex));
		return R.failed(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
	}

	/**
	 * ResourceNotExistException 异常捕获处理
	 *
	 * @param ex 自定义ResourceNotExistException异常类型
	 * @return Result
	 */
	@ExceptionHandler
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public R<?> handleException(ResourceNotExistException ex) {
		log.error("资源不存在：{}", Throwables.getStackTraceAsString(ex));
		return R.failed(HttpStatus.NOT_FOUND.value(), ex.getMessage());
	}

	/**
	 * ResourceAlreadyUseException 异常捕获处理
	 *
	 * @param ex 自定义ResourceAlreadyUseException异常类型
	 * @return Result
	 */
	@ExceptionHandler
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public R<?> handleException(ResourceAlreadyUseException ex) {
		log.error("资源已被使用：{}", Throwables.getStackTraceAsString(ex));
		return R.failed(HttpStatus.NOT_ACCEPTABLE.value(), ex.getMessage());
	}

	/**
	 * CheckException 异常捕获处理
	 *
	 * @param ex CheckException 自定义参数绑定异常
	 * @return Result
	 */
	@ExceptionHandler({CheckException.class})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public R<?> handleBodyValidException(CheckException ex) {
		log.error("参数绑定异常：{}", Throwables.getStackTraceAsString(ex));
		return R.failed(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
	}

	/**
	 * MethodArgumentNotValidException 异常捕获处理
	 *
	 * @param ex MethodArgumentNotValidException 参数绑定异常
	 * @return Result
	 */
	@ExceptionHandler({MethodArgumentNotValidException.class})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public R<?> handleBodyValidException(MethodArgumentNotValidException ex) {
		List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
		log.error("参数绑定异常：{}", fieldErrors.get(0).getDefaultMessage());
		return R.failed(HttpStatus.BAD_REQUEST.value(), fieldErrors.get(0).getDefaultMessage());
	}

	/**
	 * MethodArgumentNotValidException 异常捕获处理
	 *
	 * @param ex MethodArgumentNotValidException 参数绑定异常
	 * @return Result
	 */
	@ExceptionHandler({ConstraintViolationException.class})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public R<?> handleBodyValidException(ConstraintViolationException ex) {
		log.error("参数绑定异常：{}", ex.getMessage());
		List<String> errorMsg = Arrays.stream(ex.getMessage().split(",")).map(m -> {
			return StringUtils.trimToEmpty(StringUtils.split(m, ':')[1]);
		}).collect(Collectors.toList());
		return R.failed(HttpStatus.BAD_REQUEST.value(), errorMsg.toString());
	}


	/**
	 * BindException 异常捕获处理
	 *
	 * @param ex BindException 参数绑定异常
	 * @return Result
	 */
	@ExceptionHandler({BindException.class})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public R<?> handleBindException(BindException ex) {
		List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
		log.error("参数绑定异常：{}", fieldErrors.get(0).getDefaultMessage());
		return R.failed(HttpStatus.BAD_REQUEST.value(), fieldErrors.get(0).getDefaultMessage());
	}

	/**
	 * FileNotFoundException,NoHandlerFoundException 异常捕获处理
	 *
	 * @param exception 自定义FileNotFoundException异常类型
	 * @return Result
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({FileNotFoundException.class, NoHandlerFoundException.class})
	public R<?> noFoundException(Exception exception) {
		log.error("程序异常: {}", Throwables.getStackTraceAsString(exception));
		return R.failed(HttpStatus.NOT_FOUND.value(), exception.getMessage());
	}

	/**
	 * AccessDeniedException
	 *
	 * @param e the e
	 * @return R
	 */
	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public R<?> handleAccessDeniedException(AccessDeniedException e) {
		log.error("拒绝授权异常: {}", Throwables.getStackTraceAsString(e));
		return R.failed("权限不足，不允许访问");
	}

	/**
	 * NullPointerException 空指针异常捕获处理
	 *
	 * @param ex 自定义NullPointerException异常类型
	 * @return Result
	 */
	@ExceptionHandler
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public R<?> handleException(NullPointerException ex) {
		log.error("程序异常：{}", Throwables.getStackTraceAsString(ex));
		return R.failed(HttpStatus.INTERNAL_SERVER_ERROR.value(), "系统异常");
	}

	/**
	 * 通用Exception异常捕获
	 *
	 * @param ex 自定义Exception异常类型
	 * @return Result
	 */
	@ExceptionHandler
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public R<?> handleException(Exception ex) {
		log.error("程序异常：{}", Throwables.getStackTraceAsString(ex));
		return R.failed(HttpStatus.INTERNAL_SERVER_ERROR.value(), "系统异常");
	}
}
