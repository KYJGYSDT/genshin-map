package site.yuanshen.genshin.common.exception;

import org.springframework.http.HttpStatus;

public class ExecuteFailException extends BaseException {
	public ExecuteFailException(String message) {
		super(message);
	}

	public ExecuteFailException(HttpStatus status, String message) {
		super(status, message);
	}
}
