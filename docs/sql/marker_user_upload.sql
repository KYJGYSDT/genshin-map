/*
 Navicat Premium Data Transfer

 Source Server         : local-dev
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : genshin_map1

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 08/07/2021 22:57:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for marker_user_upload
-- ----------------------------
DROP TABLE IF EXISTS `marker_user_upload`;
CREATE TABLE `marker_user_upload` (
  `id` bigint NOT NULL,
  `x` double DEFAULT NULL COMMENT '点位X坐标',
  `y` double DEFAULT NULL COMMENT '点位Y坐标',
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '上报说明',
  `reporter_gitee` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '上报者Gitee账号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='用户上报点位';

SET FOREIGN_KEY_CHECKS = 1;
