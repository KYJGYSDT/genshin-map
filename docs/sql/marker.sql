/*
 Navicat Premium Data Transfer

 Source Server         : yuanshen.site
 Source Server Type    : MySQL
 Source Server Version : 50649
 Source Host           : 171.107.186.113:3306
 Source Schema         : genshin_map

 Target Server Type    : MySQL
 Target Server Version : 50649
 File Encoding         : 65001

 Date: 07/09/2021 22:43:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for marker
-- ----------------------------
DROP TABLE IF EXISTS `marker`;
CREATE TABLE `marker` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `m_layer` int(11) NOT NULL DEFAULT '0' COMMENT '兼容老版本',
  `m_item_id` int(11) NOT NULL COMMENT '兼容老版本',
  `position` varchar(64) NOT NULL DEFAULT '' COMMENT 'xy',
  `title` varchar(255) DEFAULT NULL COMMENT '表体',
  `content` varchar(1024) DEFAULT NULL COMMENT '描述',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `time` int(11) NOT NULL DEFAULT '-1' COMMENT '刷新时间 小时',
  `visibility` int(11) NOT NULL DEFAULT '0' COMMENT '是否显示 1显示 -1不显示',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除:0:未删除，1:删除',
  `creator_id` bigint(20) unsigned DEFAULT NULL COMMENT '创建者id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '插入时间',
  `version` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改次数',
  `updater_id` bigint(20) unsigned DEFAULT NULL COMMENT '修改者id',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24577 DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS = 1;
