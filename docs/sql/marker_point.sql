/*
 Navicat Premium Data Transfer

 Source Server         : local-dev
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : genshin_map1

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 08/07/2021 22:57:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for marker_point
-- ----------------------------
DROP TABLE IF EXISTS `marker_point`;
CREATE TABLE `marker_point` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `x` double DEFAULT NULL COMMENT 'X坐标',
  `y` double DEFAULT NULL COMMENT 'Y坐标',
  `area_id` bigint DEFAULT NULL COMMENT '区域ID',
  `store_id` bigint NOT NULL COMMENT '点位存储ID（默认和当前ID一致，但是可以绑定到其他点位）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标识',
  `creator_id` bigint DEFAULT NULL COMMENT '创建者id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '插入时间',
  `version` bigint NOT NULL DEFAULT '1' COMMENT '修改次数',
  `updater_id` bigint DEFAULT NULL COMMENT '修改者id',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='点位标记';

SET FOREIGN_KEY_CHECKS = 1;
