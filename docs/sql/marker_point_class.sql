/*
 Navicat Premium Data Transfer

 Source Server         : local-dev
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : genshin_map1

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 08/07/2021 22:57:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for marker_point_class
-- ----------------------------
DROP TABLE IF EXISTS `marker_point_class`;
CREATE TABLE `marker_point_class` (
  `point_id` bigint NOT NULL COMMENT 'point.id',
  `class_id` bigint NOT NULL COMMENT 'class.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='点位标记和类别关联';

SET FOREIGN_KEY_CHECKS = 1;
