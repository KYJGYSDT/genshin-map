/*
 Navicat Premium Data Transfer

 Source Server         : yuanshen.site
 Source Server Type    : MySQL
 Source Server Version : 50649
 Source Host           : 171.107.186.113:3306
 Source Schema         : genshin_map

 Target Server Type    : MySQL
 Target Server Version : 50649
 File Encoding         : 65001

 Date: 26/08/2021 20:49:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for punctuate
-- ----------------------------
DROP TABLE IF EXISTS `punctuate`;
CREATE TABLE `punctuate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `item_id` bigint(20) unsigned NOT NULL COMMENT '选项ID',
  `content` varchar(255) DEFAULT NULL COMMENT '标点内容',
  `remark` text COMMENT '备注',
  `img_path` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `position` varchar(255) NOT NULL COMMENT '坐标',
  `time` int(11) NOT NULL DEFAULT '-1' COMMENT '刷新时间 小时',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除:0:未删除，1:删除',
  `creator_id` bigint(20) unsigned DEFAULT NULL COMMENT '创建者id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '插入时间',
  `version` bigint(20) NOT NULL DEFAULT '1' COMMENT '修改次数',
  `updater_id` bigint(20) unsigned DEFAULT NULL COMMENT '修改者id',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='用户打点';

SET FOREIGN_KEY_CHECKS = 1;
