CREATE DEFINER=CURRENT_USER FUNCTION `unproject`(
    `position` VARCHAR(64),
    `origSizeX` INT,
    `origSizeY` INT,
    `offsetCenterX` INT,
    `offsetCenterY` INT
) RETURNS varchar(64) CHARSET utf8mb4
BEGIN
    DECLARE positionLat, positionLng VARCHAR(32);
    DECLARE positionX, positionY VARCHAR(32);
    DECLARE positionRes VARCHAR(64);
    
    SET positionX = LEFT(`position`, POSITION(',' IN `position`) - 1);
    SET positionY = RIGHT(`position`, LENGTH(`position`) - POSITION(',' IN `position`));
    SET positionLat = 2 * (ATAN(EXP((positionY + `offsetCenterY`) / (-`origSizeY`) / 2 * PI()))) * 180 / PI() - 90;
    SET positionLng = (positionX + `offsetCenterX`) / `origSizeX` * 90;
    SET positionRes = CONCAT(positionLat, ',', positionLng);
    RETURN positionRes;
END
