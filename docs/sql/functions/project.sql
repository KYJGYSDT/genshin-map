CREATE DEFINER=CURRENT_USER FUNCTION `project`(
    `position` VARCHAR(64),
    `origSizeX` INT,
    `origSizeY` INT,
    `offsetCenterX` INT,
    `offsetCenterY` INT
) RETURNS varchar(64) CHARSET utf8mb4
BEGIN
    DECLARE positionLat, positionLng VARCHAR(32);
    DECLARE positionX, positionY VARCHAR(32);
    DECLARE positionRes VARCHAR(64);
    DECLARE sinLat DOUBLE;

    SET positionLat = LEFT(`position`, POSITION(',' IN `position`) - 1);
    SET positionLng = RIGHT(`position`, LENGTH(`position`) - POSITION(',' IN `position`));
    SET sinLat = SIN(`positionLat` * PI() / 180);
    SET positionX = `positionLng` / 90 * `origSizeX` - `offsetCenterX`;
    SET positionY = -LOG((1 + sinLat) / (1 - sinLat)) / PI() * `origSizeY` - `offsetCenterY`;
    SET positionRes = CONCAT(positionX, ',', positionY);
    RETURN positionRes;
END
