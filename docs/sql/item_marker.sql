/*
 Navicat Premium Data Transfer

 Source Server         : local-dev
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : genshin_map

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 20/07/2021 22:02:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for item_marker
-- ----------------------------
DROP TABLE IF EXISTS `item_marker`;
CREATE TABLE `item_marker` (
  `item_id` bigint NOT NULL COMMENT 'item_id',
  `marker_id` bigint NOT NULL COMMENT 'marker_id',
  UNIQUE KEY `unx_item_marker` (`item_id`,`marker_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

SET FOREIGN_KEY_CHECKS = 1;
