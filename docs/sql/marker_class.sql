/*
 Navicat Premium Data Transfer

 Source Server         : local-dev
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : genshin_map1

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 08/07/2021 22:56:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for marker_class
-- ----------------------------
DROP TABLE IF EXISTS `marker_class`;
CREATE TABLE `marker_class` (
  `id` bigint NOT NULL,
  `pid` bigint NOT NULL DEFAULT '0' COMMENT '父级ID',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用于搜索使用的标记',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标题',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '说明',
  `icon` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标路径',
  `area_ids` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '逗号分隔的区域ID',
  `refresh_time` int NOT NULL DEFAULT '-1' COMMENT '刷新时间（小时）',
  `visibility` int NOT NULL DEFAULT '1' COMMENT '是否显示 1显示 -1不显示',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除:0:未删除，1:删除',
  `creator_id` bigint unsigned DEFAULT NULL COMMENT '创建者id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '插入时间',
  `version` bigint NOT NULL DEFAULT '1' COMMENT '修改次数',
  `updater_id` bigint unsigned DEFAULT NULL COMMENT '修改者id',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='点位类别';

SET FOREIGN_KEY_CHECKS = 1;
