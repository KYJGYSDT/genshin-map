package site.yuanshen.genshin.achievement.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.achievement.entity.Achievement;

/**
 * 成就
 *
 * @author Hu
 */
public interface AchievementService extends IService<Achievement> {
}
