package site.yuanshen.genshin.achievement.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import site.yuanshen.genshin.achievement.entity.Achievement;
import site.yuanshen.genshin.achievement.mapper.AchievementMapper;
import site.yuanshen.genshin.achievement.service.AchievementService;

/**
 * 成就
 *
 * @author Hu
 */
@Slf4j
@Service
@AllArgsConstructor
public class AchievementServiceImpl extends ServiceImpl<AchievementMapper, Achievement> implements AchievementService {
}
