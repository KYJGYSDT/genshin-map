package site.yuanshen.genshin.achievement.controller;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 成就
 *
 * @author :  Hu
 */
@RestController
@AllArgsConstructor
@RequestMapping("/achievement")
@Api(value = "achievement", tags = "成就API")
public class AchievementController {
}
