package site.yuanshen.genshin.achievement.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

/**
 * 成就
 *
 * @author Hu
 */
@Data
@TableName("achievement")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "成就")
public class Achievement extends BaseEntity {
	private static final long serialVersionUID = 1L;
}
