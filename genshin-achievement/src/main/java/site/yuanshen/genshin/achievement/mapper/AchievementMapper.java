package site.yuanshen.genshin.achievement.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import site.yuanshen.genshin.achievement.entity.Achievement;

/**
 * 成就
 *
 * @author Hu
 */
@Mapper
public interface AchievementMapper extends BaseMapper<Achievement> {

}
