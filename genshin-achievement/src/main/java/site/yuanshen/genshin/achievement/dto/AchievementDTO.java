package site.yuanshen.genshin.achievement.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "成就DTO")
public class AchievementDTO implements Serializable {
	private static final long serialVersionUID = 1L;

}
