package site.yuanshen.genshin.achievement.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 成就
 *
 * @author :  Hu
 */
@Data
@NoArgsConstructor
public class AchievementVO implements Serializable {
	private static final long serialVersionUID = 1L;

}
