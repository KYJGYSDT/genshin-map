package site.yuanshen.genshin.launch;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScans({
		@MapperScan("site.yuanshen.genshin.core.mapper"),
		@MapperScan("site.yuanshen.genshin.system.mapper"),
		@MapperScan("site.yuanshen.genshin.achievement.mapper"),
})
@SpringBootApplication(scanBasePackages = {"site.yuanshen.genshin.core", "site.yuanshen.genshin.system", "site.yuanshen.genshin.achievement"})
public class GenshinLaunchApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenshinLaunchApplication.class, args);
	}

}
