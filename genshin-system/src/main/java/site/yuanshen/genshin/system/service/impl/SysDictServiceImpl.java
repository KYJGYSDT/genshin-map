package site.yuanshen.genshin.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.system.entity.SysDict;
import site.yuanshen.genshin.system.entity.SysDictItem;
import site.yuanshen.genshin.system.enums.DictTypeEnum;
import site.yuanshen.genshin.system.mapper.SysDictItemMapper;
import site.yuanshen.genshin.system.mapper.SysDictMapper;
import site.yuanshen.genshin.system.service.SysDictService;

/**
 * 字典表
 *
 * @date 2019/03/19
 */
@Service
@AllArgsConstructor
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {

	private final SysDictItemMapper dictItemMapper;

	/**
	 * 根据ID 删除字典
	 *
	 * @param id 字典ID
	 * @return
	 */
	@Override
	@CacheEvict(value = CacheConstant.DICT_DETAILS, allEntries = true)
	@Transactional(rollbackFor = Exception.class)
	public R<Boolean> removeDict(Long id) {
		SysDict dict = this.getById(id);
		// 系统内置
		if (DictTypeEnum.SYSTEM.getType().equals(dict.getSystem())) {
			return R.failed("系统内置字典不能删除");
		}

		baseMapper.deleteById(id);
		dictItemMapper.delete(Wrappers.<SysDictItem>lambdaQuery().eq(SysDictItem::getDictId, id));
		return R.ok(Boolean.TRUE);
	}

	/**
	 * 更新字典
	 *
	 * @param dict 字典
	 * @return
	 */
	@Override
	public R<Boolean> updateDict(SysDict dict) {
		SysDict sysDict = this.getById(dict.getId());
		// 系统内置
		if (DictTypeEnum.SYSTEM.getType().equals(sysDict.getSystem())) {
			return R.failed("系统内置字典不能修改");
		}
		return R.ok(this.updateById(dict));
	}

}
