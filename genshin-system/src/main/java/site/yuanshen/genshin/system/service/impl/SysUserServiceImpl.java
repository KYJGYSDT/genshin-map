package site.yuanshen.genshin.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.common.constant.CommonConstant;
import site.yuanshen.genshin.common.constant.SecurityConstant;
import site.yuanshen.genshin.common.exception.DataObsoleteException;
import site.yuanshen.genshin.common.exception.ResourceNotExistException;
import site.yuanshen.genshin.common.mybatis.DataScope;
import site.yuanshen.genshin.common.security.GenshinUser;
import site.yuanshen.genshin.common.security.GenshinUserDetailService;
import site.yuanshen.genshin.common.security.SocialUser;
import site.yuanshen.genshin.system.dto.UserDTO;
import site.yuanshen.genshin.system.dto.UserInfo;
import site.yuanshen.genshin.system.entity.SysDept;
import site.yuanshen.genshin.system.entity.SysRole;
import site.yuanshen.genshin.system.entity.SysUser;
import site.yuanshen.genshin.system.entity.SysUserRole;
import site.yuanshen.genshin.system.manager.UserInfoManager;
import site.yuanshen.genshin.system.mapper.SysUserMapper;
import site.yuanshen.genshin.system.service.*;
import site.yuanshen.genshin.system.vo.MenuVO;
import site.yuanshen.genshin.system.vo.UserVO;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @date 2017/10/31
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService, GenshinUserDetailService {
	private static final PasswordEncoder ENCODER = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	private final SysMenuService sysMenuService;
	private final SysRoleService sysRoleService;
	private final SysDeptService sysDeptService;
	private final SysUserRoleService sysUserRoleService;
	private final SysDeptRelationService sysDeptRelationService;
	private final CacheManager cacheManager;
	private final UserInfoManager userInfoManager;

	/**
	 * 保存用户信息
	 *
	 * @param userDto DTO 对象
	 * @return success/fail
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean saveUser(UserDTO userDto) {
		SysUser sysUser = new SysUser();
		BeanUtils.copyProperties(userDto, sysUser);
		sysUser.setPassword(ENCODER.encode(userDto.getPassword()));
		baseMapper.insert(sysUser);
		List<SysUserRole> userRoleList = userDto.getRole().stream().map(roleId -> {
			SysUserRole userRole = new SysUserRole();
			userRole.setUserId(sysUser.getUserId());
			userRole.setRoleId(roleId);
			return userRole;
		}).collect(Collectors.toList());
		return sysUserRoleService.saveBatch(userRoleList);
	}

	/**
	 * 通过查用户的全部信息
	 *
	 * @param sysUser 用户
	 * @return
	 */
	@Override
	public UserInfo findUserInfo(SysUser sysUser) {
		UserInfo userInfo = new UserInfo();
		sysUser.setPassword(null);
		userInfo.setSysUser(sysUser);
		// 设置角色列表 （ID）
		List<Long> roleIds = sysRoleService.findRolesByUserId(sysUser.getUserId()).stream().map(SysRole::getRoleId)
				.collect(Collectors.toList());
		userInfo.setRoles(ArrayUtil.toArray(roleIds, Long.class));

		// 设置权限列表（menu.permission）
		Set<String> permissions = new HashSet<>();
		roleIds.forEach(roleId -> {
			List<String> permissionList = sysMenuService.findMenuByRoleId(roleId).stream()
					.map(MenuVO::getPermission).filter(StringUtils::isNotEmpty)
					.collect(Collectors.toList());
			permissions.addAll(permissionList);
		});
		userInfo.setPermissions(ArrayUtil.toArray(permissions, String.class));
		return userInfo;
	}

	/**
	 * 分页查询用户信息（含有角色信息）
	 *
	 * @param page    分页对象
	 * @param userDTO 参数列表
	 * @return
	 */
	@Override
	public Page<UserVO> getUsersWithRolePage(Page<UserVO> page, UserDTO userDTO) {
		return baseMapper.getUserVosPage(page, userDTO, new DataScope());
	}

	/**
	 * 通过ID查询用户信息
	 *
	 * @param id 用户ID
	 * @return 用户信息
	 */
	@Override
	public UserVO selectUserVoById(Long id) {
		return baseMapper.getUserVoById(id);
	}

	/**
	 * 删除用户
	 *
	 * @param sysUser 用户
	 * @return Boolean
	 */
	@Override
	@CacheEvict(value = CacheConstant.USER_DETAILS, key = "#sysUser.username")
	public Boolean deleteUserById(SysUser sysUser) {
		sysUserRoleService.deleteByUserId(sysUser.getUserId());
		this.removeById(sysUser.getUserId());
		return Boolean.TRUE;
	}

	@Override
	@CacheEvict(value = CacheConstant.USER_DETAILS, key = "#userDto.username")
	public R<Boolean> updateUserInfo(UserDTO userDto) {
		UserVO userVO = baseMapper.getUserVoByUsername(userDto.getUsername());
		if (!ENCODER.matches(userDto.getPassword(), userVO.getPassword())) {
			log.info("原密码错误，修改个人信息失败:{}", userDto.getUsername());
			return R.failed("原密码错误，修改个人信息失败");
		}

		SysUser sysUser = new SysUser();
		if (StrUtil.isNotBlank(userDto.getNewpassword1())) {
			sysUser.setPassword(ENCODER.encode(userDto.getNewpassword1()));
		}
		sysUser.setUserId(userVO.getUserId());
		sysUser.setAvatar(userDto.getAvatar());
		return R.ok(this.updateById(sysUser));
	}

	@Override
	@Caching(evict = {
			@CacheEvict(value = CacheConstant.USER_DETAILS, key = "#userDto.username"),
			@CacheEvict(value = CacheConstant.ROLE_OF_USER, key = "#userDto.userId")
	})
	public Boolean updateUser(UserDTO userDto) {
		SysUser sysUser = this.getById(userDto.getUserId());
		if (Objects.isNull(sysUser)) {
			throw new ResourceNotExistException("用户不存在");
		}

		if (!Objects.equals(sysUser.getVersion(), userDto.getVersion())) {
			throw new DataObsoleteException("用户已被修改");
		}

		BeanUtils.copyProperties(userDto, sysUser);
		sysUser.setUpdateTime(LocalDateTime.now());

		if (StrUtil.isNotBlank(userDto.getPassword())) {
			sysUser.setPassword(ENCODER.encode(userDto.getPassword()));
		}

		if (!this.updateById(sysUser)) {
			return Boolean.FALSE;
		}

		sysUserRoleService.remove(Wrappers.<SysUserRole>update().lambda().eq(SysUserRole::getUserId, userDto.getUserId()));
		userDto.getRole().forEach(roleId -> {
			SysUserRole userRole = new SysUserRole();
			userRole.setUserId(sysUser.getUserId());
			userRole.setRoleId(roleId);
			userRole.insert();
		});
		return Boolean.TRUE;
	}

	/**
	 * 查询上级部门的用户信息
	 *
	 * @param username 用户名
	 * @return R
	 */
	@Override
	public List<SysUser> listAncestorUsers(String username) {
		SysUser sysUser = this.getOne(Wrappers.<SysUser>query().lambda().eq(SysUser::getUsername, username));

		SysDept sysDept = sysDeptService.getById(sysUser.getDeptId());
		if (sysDept == null) {
			return null;
		}

		Long parentId = sysDept.getParentId();
		return this.list(Wrappers.<SysUser>query().lambda().eq(SysUser::getDeptId, parentId));
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Cache cache = cacheManager.getCache(CacheConstant.USER_DETAILS);
		if (cache != null && cache.get(username) != null) {
			return cache.get(username, GenshinUser.class);
		}

		UserInfo userInfo = findInfoByUsername(username);
		UserDetails userDetails = getUserDetails(userInfo);
		cache.put(username, userDetails);
		return userDetails;
	}

	private UserDetails getUserDetails(UserInfo userInfo) {
		if (Objects.isNull(userInfo)) {
			throw new UsernameNotFoundException("用户不存在");
		}

		SysUser user = userInfo.getSysUser();
		boolean enabled = StrUtil.equals(user.getLockFlag(), CommonConstant.STATUS_NORMAL);

		Set<String> authsSet = new HashSet<>();
		if (ArrayUtil.isNotEmpty(userInfo.getRoles())) {
			Arrays.stream(userInfo.getRoles()).forEach(roleId -> authsSet.add(SecurityConstant.ROLE + roleId));
			CollUtil.addAll(authsSet, ArrayUtil.filter(userInfo.getPermissions(), StringUtils::isNotBlank));
		}

		List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(ArrayUtil.toArray(authsSet, String.class));

		return new GenshinUser(user.getUserId(), user.getUsername(), user.getPassword(), enabled, user.getDeptId(), user.getAvatar(),
				true, true, enabled, authorities);
	}

	@Override
	public UserInfo findInfoByUsername(String username) {
		return userInfoManager.getUserInfo(baseMapper.selectOne(Wrappers.<SysUser>lambdaQuery().eq(SysUser::getUsername, username)));
	}

	@Override
	public UserDetails loadUserBySocial(String uuid, String grantType) {
		SysUser sysUser = baseMapper.selectOne(Wrappers.<SysUser>lambdaQuery().eq(SysUser::getGiteeLogin, uuid));

		Cache cache = cacheManager.getCache(CacheConstant.USER_DETAILS);
		if (cache != null && cache.get(sysUser.getUsername()) != null) {
			return cache.get(sysUser.getUsername(), GenshinUser.class);
		}
		UserDetails userDetails = getUserDetails(userInfoManager.getUserInfo(sysUser));
		if (Objects.nonNull(userDetails)) {
			cache.put(sysUser.getUsername(), userDetails);
			return userDetails;
		}
		return null;
	}

	@Override
	@Transactional
	public UserDetails registerSocialUser(SocialUser socialUser) {
		SysUser user = new SysUser();
		user.setGiteeLogin(socialUser.getUuid());
		user.setAvatar(socialUser.getAvatar());
		user.setDeptId(10L);
		user.setUsername(socialUser.getNickname() + StringPool.UNDERSCORE + RandomStringUtils.randomAlphabetic(6));
		user.setPassword(ENCODER.encode(RandomStringUtils.randomNumeric(11)));
		baseMapper.insertSocialUser(user);

		List<SysUserRole> userRoleList = Lists.newArrayList();
		SysUserRole userRole = new SysUserRole();
		userRole.setUserId(user.getUserId());
		userRole.setRoleId(2L);
		userRoleList.add(userRole);
		sysUserRoleService.saveBatch(userRoleList);

		return getUserDetails(userInfoManager.getUserInfo(user));
	}
}
