package site.yuanshen.genshin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import site.yuanshen.genshin.system.entity.SysVersion;

/**
 * 版本管理
 *
 * @author :  Hu
 */
@Mapper
public interface SysVersionMapper extends BaseMapper<SysVersion> {
}