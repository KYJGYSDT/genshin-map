package site.yuanshen.genshin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.system.entity.SysI18n;

import java.util.List;

/**
 * i18n
 */
public interface SysI18nService extends IService<SysI18n> {

	/**
	 * 根据关联code查询
	 *
	 * @param btnText 关联code
	 * @return List
	 */
	List<SysI18n> findByCode(String btnText);
}
