package site.yuanshen.genshin.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.system.dto.SysOauthClientDetailsDTO;
import site.yuanshen.genshin.system.entity.SysOauthClientDetails;

public interface SysOauthClientDetailsService extends IService<SysOauthClientDetails> {

	/**
	 * 通过ID删除客户端
	 *
	 * @param clientId
	 * @return
	 */
	Boolean removeByClientId(String clientId);

	/**
	 * 根据客户端信息
	 *
	 * @param clientDetailsDTO
	 * @return
	 */
	Boolean updateClientById(SysOauthClientDetailsDTO clientDetailsDTO);

	/**
	 * 添加客户端
	 *
	 * @param clientDetailsDTO
	 * @return
	 */
	Boolean saveClient(SysOauthClientDetailsDTO clientDetailsDTO);

	/**
	 * 分页查询客户端信息
	 *
	 * @param page
	 * @param query
	 * @return
	 */
	Page<SysOauthClientDetailsDTO> queryPage(Page<SysOauthClientDetails> page, SysOauthClientDetails query);

}
