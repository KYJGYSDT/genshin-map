package site.yuanshen.genshin.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @since 2017-10-29
 */
@Data
@ApiModel(value = "角色")
@EqualsAndHashCode(callSuper = true)
public class SysRole extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 角色编号
	 */
	@TableId(value = "role_id", type = IdType.AUTO)
	@ApiModelProperty(value = "角色编号")
	private Long roleId;

	/**
	 * 角色名称
	 */
	@NotBlank(message = "角色名称不能为空")
	@ApiModelProperty(value = "角色名称")
	private String roleName;

	/**
	 * 角色标识
	 */
	@NotBlank(message = "角色标识不能为空")
	@ApiModelProperty(value = "角色标识")
	private String roleCode;

	/**
	 * 角色描述
	 */
	@ApiModelProperty(value = "角色描述")
	private String roleDesc;

	/**
	 * 数据权限类型
	 */
	@NotNull(message = "数据权限类型不能为空")
	@ApiModelProperty(value = "数据权限类型")
	private Integer dsType;

	/**
	 * 数据权限作用范围
	 */
	@ApiModelProperty(value = "数据权限作用范围")
	private String dsScope;

}
