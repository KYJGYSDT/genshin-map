package site.yuanshen.genshin.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author :  Hu
 */
@Data
@ApiModel(value = "版本信息")
public class VersionDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@ApiModelProperty(value = "ID")
	private Long id;

	/**
	 * 名
	 */
	@ApiModelProperty(value = "名")
	private String name;

	/**
	 * 版本
	 */
	@ApiModelProperty(value = "版本")
	private String version;

	/**
	 * 启用二维码
	 */
	@Pattern(regexp = "1|0", message = "参数不在可选范围内")
	@ApiModelProperty(value = "启用二维码 1:启用 0:不启用")
	private String qrCode;

	/**
	 * 是否已完成
	 */
	@Pattern(regexp = "1|0", message = "参数不在可选范围内")
	@ApiModelProperty(value = "是否已完成 1:完成 0:未完成")
	private String disabled;

	/**
	 * 背景图片
	 */
	@ApiModelProperty(value = "背景图片")
	private String backgroundImg;

	/**
	 * 网盘地址
	 */
	@ApiModelProperty(value = "网盘地址")
	private List<Map<String, String>> netdisk;

	/**
	 * 按钮文字
	 */
	@ApiModelProperty(value = "按钮文字")
	private List<Map<String, String>> btnText;
}
