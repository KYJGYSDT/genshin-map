package site.yuanshen.genshin.system.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 部门关系表
 * </p>
 *
 * @since 2018-01-22
 */
@Data
@ApiModel(value = "部门关系")
public class SysDeptRelation implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 祖先节点
	 */
	@ApiModelProperty(value = "祖先节点")
	private Long ancestor;

	/**
	 * 后代节点
	 */
	@ApiModelProperty(value = "后代节点")
	private Long descendant;

}
