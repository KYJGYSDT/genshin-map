package site.yuanshen.genshin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import site.yuanshen.genshin.system.entity.SysPublicParam;

/**
 * 公共参数配置
 *
 * @author Lucky
 * @date 2019-04-29
 */
@Mapper
public interface SysPublicParamMapper extends BaseMapper<SysPublicParam> {

}
