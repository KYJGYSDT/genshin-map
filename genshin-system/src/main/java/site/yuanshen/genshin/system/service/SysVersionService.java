package site.yuanshen.genshin.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.system.dto.VersionDTO;
import site.yuanshen.genshin.system.entity.SysVersion;
import site.yuanshen.genshin.system.vo.VersionVO;

import java.util.List;

/**
 * 版本管理
 *
 * @author :  Hu
 */
public interface SysVersionService extends IService<SysVersion> {

	/**
	 * 查询激活的版本信息
	 *
	 * @return List
	 */
	List<SysVersion> listActivated();

	/**
	 * 保存版本信息
	 *
	 * @return Boolean
	 */
	Boolean save(VersionDTO versionDTO);

	/**
	 * vo分页
	 *
	 * @return Page
	 */
	Page<VersionVO> voPage(Page<SysVersion> page, SysVersion version);

	/**
	 * 激活版本
	 *
	 * @return Boolean
	 */
	Boolean activeVersion(Long versionId);

	/**
	 * 更新版本信息
	 *
	 * @return Boolean
	 */
	Boolean update(VersionDTO versionDTO);
}
