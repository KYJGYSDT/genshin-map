package site.yuanshen.genshin.system.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.common.exception.ResourceNotExistException;
import site.yuanshen.genshin.common.i18n.BaseI18nService;
import site.yuanshen.genshin.system.dto.VersionDTO;
import site.yuanshen.genshin.system.entity.SysI18n;
import site.yuanshen.genshin.system.entity.SysVersion;
import site.yuanshen.genshin.system.enums.VersionActiveEnum;
import site.yuanshen.genshin.system.manager.I18nManager;
import site.yuanshen.genshin.system.mapper.SysVersionMapper;
import site.yuanshen.genshin.system.service.SysI18nService;
import site.yuanshen.genshin.system.service.SysVersionService;
import site.yuanshen.genshin.system.vo.VersionVO;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author :  Hu
 */
@Service
@AllArgsConstructor
public class SysVersionServiceImpl extends ServiceImpl<SysVersionMapper, SysVersion> implements SysVersionService {
	private final BaseI18nService dbMessageSource;
	private final SysI18nService sysI18NService;
	private final I18nManager i18nManager;

	@Override
	@Cacheable(value = CacheConstant.VERSION_KEY, key = "'activated:all'")
	public List<SysVersion> listActivated() {
		return baseMapper.selectList(Wrappers.<SysVersion>lambdaQuery().eq(SysVersion::getActive, VersionActiveEnum.ACTIVATED.getCode()));
	}

	@Override
	@Transactional
	public Boolean save(VersionDTO versionDTO) {
		SysVersion version = new SysVersion();
		version.setName(versionDTO.getName());
		version.setBVersion(versionDTO.getVersion());
		version.setQrCode(versionDTO.getQrCode());
		version.setDisabled(versionDTO.getDisabled());
		version.setBackgroundImg(versionDTO.getBackgroundImg());
		version.setNetdisk(JSONObject.toJSONString(versionDTO.getNetdisk()));

		String code = dbMessageSource.storeI18n(versionDTO.getBtnText());
		version.setBtnText(code);

		return SqlHelper.retBool(baseMapper.insert(version));
	}

	@Override
	public Page<VersionVO> voPage(Page<SysVersion> page, SysVersion version) {
		Page<SysVersion> versionPage = baseMapper.selectPage(page, Wrappers.lambdaQuery(version));
		List<VersionVO> voRecords = versionPage.getRecords().stream().map(VersionVO::new).peek(vo -> {
			vo.setBtnText(sysI18NService.findByCode((String) vo.getBtnText()));
		}).collect(Collectors.toList());

		Page<VersionVO> voPage = new Page<>();
		BeanUtils.copyProperties(versionPage, voPage);
		voPage.setRecords(voRecords);

		return voPage;
	}

	@Override
	@Transactional
	@CacheEvict(value = CacheConstant.VERSION_KEY, allEntries = true)
	public Boolean activeVersion(Long versionId) {
		SysVersion version = baseMapper.selectById(versionId);
		if (Objects.isNull(version)) {
			throw new ResourceNotExistException("版本信息不存在");
		}

		version.setActive(VersionActiveEnum.ACTIVATED.getCode());

		baseMapper.update(null, Wrappers.lambdaUpdate(SysVersion.class)
				.set(SysVersion::getActive, VersionActiveEnum.INACTIVE.getCode())
				.ne(SysVersion::getId, version.getId())
				.eq(SysVersion::getName, version.getName())
		);

		return SqlHelper.retBool(baseMapper.updateById(version));
	}

	@Override
	@Transactional
	@CacheEvict(value = CacheConstant.VERSION_KEY, allEntries = true)
	public Boolean update(VersionDTO versionDTO) {
		SysVersion version = new SysVersion();
		version.setId(versionDTO.getId());
		version.setName(versionDTO.getName());
		version.setBVersion(versionDTO.getVersion());
		version.setQrCode(versionDTO.getQrCode());
		version.setDisabled(versionDTO.getDisabled());
		version.setBackgroundImg(versionDTO.getBackgroundImg());
		version.setNetdisk(JSONObject.toJSONString(versionDTO.getNetdisk()));

		String btnTextJson = JSONObject.toJSONString(versionDTO.getBtnText());
		List<SysI18n> sysI18ns = JSONObject.parseObject(btnTextJson, new TypeReference<List<SysI18n>>() {
		});
		boolean i18nUpdate = i18nManager.updateBatchById(sysI18ns);

		return SqlHelper.retBool(baseMapper.updateById(version)) && i18nUpdate;
	}
}
