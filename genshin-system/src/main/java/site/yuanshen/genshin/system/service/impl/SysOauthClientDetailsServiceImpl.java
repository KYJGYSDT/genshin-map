package site.yuanshen.genshin.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.common.constant.CommonConstant;
import site.yuanshen.genshin.system.dto.SysOauthClientDetailsDTO;
import site.yuanshen.genshin.system.entity.SysOauthClientDetails;
import site.yuanshen.genshin.system.mapper.SysOauthClientDetailsMapper;
import site.yuanshen.genshin.system.service.SysOauthClientDetailsService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class SysOauthClientDetailsServiceImpl extends ServiceImpl<SysOauthClientDetailsMapper, SysOauthClientDetails>
		implements SysOauthClientDetailsService, ClientDetailsService {

	/**
	 * 通过ID删除客户端
	 *
	 * @param clientId
	 * @return
	 */
	@Override
	@CacheEvict(value = CacheConstant.CLIENT_DETAILS_KEY, key = "#clientId")
	public Boolean removeByClientId(String clientId) {
		// 更新库
		baseMapper
				.delete(Wrappers.<SysOauthClientDetails>lambdaQuery().eq(SysOauthClientDetails::getClientId, clientId));
		// 更新Redis
//		SpringContextHolder.publishEvent(new ClientDetailsInitRunner.ClientDetailsInitEvent(clientId));
		return Boolean.TRUE;
	}

	/**
	 * 根据客户端信息
	 *
	 * @param clientDetailsDTO
	 * @return
	 */
	@Override
	@CacheEvict(value = CacheConstant.CLIENT_DETAILS_KEY, key = "#clientDetailsDTO.clientId")
	public Boolean updateClientById(SysOauthClientDetailsDTO clientDetailsDTO) {
		// copy dto 对象
		SysOauthClientDetails clientDetails = new SysOauthClientDetails();
		BeanUtils.copyProperties(clientDetailsDTO, clientDetails);

		// 获取扩展信息,插入开关相关
		String information = clientDetailsDTO.getAdditionalInformation();
		JSONObject informationObj = JSONUtil.parseObj(information)
				.put(CommonConstant.CAPTCHA_FLAG, clientDetailsDTO.getCaptchaFlag())
				.put(CommonConstant.ENC_FLAG, clientDetailsDTO.getEncFlag());
		clientDetails.setAdditionalInformation(informationObj.toString());

		// 更新数据库
		baseMapper.updateById(clientDetails);
		// 更新Redis
//		SpringContextHolder.publishEvent(new ClientDetailsInitRunner.ClientDetailsInitEvent(clientDetails));
		return Boolean.TRUE;
	}

	/**
	 * 添加客户端
	 *
	 * @param clientDetailsDTO
	 * @return
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean saveClient(SysOauthClientDetailsDTO clientDetailsDTO) {
		// copy dto 对象
		SysOauthClientDetails clientDetails = new SysOauthClientDetails();
		BeanUtils.copyProperties(clientDetailsDTO, clientDetails);

		// 获取扩展信息,插入开关相关
		String information = clientDetailsDTO.getAdditionalInformation();

		JSONUtil.parseObj(information).put(CommonConstant.CAPTCHA_FLAG, clientDetailsDTO.getCaptchaFlag())
				.put(CommonConstant.ENC_FLAG, clientDetailsDTO.getEncFlag());

		// 插入数据
		this.baseMapper.insert(clientDetails);
		// 更新Redis
//		SpringContextHolder.publishEvent(new ClientDetailsInitRunner.ClientDetailsInitEvent(clientDetails));
		return Boolean.TRUE;
	}

	/**
	 * 分页查询客户端信息
	 *
	 * @param page
	 * @param query
	 * @return
	 */
	@Override
	public Page<SysOauthClientDetailsDTO> queryPage(Page<SysOauthClientDetails> page, SysOauthClientDetails query) {
		Page<SysOauthClientDetails> selectPage = baseMapper.selectPage(page, Wrappers.query(query));

		// 处理扩展字段组装dto
		List<SysOauthClientDetailsDTO> collect = selectPage.getRecords().stream().map(details -> {
			String information = details.getAdditionalInformation();
			String captchaFlag = JSONUtil.parseObj(information).getStr(CommonConstant.CAPTCHA_FLAG);
			String encFlag = JSONUtil.parseObj(information).getStr(CommonConstant.ENC_FLAG);
			SysOauthClientDetailsDTO dto = new SysOauthClientDetailsDTO();
			BeanUtils.copyProperties(details, dto);
			dto.setCaptchaFlag(captchaFlag);
			dto.setEncFlag(encFlag);
			return dto;
		}).collect(Collectors.toList());

		// 构建dto page 对象
		Page<SysOauthClientDetailsDTO> dtoPage = new Page<>(page.getCurrent(), page.getSize(), selectPage.getTotal());
		dtoPage.setRecords(collect);
		return dtoPage;
	}

	@Override
	@Cacheable(value = CacheConstant.CLIENT_DETAILS_KEY, key = "#clientId")
	public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
		return clientDetailsWrapper(baseMapper.selectOne(Wrappers.<SysOauthClientDetails>lambdaQuery().eq(SysOauthClientDetails::getClientId, clientId)));
	}


	private ClientDetails clientDetailsWrapper(SysOauthClientDetails origin) {
		BaseClientDetails target = new BaseClientDetails();
		// 必选项
		target.setClientId(origin.getClientId());
		target.setClientSecret(String.format("{noop}%s", origin.getClientSecret()));

		if (ArrayUtil.isNotEmpty(origin.getAuthorizedGrantTypes())) {
			target.setAuthorizedGrantTypes(CollUtil.newArrayList(origin.getAuthorizedGrantTypes()));
		}

		if (StrUtil.isNotBlank(origin.getAuthorities())) {
			target.setAuthorities(AuthorityUtils.commaSeparatedStringToAuthorityList(origin.getAuthorities()));
		}

		if (StrUtil.isNotBlank(origin.getResourceIds())) {
			target.setResourceIds(StringUtils.commaDelimitedListToSet(origin.getResourceIds()));
		}

		if (StrUtil.isNotBlank(origin.getWebServerRedirectUri())) {
			target.setRegisteredRedirectUri(StringUtils.commaDelimitedListToSet(origin.getWebServerRedirectUri()));
		}

		if (StrUtil.isNotBlank(origin.getScope())) {
			target.setScope(StringUtils.commaDelimitedListToSet(origin.getScope()));
		}

		if (StrUtil.isNotBlank(origin.getAutoapprove())) {
			target.setAutoApproveScopes(StringUtils.commaDelimitedListToSet(origin.getAutoapprove()));
		}

		if (origin.getAccessTokenValidity() != null) {
			target.setAccessTokenValiditySeconds(origin.getAccessTokenValidity());
		}

		if (origin.getRefreshTokenValidity() != null) {
			target.setRefreshTokenValiditySeconds(origin.getRefreshTokenValidity());
		}

		String json = origin.getAdditionalInformation();
		if (StrUtil.isNotBlank(json)) {
			try {
				@SuppressWarnings("unchecked")
				Map<String, Object> additionalInformation = JSONUtil.toBean(json, Map.class);
				target.setAdditionalInformation(additionalInformation);
			} catch (Exception e) {
				log.warn("Could not decode JSON for additional information: " + json, e);
			}
		}

		return target;
	}
}
