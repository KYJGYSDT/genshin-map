package site.yuanshen.genshin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import site.yuanshen.genshin.system.entity.SysDictItem;

/**
 * 字典项
 *
 * @date 2019/03/19
 */
@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}
