package site.yuanshen.genshin.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

/**
 * 用户表
 */
@ApiModel(value = "用户表")
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_user")
public class SysUser extends BaseEntity {
	private static final long serialVersionUID = 1L;
	/**
	 * 主键ID
	 */
	@TableId(value = "user_id", type = IdType.AUTO)
	@ApiModelProperty(value = "主键ID")
	private Long userId;

	/**
	 * 用户名
	 */
	@TableField(value = "username")
	@ApiModelProperty(value = "用户名")
	private String username;

	/**
	 * 密码
	 */
	@TableField(value = "`password`")
	@ApiModelProperty(value = "密码")
	private String password;

	/**
	 * 头像
	 */
	@TableField(value = "avatar")
	@ApiModelProperty(value = "头像")
	private String avatar;

	/**
	 * 部门ID
	 */
	@TableField(value = "dept_id")
	@ApiModelProperty(value = "部门ID")
	private Long deptId;

	/**
	 * 码云 标识
	 */
	@TableField(value = "gitee_login")
	@ApiModelProperty(value = "码云 标识")
	private String giteeLogin;

	/**
	 * 锁定标识
	 */
	@TableField(value = "lock_flag")
	@ApiModelProperty(value = "锁定标识")
	private String lockFlag;
}