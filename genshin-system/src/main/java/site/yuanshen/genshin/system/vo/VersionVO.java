package site.yuanshen.genshin.system.vo;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import site.yuanshen.genshin.system.entity.SysVersion;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author :  Hu
 */
@Data
@NoArgsConstructor
@ApiModel(value = "前端版本展示对象")
public class VersionVO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@ApiModelProperty(value = "ID")
	private Long id;

	/**
	 * 名
	 */
	@ApiModelProperty(value = "名")
	private String name;

	/**
	 * 版本
	 */
	@ApiModelProperty(value = "版本")
	private String version;

	/**
	 * 启用二维码
	 */
	@ApiModelProperty(value = "启用二维码")
	private String qrCode;

	/**
	 * 是否已完成
	 */
	@ApiModelProperty(value = "是否已完成")
	private String disabled;

	/**
	 * 背景图片
	 */
	@ApiModelProperty(value = "背景图片")
	private String backgroundImg;

	/**
	 * 激活
	 */
	@ApiModelProperty(value = "激活")
	private String active;

	/**
	 * 网盘地址
	 */
	@ApiModelProperty(value = "网盘地址")
	private List<Map<String, String>> netdisk;

	/**
	 * 按钮文字
	 */
	@ApiModelProperty(value = "按钮文字")
	private Object btnText;


	public VersionVO(SysVersion target) {
		if (Objects.nonNull(target)) {
			BeanUtils.copyProperties(target, this);
			this.netdisk = JSONObject.parseObject(target.getNetdisk(), new TypeReference<List<Map<String, String>>>() {
			});
			this.version = target.getBVersion();
		}
	}
}
