package site.yuanshen.genshin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.system.entity.SysDept;
import site.yuanshen.genshin.system.entity.SysDeptRelation;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @since 2018-02-12
 */
public interface SysDeptRelationService extends IService<SysDeptRelation> {

	/**
	 * 新建部门关系
	 *
	 * @param sysDept 部门
	 */
	void insertDeptRelation(SysDept sysDept);

	/**
	 * 通过ID删除部门关系
	 *
	 * @param id
	 */
	void deleteAllDeptRealtion(Long id);

	/**
	 * 更新部门关系
	 *
	 * @param relation
	 */
	void updateDeptRealtion(SysDeptRelation relation);

}
