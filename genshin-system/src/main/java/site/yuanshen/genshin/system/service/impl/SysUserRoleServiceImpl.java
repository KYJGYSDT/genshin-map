package site.yuanshen.genshin.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import site.yuanshen.genshin.system.entity.SysUserRole;
import site.yuanshen.genshin.system.mapper.SysUserRoleMapper;
import site.yuanshen.genshin.system.service.SysUserRoleService;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @since 2017-10-29
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

	/**
	 * 根据用户Id删除该用户的角色关系
	 *
	 * @param userId 用户ID
	 * @return boolean
	 * @author 寻欢·李
	 * @date 2017年12月7日 16:31:38
	 */
	@Override
	public Boolean deleteByUserId(Long userId) {
		return baseMapper.deleteByUserId(userId);
	}

}
