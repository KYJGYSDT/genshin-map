package site.yuanshen.genshin.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.common.constant.CommonConstant;
import site.yuanshen.genshin.system.dto.MenuTree;
import site.yuanshen.genshin.system.entity.SysMenu;
import site.yuanshen.genshin.system.entity.SysRoleMenu;
import site.yuanshen.genshin.system.enums.MenuTypeEnum;
import site.yuanshen.genshin.system.mapper.SysMenuMapper;
import site.yuanshen.genshin.system.mapper.SysRoleMenuMapper;
import site.yuanshen.genshin.system.service.SysMenuService;
import site.yuanshen.genshin.system.util.MenuTreeUtil;
import site.yuanshen.genshin.system.vo.MenuVO;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 *
 * @since 2017-10-29
 */
@Service
@AllArgsConstructor
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

	private final SysRoleMenuMapper sysRoleMenuMapper;

	@Override
	@Cacheable(value = CacheConstant.MENU_DETAILS, key = "#roleId", unless = "#result.isEmpty()")
	public List<MenuVO> findMenuByRoleId(Long roleId) {
		return baseMapper.listMenusByRoleId(roleId);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	@CacheEvict(value = CacheConstant.MENU_DETAILS, allEntries = true)
	public R<Boolean> removeMenuById(Long id) {
		// 查询父节点为当前节点的节点
		List<SysMenu> menuList = this.list(Wrappers.<SysMenu>query().lambda().eq(SysMenu::getParentId, id));
		if (CollUtil.isNotEmpty(menuList)) {
			return R.failed("菜单含有下级不能删除");
		}

		sysRoleMenuMapper.delete(Wrappers.<SysRoleMenu>query().lambda().eq(SysRoleMenu::getMenuId, id));
		// 删除当前菜单及其子菜单
		return R.ok(this.removeById(id));
	}

	@Override
	@CacheEvict(value = CacheConstant.MENU_DETAILS, allEntries = true)
	public Boolean updateMenuById(SysMenu sysMenu) {
		return this.updateById(sysMenu);
	}

	/**
	 * 构建树查询 1. 不是懒加载情况，查询全部 2. 是懒加载，根据parentId 查询 2.1 父节点为空，则查询ID -1
	 *
	 * @param lazy     是否是懒加载
	 * @param parentId 父节点ID
	 * @return
	 */
	@Override
	public List<MenuTree> treeMenu(boolean lazy, Long parentId) {
		if (!lazy) {
			return MenuTreeUtil.buildTree(
					baseMapper.selectList(Wrappers.<SysMenu>lambdaQuery().orderByAsc(SysMenu::getSort)),
					CommonConstant.MENU_TREE_ROOT_ID);
		}

		Long parent = parentId == null ? CommonConstant.MENU_TREE_ROOT_ID : parentId;
		return MenuTreeUtil.buildTree(
				baseMapper.selectList(
						Wrappers.<SysMenu>lambdaQuery().eq(SysMenu::getParentId, parent).orderByAsc(SysMenu::getSort)),
				parent);
	}

	/**
	 * 查询菜单
	 *
	 * @param all      全部菜单
	 * @param type     类型
	 * @param parentId 父节点ID
	 * @return
	 */
	@Override
	public List<MenuTree> filterMenu(Set<MenuVO> all, String type, Long parentId) {
		List<MenuTree> menuTreeList = all.stream().filter(menuTypePredicate(type)).map(MenuTree::new)
				.sorted(Comparator.comparingInt(MenuTree::getSort)).collect(Collectors.toList());
		Long parent = parentId == null ? CommonConstant.MENU_TREE_ROOT_ID : parentId;
		return MenuTreeUtil.build(menuTreeList, parent);
	}

	/**
	 * menu 类型断言
	 *
	 * @param type 类型
	 * @return Predicate
	 */
	private Predicate<MenuVO> menuTypePredicate(String type) {
		return vo -> {
			if (MenuTypeEnum.TOP_MENU.getDescription().equals(type)) {
				return MenuTypeEnum.TOP_MENU.getType().equals(vo.getType());
			}
			// 其他查询 左侧 + 顶部
			return !MenuTypeEnum.BUTTON.getType().equals(vo.getType());
		};
	}

}
