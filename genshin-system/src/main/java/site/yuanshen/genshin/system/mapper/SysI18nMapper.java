package site.yuanshen.genshin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import site.yuanshen.genshin.system.entity.SysI18n;

/**
 * i18n
 */
@Mapper
public interface SysI18nMapper extends BaseMapper<SysI18n> {
}