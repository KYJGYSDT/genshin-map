package site.yuanshen.genshin.system.controller;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import site.yuanshen.genshin.common.api.R;

/**
 * @author Hu
 */
@RestController
@AllArgsConstructor
@RequestMapping("/ts")
@Api(value = "ts", tags = "时间校准API")
public class SysTimestampController {

	@GetMapping("/now")
	public R<Long> nowTs() {
		return R.ok(System.currentTimeMillis());
	}

}
