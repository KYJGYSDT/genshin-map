package site.yuanshen.genshin.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * 用户状态
 */
@AllArgsConstructor
public enum UserStatus {

	/**
	 * 正常
	 */
	NORMAL("1", "正常"),

	/**
	 * 冻结
	 */
	FREEZE("-1", "冻结");

	@Getter
	private final String code;
	@Getter
	private final String desc;

	public static boolean isNormal(String code) {
		return StringUtils.equals(NORMAL.code, code);
	}
}
