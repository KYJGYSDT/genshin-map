package site.yuanshen.genshin.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.common.exception.ResourceAlreadyUseException;
import site.yuanshen.genshin.system.dto.VersionDTO;
import site.yuanshen.genshin.system.entity.SysVersion;
import site.yuanshen.genshin.system.enums.VersionActiveEnum;
import site.yuanshen.genshin.system.manager.VersionManager;
import site.yuanshen.genshin.system.service.SysVersionService;
import site.yuanshen.genshin.system.vo.VersionVO;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.List;
import java.util.Objects;

/**
 * 版本管理
 *
 * @author :  Hu
 */
@RestController
@AllArgsConstructor
@RequestMapping("/version")
@Api(value = "version", tags = "版本管理模块")
public class SysVersionController {
	private final SysVersionService versionService;
	private final VersionManager versionManager;

	/**
	 * 获取激活的版本
	 *
	 * @return R
	 */
	@GetMapping("/activated")
	@ApiOperation(value = "获取激活的版本", notes = "获取激活的版本")
	public R<List<VersionVO>> activatedVersion() {
		return R.ok(versionManager.listActivated());
	}

	/**
	 * 版本分页
	 *
	 * @return R
	 */
	@GetMapping("/page")
	@ApiOperation(value = "版本分页", notes = "版本分页")
	public R<Page<VersionVO>> page(Page<SysVersion> page, SysVersion version) {
		return R.ok(versionService.voPage(page, version));
	}

	/**
	 * 新增版本
	 *
	 * @return R
	 */
	@PostMapping
	@ApiOperation(value = "新增版本", notes = "新增版本")
	public R<Boolean> save(@RequestBody @Valid VersionDTO versionDTO) {
		return R.ok(versionService.save(versionDTO));
	}

	/**
	 * 删除版本
	 *
	 * @return R
	 */
	@DeleteMapping("/{versionId}")
	@ApiOperation(value = "删除版本", notes = "删除版本")
	public R<Boolean> remove(@PathVariable Long versionId) {
		SysVersion version = versionService.getById(versionId);
		if (Objects.nonNull(version) && VersionActiveEnum.isActivated(version.getActive())) {
			throw new ResourceAlreadyUseException("当前版本已被使用, 无法删除");
		}

		return R.ok(versionService.removeById(versionId));
	}

	/**
	 * 激活版本
	 *
	 * @return R
	 */
	@PutMapping("/{versionId}")
	@ApiOperation(value = "激活版本", notes = "激活版本")
	public R<Boolean> activate(@PathVariable Long versionId) {
		return R.ok(versionService.activeVersion(versionId));
	}

	/**
	 * 修改版本
	 *
	 * @return R
	 */
	@PutMapping
	@ApiOperation(value = "修改版本", notes = "修改版本")
	public R<Boolean> update(@RequestBody @Valid VersionDTO versionDTO) {
		if (Objects.isNull(versionDTO.getId())) {
			throw new ValidationException("版本ID不能为空");
		}

		return R.ok(versionService.update(versionDTO));
	}
}
