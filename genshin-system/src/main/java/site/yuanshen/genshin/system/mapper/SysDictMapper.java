package site.yuanshen.genshin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import site.yuanshen.genshin.system.entity.SysDict;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @since 2017-11-19
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {

}
