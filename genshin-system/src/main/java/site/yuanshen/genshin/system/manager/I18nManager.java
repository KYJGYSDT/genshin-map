package site.yuanshen.genshin.system.manager;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.system.entity.SysI18n;
import site.yuanshen.genshin.system.service.SysI18nService;

import java.util.Collection;

/**
 * i18n
 */
@Slf4j
@Component
@AllArgsConstructor
public class I18nManager {
	private final SysI18nService i18nService;
	private final CacheManager cacheManager;

	@Transactional
	public boolean updateBatchById(Collection<SysI18n> entityList) {
		Cache cache = cacheManager.getCache(CacheConstant.I18N_DETAILS);
		entityList.stream().map(i18n -> i18n.getCode() + ":" + i18n.getLocale()).forEach(cache::evict);
		entityList.stream().map(i18n -> i18n.getCode() + ":allLocale").forEach(cache::evict);
		return i18nService.updateBatchById(entityList);
	}
}
