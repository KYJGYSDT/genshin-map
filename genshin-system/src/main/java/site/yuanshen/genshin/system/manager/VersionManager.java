package site.yuanshen.genshin.system.manager;


import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import site.yuanshen.genshin.common.i18n.BaseI18nService;
import site.yuanshen.genshin.system.entity.SysVersion;
import site.yuanshen.genshin.system.service.SysVersionService;
import site.yuanshen.genshin.system.vo.VersionVO;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 版本
 */
@Slf4j
@Component
@AllArgsConstructor
public class VersionManager {
	private final SysVersionService versionService;
	private final BaseI18nService dbMessageSource;

	public List<VersionVO> listActivated() {
		List<SysVersion> activatedVersionList = versionService.listActivated();

		if (CollUtil.isEmpty(activatedVersionList)) {
			return Lists.newArrayList();
		}

		return activatedVersionList.stream().map(VersionVO::new)
				.peek(vo -> vo.setBtnText(dbMessageSource.getMessage((String) vo.getBtnText(), LocaleContextHolder.getLocale())))
				.collect(Collectors.toList());
	}

}
