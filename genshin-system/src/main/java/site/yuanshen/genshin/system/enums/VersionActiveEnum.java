package site.yuanshen.genshin.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author :  Hu
 */
@Getter
@AllArgsConstructor
public enum VersionActiveEnum {

	/**
	 * 激活
	 */
	ACTIVATED("1", "激活"),

	/**
	 * 未激活
	 */
	INACTIVE("0", "未激活");

	private final String code;
	private final String desc;

	public static Boolean isActivated(String code) {
		return ACTIVATED.getCode().equals(code);
	}
}
