package site.yuanshen.genshin.system.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.system.entity.SysOauthClientDetails;

@Data
@EqualsAndHashCode(callSuper = true)
public class SysOauthClientDetailsDTO extends SysOauthClientDetails {

	/**
	 * 验证码开关
	 */
	private String captchaFlag;

	/**
	 * 前端密码传输是否加密
	 */
	private String encFlag;

}
