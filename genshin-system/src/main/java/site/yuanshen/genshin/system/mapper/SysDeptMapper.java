package site.yuanshen.genshin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import site.yuanshen.genshin.system.entity.SysDept;

/**
 * <p>
 * 部门管理 Mapper 接口
 * </p>
 *
 * @since 2018-01-20
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
