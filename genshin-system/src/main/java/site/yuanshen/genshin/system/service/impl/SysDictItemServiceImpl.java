package site.yuanshen.genshin.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.system.entity.SysDict;
import site.yuanshen.genshin.system.entity.SysDictItem;
import site.yuanshen.genshin.system.enums.DictTypeEnum;
import site.yuanshen.genshin.system.mapper.SysDictItemMapper;
import site.yuanshen.genshin.system.service.SysDictItemService;
import site.yuanshen.genshin.system.service.SysDictService;

/**
 * 字典项
 *
 * @date 2019/03/19
 */
@Service
@AllArgsConstructor
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements SysDictItemService {

	private final SysDictService dictService;

	/**
	 * 删除字典项
	 *
	 * @param id 字典项ID
	 * @return
	 */
	@Override
	@CacheEvict(value = CacheConstant.DICT_DETAILS, allEntries = true)
	public R<Boolean> removeDictItem(Long id) {
		// 根据ID查询字典ID
		SysDictItem dictItem = this.getById(id);
		SysDict dict = dictService.getById(dictItem.getDictId());
		// 系统内置
		if (DictTypeEnum.SYSTEM.getType().equals(dict.getSystem())) {
			return R.failed("系统内置字典项目不能删除");
		}
		return R.ok(this.removeById(id));
	}

	/**
	 * 更新字典项
	 *
	 * @param item 字典项
	 * @return
	 */
	@Override
	@CacheEvict(value = CacheConstant.DICT_DETAILS, allEntries = true)
	public R<Boolean> updateDictItem(SysDictItem item) {
		// 查询字典
		SysDict dict = dictService.getById(item.getDictId());
		// 系统内置
		if (DictTypeEnum.SYSTEM.getType().equals(dict.getSystem())) {
			return R.failed("系统内置字典项目不能删除");
		}
		return R.ok(this.updateById(item));
	}

}
