package site.yuanshen.genshin.system.manager;


import cn.hutool.core.collection.CollectionUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import site.yuanshen.genshin.system.dto.UserInfo;
import site.yuanshen.genshin.system.entity.SysRole;
import site.yuanshen.genshin.system.entity.SysUser;
import site.yuanshen.genshin.system.service.SysMenuService;
import site.yuanshen.genshin.system.service.SysRoleService;
import site.yuanshen.genshin.system.vo.MenuVO;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 用户信息
 */
@Slf4j
@Component
@AllArgsConstructor
public class UserInfoManager {
	private final SysMenuService sysMenuService;
	private final SysRoleService sysRoleService;


	public UserInfo getUserInfo(SysUser user) {
		if (Objects.isNull(user)) {
			return null;
		}

		UserInfo userInfo = new UserInfo();
		userInfo.setSysUser(user);

		List<SysRole> roles = sysRoleService.findRolesByUserId(user.getUserId());
		if (CollectionUtil.isNotEmpty(roles)) {
			Long[] roleIds = roles.stream().map(SysRole::getRoleId).toArray(Long[]::new);
			userInfo.setRoles(roleIds);
			List<MenuVO> menus = Arrays.stream(roleIds).map(sysMenuService::findMenuByRoleId).flatMap(Collection::stream).collect(Collectors.toList());
			if (CollectionUtil.isNotEmpty(menus)) {
				userInfo.setPermissions(menus.stream().map(MenuVO::getPermission).toArray(String[]::new));
			}
		}
		return userInfo;
	}
}
