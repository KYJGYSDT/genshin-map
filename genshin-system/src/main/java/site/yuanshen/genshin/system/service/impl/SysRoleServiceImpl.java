package site.yuanshen.genshin.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.system.entity.SysRole;
import site.yuanshen.genshin.system.entity.SysRoleMenu;
import site.yuanshen.genshin.system.mapper.SysRoleMapper;
import site.yuanshen.genshin.system.service.SysRoleMenuService;
import site.yuanshen.genshin.system.service.SysRoleService;
import site.yuanshen.genshin.system.vo.RoleVO;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @since 2017-10-29
 */
@Service
@AllArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

	private final SysRoleMenuService roleMenuService;

	/**
	 * 通过用户ID，查询角色信息
	 *
	 * @param userId
	 * @return
	 */
	@Override
	@Cacheable(value = CacheConstant.ROLE_OF_USER, key = "#userId")
	public List<SysRole> findRolesByUserId(Long userId) {
		return baseMapper.listRolesByUserId(userId);
	}

	/**
	 * 根据角色ID 查询角色列表，注意缓存删除
	 *
	 * @param roleIdList 角色ID列表
	 * @param key        缓存key
	 * @return
	 */
	@Override
	@Cacheable(value = CacheConstant.ROLE_DETAILS, key = "#key")
	public List<SysRole> findRolesByRoleIds(List<Long> roleIdList, String key) {
		return baseMapper.selectBatchIds(roleIdList);
	}

	/**
	 * 通过角色ID，删除角色,并清空角色菜单缓存
	 *
	 * @param id
	 * @return
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean removeRoleById(Long id) {
		roleMenuService.remove(Wrappers.<SysRoleMenu>update().lambda().eq(SysRoleMenu::getRoleId, id));
		return this.removeById(id);
	}

	/**
	 * 根据角色菜单列表
	 *
	 * @param roleVo 角色&菜单列表
	 * @return
	 */
	@Override
	public Boolean updateRoleMenus(RoleVO roleVo) {
		SysRole sysRole = baseMapper.selectById(roleVo.getRoleId());
		return roleMenuService.saveRoleMenus(sysRole.getRoleCode(), roleVo.getRoleId(), roleVo.getMenuIds());
	}

}
