package site.yuanshen.genshin.system.util;

import lombok.experimental.UtilityClass;
import site.yuanshen.genshin.common.util.TreeUtil;
import site.yuanshen.genshin.system.dto.MenuTree;
import site.yuanshen.genshin.system.entity.SysMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * 树 工具类
 */
@UtilityClass
public class MenuTreeUtil extends TreeUtil {

	/**
	 * 通过sysMenu创建树形节点
	 *
	 * @param menus
	 * @param root
	 * @return
	 */
	public List<MenuTree> buildTree(List<SysMenu> menus, Long root) {
		List<MenuTree> trees = new ArrayList<>();
		MenuTree node;
		for (SysMenu menu : menus) {
			node = new MenuTree();
			node.setId(menu.getMenuId());
			node.setParentId(menu.getParentId());
			node.setName(menu.getName());
			node.setPath(menu.getPath());
			node.setPermission(menu.getPermission());
			node.setLabel(menu.getName());
			node.setIcon(menu.getIcon());
			node.setType(menu.getType());
			node.setSort(menu.getSort());
			node.setHasChildren(true);
			node.setKeepAlive(menu.getKeepAlive());
			trees.add(node);
		}
		return MenuTreeUtil.build(trees, root);
	}

}
