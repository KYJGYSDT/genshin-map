package site.yuanshen.genshin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.system.entity.SysDictItem;

/**
 * 字典项
 *
 * @date 2019/03/19
 */
public interface SysDictItemService extends IService<SysDictItem> {

	/**
	 * 删除字典项
	 *
	 * @param id 字典项ID
	 * @return
	 */
	R<Boolean> removeDictItem(Long id);

	/**
	 * 更新字典项
	 *
	 * @param item 字典项
	 * @return
	 */
	R<Boolean> updateDictItem(SysDictItem item);

}
