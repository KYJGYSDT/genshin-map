package site.yuanshen.genshin.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @date 2020-11-18
 * <p>
 * 密码是否加密传输
 */
@Getter
@AllArgsConstructor
public enum EncFlagTypeEnum {

	/**
	 * 是
	 */
	YES("1", "是"),

	/**
	 * 否
	 */
	NO("0", "否");

	/**
	 * 类型
	 */
	private final String type;

	/**
	 * 描述
	 */
	private final String description;

}
