package site.yuanshen.genshin.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

/**
 * i18n
 */
@ApiModel(value = "i18n")
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_i18n")
public class SysI18n extends BaseEntity {
	/**
	 * ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "ID")
	private Long id;

	/**
	 * i18n关联code
	 */
	@TableField(value = "code")
	@ApiModelProperty(value = "i18n关联code")
	private String code;

	/**
	 * 语言标识
	 */
	@TableField(value = "`locale`")
	@ApiModelProperty(value = "语言标识")
	private String locale;

	/**
	 * 内容
	 */
	@TableField(value = "content")
	@ApiModelProperty(value = "内容")
	private String content;
}