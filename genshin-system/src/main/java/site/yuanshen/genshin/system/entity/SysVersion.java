package site.yuanshen.genshin.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.BaseEntity;

/**
 * 版本管理
 *
 * @author :  Hu
 */
@ApiModel(value = "版本管理")
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_version")
public class SysVersion extends BaseEntity {
	/**
	 * ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "ID")
	private Long id;

	/**
	 * 名
	 */
	@TableField(value = "`name`")
	@ApiModelProperty(value = "名")
	private String name;

	/**
	 * 版本
	 */
	@TableField(value = "b_version")
	@ApiModelProperty(value = "版本")
	private String bVersion;

	/**
	 * 启用二维码
	 */
	@TableField(value = "qr_code")
	@ApiModelProperty(value = "启用二维码")
	private String qrCode;

	/**
	 * 是否已完成
	 */
	@TableField(value = "disabled")
	@ApiModelProperty(value = "是否已完成")
	private String disabled;

	/**
	 * 背景图片
	 */
	@TableField(value = "background_img")
	@ApiModelProperty(value = "背景图片")
	private String backgroundImg;

	/**
	 * 网盘地址
	 */
	@TableField(value = "netdisk")
	@ApiModelProperty(value = "网盘地址")
	private String netdisk;

	/**
	 * 激活
	 */
	@TableField(value = "active")
	@ApiModelProperty(value = "激活")
	private String active;

	/**
	 * 按钮文字
	 */
	@TableField(value = "btn_text")
	@ApiModelProperty(value = "按钮文字")
	private String btnText;
}