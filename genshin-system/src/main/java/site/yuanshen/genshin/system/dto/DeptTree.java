package site.yuanshen.genshin.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import site.yuanshen.genshin.common.base.TreeNode;

/**
 * 部门树对象
 *
 * @date 2020-06-19
 */
@Data
@ApiModel(value = "部门树")
@EqualsAndHashCode(callSuper = true)
public class DeptTree extends TreeNode {

	@ApiModelProperty(value = "部门名称")
	private String name;

	/**
	 * 是否显示被锁定
	 */
	private Boolean isLock = true;

}
