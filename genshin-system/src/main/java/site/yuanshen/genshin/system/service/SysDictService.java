package site.yuanshen.genshin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.system.entity.SysDict;

/**
 * 字典表
 *
 * @date 2019/03/19
 */
public interface SysDictService extends IService<SysDict> {

	/**
	 * 根据ID 删除字典
	 *
	 * @param id
	 * @return
	 */
	R<Boolean> removeDict(Long id);

	/**
	 * 更新字典
	 *
	 * @param sysDict 字典
	 * @return
	 */
	R<Boolean> updateDict(SysDict sysDict);

}
