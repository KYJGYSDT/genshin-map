package site.yuanshen.genshin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import site.yuanshen.genshin.common.api.R;
import site.yuanshen.genshin.system.entity.SysPublicParam;

/**
 * 公共参数配置
 *
 * @author Lucky
 * @date 2019-04-29
 */
public interface SysPublicParamService extends IService<SysPublicParam> {

	/**
	 * 通过key查询公共参数指定值
	 *
	 * @param publicKey
	 * @return
	 */
	String getSysPublicParamKeyToValue(String publicKey);

	/**
	 * 更新参数
	 *
	 * @param sysPublicParam
	 * @return
	 */
	R<Boolean> updateParam(SysPublicParam sysPublicParam);

	/**
	 * 删除参数
	 *
	 * @param publicId
	 * @return
	 */
	R<Boolean> removeParam(Long publicId);

}
