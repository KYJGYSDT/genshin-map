package site.yuanshen.genshin.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;
import site.yuanshen.genshin.common.constant.CacheConstant;
import site.yuanshen.genshin.common.i18n.BaseI18nService;
import site.yuanshen.genshin.common.sequence.SnowflakeSequence;
import site.yuanshen.genshin.system.entity.SysI18n;
import site.yuanshen.genshin.system.mapper.SysI18nMapper;
import site.yuanshen.genshin.system.service.SysI18nService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * i18n
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysI18NServiceImpl extends ServiceImpl<SysI18nMapper, SysI18n> implements SysI18nService, BaseI18nService {
	private final SnowflakeSequence sequence;

	@Override
	@Cacheable(value = CacheConstant.I18N_DETAILS, key = "#code + ':' + #locale.toLanguageTag()", unless = "#result == null")
	public String getMessage(String code, Locale locale) throws NoSuchMessageException {
		SysI18n sysI18N = baseMapper.selectOne(Wrappers.<SysI18n>lambdaQuery().eq(SysI18n::getCode, code).eq(SysI18n::getLocale, locale.toLanguageTag()));
		if (Objects.isNull(sysI18N)) {
			log.error("no such message found, code:{}, locale:{}", code, locale.toLanguageTag());
			return null;
		}
		return sysI18N.getContent();
	}

	@Override
	public String storeI18n(List<Map<String, String>> i18nMessages) {
		String code = sequence.nextStrId();

		this.saveBatch(i18nMessages.stream().map(Map::entrySet).flatMap(Collection::stream).map(entry -> {
			SysI18n sysI18N = new SysI18n();
			sysI18N.setLocale(entry.getKey());
			sysI18N.setContent(entry.getValue());
			sysI18N.setCode(code);
			return sysI18N;
		}).collect(Collectors.toList()));

		return code;
	}

	@Override
	@Cacheable(value = CacheConstant.I18N_DETAILS, key = "#btnText + ':allLocale'")
	public List<SysI18n> findByCode(String btnText) {
		return baseMapper.selectList(Wrappers.<SysI18n>lambdaQuery().eq(SysI18n::getCode, btnText));
	}
}
